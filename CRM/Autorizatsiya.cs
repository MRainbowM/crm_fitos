﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace CRM
{
    public partial class Autorizatsiya : Telerik.WinControls.UI.RadForm
    {
        public Autorizatsiya()
        {
            InitializeComponent();
        }

        private void enter_Click(object sender, EventArgs e)
        {
            User user = new User();
            if (Validation())
            {
                if (user.Validation(tbLogin.Text, tbPassword.Text))
                {
                    MainForm radForm1 = new MainForm();
                    this.Visible = false;
                    radForm1.ShowDialog();
                    //this.Close();
                }
                else
                {
                    MessageBox.Show(
                    "Неправильный логин или пароль",
                    "Ошибка авторизации!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Stop,
                    MessageBoxDefaultButton.Button1,
                    MessageBoxOptions.DefaultDesktopOnly);
                }
            }
        }

        private void Autorizatsiya_Load(object sender, EventArgs e)
        {
            ApplicationContext.Check();

            tbPassword.PasswordChar = '*';
            changePassword.Visible = false;
            tbLogin.Text = "admin";
            tbPassword.Text = "admin";
        }

        private bool Validation() //true - все норм false - ошибки
        {
            string message = "\n";
            bool v = true;
            if (tbLogin.Text == "") { error.SetError(tbLogin, "Заполните поле!"); message += "Логин \n"; v = false; }
            if (tbPassword.Text == "") { error.SetError(tbPassword, "Заполните поле!"); message += "Пароль \n"; v = false; }

            if (v == false)
            {
                DialogResult result = MessageBox.Show(
                "Заполните поля: " + message,
                "Ошибка авторизации!",
                MessageBoxButtons.OK,
                MessageBoxIcon.Stop,
                MessageBoxDefaultButton.Button1,
                MessageBoxOptions.DefaultDesktopOnly);
            }
            return v;
        }
    }
}
