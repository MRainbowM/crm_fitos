﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace CRM
{
    public partial class SelectSchedule : Telerik.WinControls.UI.RadForm
    {
        public SelectSchedule()
        {
            InitializeComponent();
        }

        public static List<ScheduleService> SelectSchedules = new List<ScheduleService>();


        private void SelectSchedule_Load(object sender, EventArgs e)
        {
            List<ScheduleService> ScheduleAll = ScheduleService.GetAll();
            List<ScheduleService> ScheduleNow = new List<ScheduleService>();
            for (int i = 0; i < ScheduleAll.Count; i++)
            {
                DateTime s = DateTime.Now.AddHours(-DateTime.Now.Hour);
                if (ScheduleAll[i].Date.AddMinutes(ScheduleAll[i].Duration) >= DateTime.Now.AddHours(-DateTime.Now.Hour))
                {
                    if (ScheduleAll[i].Date <= DateTime.Now.AddDays(1))
                    {
                        ScheduleNow.Add(ScheduleAll[i]);
                    }
                }
            }
            SelectSchedules.Clear();
            GVSchedule.Rows.Clear();
            ScheduleService schedule;



            List<ScheduleService> ReservList = Reservation.FindReservationScheduleNowByIDClient(MainForm.Client.id);



            List<Reservation> reservations = new List<Reservation>();
            int k = 0;
            for (int i = 0; i < ScheduleNow.Count; i++)
            {
                int p = 0;
                reservations = Reservation.FindByIDSchudule(ScheduleNow[i].ID);
                for (int j = 0; j < ReservList.Count; j++)
                {
                    for (int x = 0; x < reservations.Count; x++)
                    {
                        if (ReservList[j].ID == reservations[x].ID_ScheduleService)
                        {
                            p++;
                        }
                    }
                }
                if (p == 0)
                {
                    k = ScheduleNow[i].NumberOfPeople - reservations.Count;
                    if (k > 0)
                    {
                        schedule = ScheduleNow[i];
                        GVSchedule.Rows.Add(schedule.ID, schedule.Date, Service.FindByID(schedule.ID_Service).name, Room.FindByID(schedule.ID_Room).name, k);
                    }
                }
            }

        }

        private void GVSchedule_Click(object sender, EventArgs e)
        {
            if (GVSchedule.RowCount > 0)
            {
                int row = GVSchedule.CurrentCell.RowIndex;
                if (GVSchedule.Rows[row].Cells[5].Value != null)
                {
                    if ((bool)GVSchedule.Rows[row].Cells[5].Value == true)
                    {
                        GVSchedule.Rows[row].Cells[5].Value = false;
                    }
                    else
                    {
                        GVSchedule.Rows[row].Cells[5].Value = true;
                    }
                }
                else
                {
                    GVSchedule.Rows[row].Cells[5].Value = true;
                }
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            SelectSchedules.Clear();
            ScheduleService service = new ScheduleService();
            for (int i = 0; i < GVSchedule.RowCount; i++)
            {
                if (GVSchedule.Rows[i].Cells[5].Value != null)
                {
                    if ((bool)GVSchedule.Rows[i].Cells[5].Value)
                    {
                        service = ScheduleService.FindByID(Convert.ToInt32(GVSchedule.Rows[i].Cells[0].Value));
                        SelectSchedules.Add(service);
                    }
                }
            }
            this.Close();
        }
    }
}
