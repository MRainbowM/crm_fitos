﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace CRM
{
    public partial class SearchRoom : Telerik.WinControls.UI.RadForm
    {
        public SearchRoom()
        {
            InitializeComponent();
        }

        //public static List<Room> Rooms = Room.GetAll();
        public static List<Room> FilterRooms = new List<Room>();

        string NameRoom;
        int From;
        int To;

        private void SearchRoom_Load(object sender, EventArgs e)
        {
            spFrom.Minimum = 0;
            spFrom.Maximum = 1000;
            spTo.Maximum = 1000;
            //tbName.Text = NameRoom;
            //spFrom.Value = From;
            //spTo.Value = To;

            spFrom.Enabled = false;
            spTo.Enabled = false;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            NameRoom = tbName.Text;
            From = (int)spFrom.Value;
            To = (int)spTo.Value;
            int p = -1;

            MainForm.RoomList = Room.GetAll();
            FilterRooms.Clear();
            if (cbCapacity.Checked == false)
            {
                for (int i = 0; i < MainForm.RoomList.Count; i++)
                {
                    p = MainForm.RoomList[i].name.ToLower().IndexOf(NameRoom.ToLower());
                    if (p > -1)
                    {
                        FilterRooms.Add(MainForm.RoomList[i]);
                    }
                    p = -1;
                }
            }
            if (cbCapacity.Checked)
            {
                for (int i = 0; i < MainForm.RoomList.Count; i++)
                {
                    if (MainForm.RoomList[i].capacity >= spFrom.Value)
                    {
                        if (MainForm.RoomList[i].capacity <= spTo.Value)
                        {
                            FilterRooms.Add(MainForm.RoomList[i]);
                        }
                    }
                }
            }

            if (FilterRooms.Count != 0)
            {
                MainForm.RoomList.Clear();
                for (int i = 0; i < FilterRooms.Count; i++)
                {
                    MainForm.RoomList.Add(FilterRooms[i]);
                }
            }
            else
            {
                MessageBox.Show(
                "Совпадений не найдено",
                "Результат поиска",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information,
                MessageBoxDefaultButton.Button1,
                MessageBoxOptions.DefaultDesktopOnly);
                MainForm.RoomList = Room.GetAll();
            }
            this.Close();
        }

        private void spFrom_ValueChanged(object sender, EventArgs e)
        {
            spTo.Minimum = spFrom.Value;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            tbName.Clear();
            spFrom.Value = 0;
            spTo.Value = 0;
        }


        private void cbCapacity_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (cbCapacity.Checked)
            {
                spFrom.Enabled = true;
                spTo.Enabled = true;
            }
            else
            {
                spFrom.Enabled = false;
                spTo.Enabled = false;
            }
        }
    }
}
