﻿namespace CRM
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn1 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn2 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn2 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn3 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn4 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn5 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn3 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn6 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn4 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn7 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor1 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor2 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor3 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor4 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor5 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor6 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor7 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor8 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor9 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor1 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition6 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn8 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn30 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn31 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn32 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn33 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn34 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn35 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor10 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor11 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor12 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor13 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor14 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor15 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor16 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor17 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor18 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor2 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition7 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn9 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn36 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn37 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn38 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn39 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor19 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor20 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor21 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor22 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor23 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor24 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor25 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor26 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor27 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor3 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition8 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn10 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn40 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn41 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn42 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor28 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor29 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor30 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor4 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition9 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn11 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn43 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn44 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn45 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn46 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn47 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor31 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor32 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor33 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor5 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition10 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn12 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn48 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn49 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn50 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor6 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition11 = new Telerik.WinControls.UI.TableViewDefinition();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.PageView = new Telerik.WinControls.UI.RadPageView();
            this.radPageViewPageOperation = new Telerik.WinControls.UI.RadPageViewPage();
            this.radPageView1 = new Telerik.WinControls.UI.RadPageView();
            this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.btnGetCard = new Telerik.WinControls.UI.RadButton();
            this.cbNCard = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.GVReservation = new Telerik.WinControls.UI.RadGridView();
            this.btnSelectClient = new Telerik.WinControls.UI.RadButton();
            this.lNCard = new Telerik.WinControls.UI.RadLabel();
            this.btnSelectSchedule = new Telerik.WinControls.UI.RadButton();
            this.tbNameClient = new Telerik.WinControls.UI.RadTextBox();
            this.btnAddVisit = new Telerik.WinControls.UI.RadButton();
            this.tbAmount = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.radPageViewPage5 = new Telerik.WinControls.UI.RadPageViewPage();
            this.btnGetCard2 = new Telerik.WinControls.UI.RadButton();
            this.cbNCard2 = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.btnSelectClient2 = new Telerik.WinControls.UI.RadButton();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.tbNameClient2 = new Telerik.WinControls.UI.RadTextBox();
            this.btnEndVisit = new Telerik.WinControls.UI.RadButton();
            this.radGridView4 = new Telerik.WinControls.UI.RadGridView();
            this.GVReservation2 = new Telerik.WinControls.UI.RadGridView();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pvPayment = new Telerik.WinControls.UI.RadPageViewPage();
            this.spCountPayment = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.spToPayment = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel17 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.spFromPayment = new Telerik.WinControls.UI.RadSpinEditor();
            this.btnPrevPayment = new Telerik.WinControls.UI.RadButton();
            this.btnNextPayment = new Telerik.WinControls.UI.RadButton();
            this.btnDeletePay = new Telerik.WinControls.UI.RadButton();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.tbAmountPayments = new Telerik.WinControls.UI.RadTextBox();
            this.rbYear = new Telerik.WinControls.UI.RadRadioButton();
            this.rbDay = new Telerik.WinControls.UI.RadRadioButton();
            this.rbMonth = new Telerik.WinControls.UI.RadRadioButton();
            this.GVPayments = new Telerik.WinControls.UI.RadGridView();
            this.searchPay = new Telerik.WinControls.UI.RadButton();
            this.addPay = new Telerik.WinControls.UI.RadButton();
            this.pvSchedule = new Telerik.WinControls.UI.RadPageViewPage();
            this.spCountSchedule = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.spToSchedule = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.spFromSchedule = new Telerik.WinControls.UI.RadSpinEditor();
            this.btnPrevSchedule = new Telerik.WinControls.UI.RadButton();
            this.btnNextSchedule = new Telerik.WinControls.UI.RadButton();
            this.btnClear = new Telerik.WinControls.UI.RadButton();
            this.tbServiceInSchedule = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.delSchedule = new Telerik.WinControls.UI.RadButton();
            this.CalendarSchedule = new Telerik.WinControls.UI.RadCalendar();
            this.GVSchedule = new Telerik.WinControls.UI.RadGridView();
            this.addSchedule = new Telerik.WinControls.UI.RadButton();
            this.pvClients = new Telerik.WinControls.UI.RadPageViewPage();
            this.spCountClient = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.spToClient = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.spFromClient = new Telerik.WinControls.UI.RadSpinEditor();
            this.btnPrevClient = new Telerik.WinControls.UI.RadButton();
            this.btnNextClient = new Telerik.WinControls.UI.RadButton();
            this.GVClients = new Telerik.WinControls.UI.RadGridView();
            this.searchClient = new Telerik.WinControls.UI.RadButton();
            this.AddClient = new Telerik.WinControls.UI.RadButton();
            this.pvCards = new Telerik.WinControls.UI.RadPageViewPage();
            this.spCountCard = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel19 = new Telerik.WinControls.UI.RadLabel();
            this.spToCard = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel20 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel21 = new Telerik.WinControls.UI.RadLabel();
            this.spFromCard = new Telerik.WinControls.UI.RadSpinEditor();
            this.btnPrevCard = new Telerik.WinControls.UI.RadButton();
            this.btnNextCard = new Telerik.WinControls.UI.RadButton();
            this.GVCard = new Telerik.WinControls.UI.RadGridView();
            this.searchCard = new Telerik.WinControls.UI.RadButton();
            this.addCard = new Telerik.WinControls.UI.RadButton();
            this.pvTrainers = new Telerik.WinControls.UI.RadPageViewPage();
            this.GVTrainers = new Telerik.WinControls.UI.RadGridView();
            this.searchWorker = new Telerik.WinControls.UI.RadButton();
            this.addTrainer = new Telerik.WinControls.UI.RadButton();
            this.pvServices = new Telerik.WinControls.UI.RadPageViewPage();
            this.GVServices = new Telerik.WinControls.UI.RadGridView();
            this.searchService = new Telerik.WinControls.UI.RadButton();
            this.addService = new Telerik.WinControls.UI.RadButton();
            this.pvTariffs = new Telerik.WinControls.UI.RadPageViewPage();
            this.GVTariffs = new Telerik.WinControls.UI.RadGridView();
            this.searchTariff = new Telerik.WinControls.UI.RadButton();
            this.addTariff = new Telerik.WinControls.UI.RadButton();
            this.pvRooms = new Telerik.WinControls.UI.RadPageViewPage();
            this.searchRoom = new Telerik.WinControls.UI.RadButton();
            this.addRoom = new Telerik.WinControls.UI.RadButton();
            this.GVRooms = new Telerik.WinControls.UI.RadGridView();
            this.pvStatistics = new Telerik.WinControls.UI.RadPageViewPage();
            this.ChartStatistics = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.radGroupBox8 = new Telerik.WinControls.UI.RadGroupBox();
            this.rbDateStatistics = new Telerik.WinControls.UI.RadRadioButton();
            this.rbServiceStatistics = new Telerik.WinControls.UI.RadRadioButton();
            this.gbDateStatistics = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.toStatistics = new Telerik.WinControls.UI.RadDateTimePicker();
            this.fromStatistics = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.gbServiceStatistics = new Telerik.WinControls.UI.RadGroupBox();
            this.cbService = new System.Windows.Forms.CheckedListBox();
            this.radPageViewPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.MenuOperations = new Telerik.WinControls.UI.RadMenuItem();
            this.MenuSchedule = new Telerik.WinControls.UI.RadMenuItem();
            this.MenuClients = new Telerik.WinControls.UI.RadMenuItem();
            this.MenuCards = new Telerik.WinControls.UI.RadMenuItem();
            this.MenuWorkers = new Telerik.WinControls.UI.RadMenuItem();
            this.MenuServices = new Telerik.WinControls.UI.RadMenuItem();
            this.MenuTariffs = new Telerik.WinControls.UI.RadMenuItem();
            this.MenuRooms = new Telerik.WinControls.UI.RadMenuItem();
            this.MenuPay = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenu1 = new Telerik.WinControls.UI.RadMenu();
            this.MenuOptions = new Telerik.WinControls.UI.RadMenuItem();
            this.MenuStatistics = new Telerik.WinControls.UI.RadMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.PageView)).BeginInit();
            this.PageView.SuspendLayout();
            this.radPageViewPageOperation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).BeginInit();
            this.radPageView1.SuspendLayout();
            this.radPageViewPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnGetCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbNCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVReservation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVReservation.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSelectClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lNCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSelectSchedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNameClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddVisit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.radPageViewPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnGetCard2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbNCard2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSelectClient2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNameClient2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEndVisit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView4.MasterTemplate)).BeginInit();
            this.radGridView4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVReservation2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVReservation2.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.pvPayment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spCountPayment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spToPayment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spFromPayment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrevPayment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNextPayment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeletePay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAmountPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVPayments.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchPay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addPay)).BeginInit();
            this.pvSchedule.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spCountSchedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spToSchedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spFromSchedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrevSchedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNextSchedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbServiceInSchedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.delSchedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalendarSchedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVSchedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVSchedule.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addSchedule)).BeginInit();
            this.pvClients.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spCountClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spToClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spFromClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrevClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNextClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVClients)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVClients.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddClient)).BeginInit();
            this.pvCards.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spCountCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spToCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spFromCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrevCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNextCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVCard.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addCard)).BeginInit();
            this.pvTrainers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVTrainers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVTrainers.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchWorker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addTrainer)).BeginInit();
            this.pvServices.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVServices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVServices.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchService)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addService)).BeginInit();
            this.pvTariffs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVTariffs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVTariffs.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchTariff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addTariff)).BeginInit();
            this.pvRooms.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchRoom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addRoom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVRooms)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVRooms.MasterTemplate)).BeginInit();
            this.pvStatistics.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChartStatistics)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox8)).BeginInit();
            this.radGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbDateStatistics)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbServiceStatistics)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbDateStatistics)).BeginInit();
            this.gbDateStatistics.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toStatistics)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fromStatistics)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbServiceStatistics)).BeginInit();
            this.gbServiceStatistics.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // PageView
            // 
            this.PageView.Controls.Add(this.radPageViewPageOperation);
            this.PageView.Controls.Add(this.pvPayment);
            this.PageView.Controls.Add(this.pvSchedule);
            this.PageView.Controls.Add(this.pvClients);
            this.PageView.Controls.Add(this.pvCards);
            this.PageView.Controls.Add(this.pvTrainers);
            this.PageView.Controls.Add(this.pvServices);
            this.PageView.Controls.Add(this.pvTariffs);
            this.PageView.Controls.Add(this.pvRooms);
            this.PageView.Controls.Add(this.pvStatistics);
            this.PageView.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PageView.Location = new System.Drawing.Point(12, 26);
            this.PageView.Name = "PageView";
            this.PageView.SelectedPage = this.pvSchedule;
            this.PageView.Size = new System.Drawing.Size(794, 461);
            this.PageView.TabIndex = 0;
            this.PageView.SelectedPageChanged += new System.EventHandler(this.PageView_SelectedPageChanged);
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.PageView.GetChildAt(0))).ShowItemPinButton = false;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.PageView.GetChildAt(0))).ShowItemCloseButton = true;
            // 
            // radPageViewPageOperation
            // 
            this.radPageViewPageOperation.Controls.Add(this.radPageView1);
            this.radPageViewPageOperation.ItemSize = new System.Drawing.SizeF(98F, 29F);
            this.radPageViewPageOperation.Location = new System.Drawing.Point(10, 38);
            this.radPageViewPageOperation.Name = "radPageViewPageOperation";
            this.radPageViewPageOperation.Size = new System.Drawing.Size(773, 412);
            this.radPageViewPageOperation.Text = "Операции";
            // 
            // radPageView1
            // 
            this.radPageView1.Controls.Add(this.radPageViewPage1);
            this.radPageView1.Controls.Add(this.radPageViewPage5);
            this.radPageView1.DefaultPage = this.radPageViewPage1;
            this.radPageView1.Location = new System.Drawing.Point(3, 3);
            this.radPageView1.Name = "radPageView1";
            this.radPageView1.SelectedPage = this.radPageViewPage1;
            this.radPageView1.Size = new System.Drawing.Size(767, 406);
            this.radPageView1.TabIndex = 0;
            this.radPageView1.ViewMode = Telerik.WinControls.UI.PageViewMode.Backstage;
            ((Telerik.WinControls.UI.StripViewItemContainer)(this.radPageView1.GetChildAt(0).GetChildAt(0))).MinSize = new System.Drawing.Size(170, 0);
            // 
            // radPageViewPage1
            // 
            this.radPageViewPage1.Controls.Add(this.btnGetCard);
            this.radPageViewPage1.Controls.Add(this.cbNCard);
            this.radPageViewPage1.Controls.Add(this.radLabel9);
            this.radPageViewPage1.Controls.Add(this.radLabel1);
            this.radPageViewPage1.Controls.Add(this.GVReservation);
            this.radPageViewPage1.Controls.Add(this.btnSelectClient);
            this.radPageViewPage1.Controls.Add(this.lNCard);
            this.radPageViewPage1.Controls.Add(this.btnSelectSchedule);
            this.radPageViewPage1.Controls.Add(this.tbNameClient);
            this.radPageViewPage1.Controls.Add(this.btnAddVisit);
            this.radPageViewPage1.Controls.Add(this.tbAmount);
            this.radPageViewPage1.Controls.Add(this.radLabel3);
            this.radPageViewPage1.Controls.Add(this.pictureBox1);
            this.radPageViewPage1.ItemSize = new System.Drawing.SizeF(136F, 45F);
            this.radPageViewPage1.Location = new System.Drawing.Point(175, 4);
            this.radPageViewPage1.Name = "radPageViewPage1";
            this.radPageViewPage1.Size = new System.Drawing.Size(588, 398);
            this.radPageViewPage1.Text = "Добавить посещение";
            // 
            // btnGetCard
            // 
            this.btnGetCard.Location = new System.Drawing.Point(277, 30);
            this.btnGetCard.Name = "btnGetCard";
            this.btnGetCard.Size = new System.Drawing.Size(110, 24);
            this.btnGetCard.TabIndex = 4;
            this.btnGetCard.Text = "Показать карту";
            this.btnGetCard.Click += new System.EventHandler(this.btnGetCard_Click);
            // 
            // cbNCard
            // 
            this.cbNCard.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbNCard.Location = new System.Drawing.Point(51, 33);
            this.cbNCard.Name = "cbNCard";
            this.cbNCard.Size = new System.Drawing.Size(220, 20);
            this.cbNCard.TabIndex = 14;
            this.cbNCard.Text = "radDropDownList1";
            this.cbNCard.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbNCard_SelectedIndexChanged);
            // 
            // radLabel9
            // 
            this.radLabel9.Location = new System.Drawing.Point(444, 377);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(25, 18);
            this.radLabel9.TabIndex = 3;
            this.radLabel9.Text = "руб";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(9, 3);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(31, 18);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "ФИО";
            // 
            // GVReservation
            // 
            this.GVReservation.Location = new System.Drawing.Point(5, 165);
            // 
            // 
            // 
            this.GVReservation.MasterTemplate.AllowAddNewRow = false;
            gridViewDecimalColumn1.HeaderText = "ID";
            gridViewDecimalColumn1.Name = "column7";
            gridViewDecimalColumn1.Width = 30;
            gridViewTextBoxColumn1.HeaderText = "Дата и время";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.Width = 80;
            gridViewTextBoxColumn2.HeaderText = "Услуга";
            gridViewTextBoxColumn2.Name = "column2";
            gridViewTextBoxColumn2.Width = 215;
            gridViewTextBoxColumn3.HeaderText = "Помещение";
            gridViewTextBoxColumn3.Name = "column6";
            gridViewTextBoxColumn3.Width = 80;
            gridViewDecimalColumn2.HeaderText = "Стоимость";
            gridViewDecimalColumn2.Name = "column8";
            gridViewDecimalColumn2.Width = 65;
            gridViewTextBoxColumn4.HeaderText = "Оплата";
            gridViewTextBoxColumn4.Name = "column4";
            gridViewCheckBoxColumn1.HeaderText = "Выбор";
            gridViewCheckBoxColumn1.Name = "column5";
            gridViewCheckBoxColumn1.Width = 43;
            this.GVReservation.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewDecimalColumn1,
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewDecimalColumn2,
            gridViewTextBoxColumn4,
            gridViewCheckBoxColumn1});
            this.GVReservation.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GVReservation.Name = "GVReservation";
            this.GVReservation.ReadOnly = true;
            this.GVReservation.Size = new System.Drawing.Size(580, 200);
            this.GVReservation.TabIndex = 17;
            this.GVReservation.Click += new System.EventHandler(this.GVReservation_Click);
            // 
            // btnSelectClient
            // 
            this.btnSelectClient.Location = new System.Drawing.Point(277, 0);
            this.btnSelectClient.Name = "btnSelectClient";
            this.btnSelectClient.Size = new System.Drawing.Size(110, 24);
            this.btnSelectClient.TabIndex = 3;
            this.btnSelectClient.Text = "Выбрать клиента";
            this.btnSelectClient.Click += new System.EventHandler(this.btnSelectClient_Click);
            // 
            // lNCard
            // 
            this.lNCard.Location = new System.Drawing.Point(9, 33);
            this.lNCard.Name = "lNCard";
            this.lNCard.Size = new System.Drawing.Size(36, 18);
            this.lNCard.TabIndex = 1;
            this.lNCard.Text = "Карта";
            // 
            // btnSelectSchedule
            // 
            this.btnSelectSchedule.Location = new System.Drawing.Point(5, 374);
            this.btnSelectSchedule.Name = "btnSelectSchedule";
            this.btnSelectSchedule.Size = new System.Drawing.Size(110, 24);
            this.btnSelectSchedule.TabIndex = 6;
            this.btnSelectSchedule.Text = "Выбрать услуги";
            this.btnSelectSchedule.Click += new System.EventHandler(this.btnSelectSchedule_Click);
            // 
            // tbNameClient
            // 
            this.tbNameClient.Location = new System.Drawing.Point(52, 3);
            this.tbNameClient.Name = "tbNameClient";
            this.tbNameClient.Size = new System.Drawing.Size(219, 20);
            this.tbNameClient.TabIndex = 1;
            this.tbNameClient.TextChanged += new System.EventHandler(this.tbNameClient_TextChanged);
            // 
            // btnAddVisit
            // 
            this.btnAddVisit.Location = new System.Drawing.Point(475, 372);
            this.btnAddVisit.Name = "btnAddVisit";
            this.btnAddVisit.Size = new System.Drawing.Size(110, 24);
            this.btnAddVisit.TabIndex = 5;
            this.btnAddVisit.Text = "Оплатить";
            this.btnAddVisit.Click += new System.EventHandler(this.btnAddVisit_Click);
            // 
            // tbAmount
            // 
            this.tbAmount.Enabled = false;
            this.tbAmount.Location = new System.Drawing.Point(371, 375);
            this.tbAmount.Name = "tbAmount";
            this.tbAmount.Size = new System.Drawing.Size(67, 20);
            this.tbAmount.TabIndex = 3;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(325, 377);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(40, 18);
            this.radLabel3.TabIndex = 2;
            this.radLabel3.Text = "Сумма";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(393, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(192, 159);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // radPageViewPage5
            // 
            this.radPageViewPage5.Controls.Add(this.btnGetCard2);
            this.radPageViewPage5.Controls.Add(this.cbNCard2);
            this.radPageViewPage5.Controls.Add(this.radLabel2);
            this.radPageViewPage5.Controls.Add(this.btnSelectClient2);
            this.radPageViewPage5.Controls.Add(this.radLabel4);
            this.radPageViewPage5.Controls.Add(this.tbNameClient2);
            this.radPageViewPage5.Controls.Add(this.btnEndVisit);
            this.radPageViewPage5.Controls.Add(this.radGridView4);
            this.radPageViewPage5.Controls.Add(this.pictureBox2);
            this.radPageViewPage5.ItemSize = new System.Drawing.SizeF(136F, 45F);
            this.radPageViewPage5.Location = new System.Drawing.Point(175, 4);
            this.radPageViewPage5.Name = "radPageViewPage5";
            this.radPageViewPage5.Size = new System.Drawing.Size(588, 398);
            this.radPageViewPage5.Text = "Закрыть посещение";
            // 
            // btnGetCard2
            // 
            this.btnGetCard2.Location = new System.Drawing.Point(281, 30);
            this.btnGetCard2.Name = "btnGetCard2";
            this.btnGetCard2.Size = new System.Drawing.Size(110, 24);
            this.btnGetCard2.TabIndex = 22;
            this.btnGetCard2.Text = "Показать карту";
            // 
            // cbNCard2
            // 
            this.cbNCard2.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbNCard2.Location = new System.Drawing.Point(55, 33);
            this.cbNCard2.Name = "cbNCard2";
            this.cbNCard2.Size = new System.Drawing.Size(220, 20);
            this.cbNCard2.TabIndex = 23;
            this.cbNCard2.Text = "radDropDownList1";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(13, 3);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(31, 18);
            this.radLabel2.TabIndex = 18;
            this.radLabel2.Text = "ФИО";
            // 
            // btnSelectClient2
            // 
            this.btnSelectClient2.Location = new System.Drawing.Point(281, 0);
            this.btnSelectClient2.Name = "btnSelectClient2";
            this.btnSelectClient2.Size = new System.Drawing.Size(110, 24);
            this.btnSelectClient2.TabIndex = 21;
            this.btnSelectClient2.Text = "Выбрать клиента";
            this.btnSelectClient2.Click += new System.EventHandler(this.btnSelectClient2_Click);
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(13, 33);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(36, 18);
            this.radLabel4.TabIndex = 19;
            this.radLabel4.Text = "Карта";
            // 
            // tbNameClient2
            // 
            this.tbNameClient2.Location = new System.Drawing.Point(56, 3);
            this.tbNameClient2.Name = "tbNameClient2";
            this.tbNameClient2.Size = new System.Drawing.Size(219, 20);
            this.tbNameClient2.TabIndex = 20;
            // 
            // btnEndVisit
            // 
            this.btnEndVisit.Location = new System.Drawing.Point(475, 371);
            this.btnEndVisit.Name = "btnEndVisit";
            this.btnEndVisit.Size = new System.Drawing.Size(110, 24);
            this.btnEndVisit.TabIndex = 17;
            this.btnEndVisit.Text = "Закрыть посещение";
            this.btnEndVisit.Click += new System.EventHandler(this.btnEndVisit_Click);
            // 
            // radGridView4
            // 
            this.radGridView4.Controls.Add(this.GVReservation2);
            this.radGridView4.Location = new System.Drawing.Point(5, 168);
            // 
            // 
            // 
            gridViewTextBoxColumn9.HeaderText = "Дата и время";
            gridViewTextBoxColumn9.Name = "column1";
            gridViewTextBoxColumn9.Width = 80;
            gridViewTextBoxColumn10.HeaderText = "Услуга";
            gridViewTextBoxColumn10.Name = "column2";
            gridViewTextBoxColumn10.Width = 220;
            gridViewTextBoxColumn11.HeaderText = "Помещение";
            gridViewTextBoxColumn11.Name = "column6";
            gridViewTextBoxColumn11.Width = 80;
            gridViewTextBoxColumn12.HeaderText = "Стоимость";
            gridViewTextBoxColumn12.Name = "column3";
            gridViewTextBoxColumn12.Width = 70;
            gridViewTextBoxColumn13.HeaderText = "По тарифу";
            gridViewTextBoxColumn13.Name = "column4";
            gridViewTextBoxColumn13.Width = 70;
            gridViewCheckBoxColumn2.HeaderText = "Выбор";
            gridViewCheckBoxColumn2.Name = "column5";
            gridViewCheckBoxColumn2.Width = 45;
            this.radGridView4.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewCheckBoxColumn2});
            this.radGridView4.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.radGridView4.Name = "radGridView4";
            this.radGridView4.Size = new System.Drawing.Size(580, 193);
            this.radGridView4.TabIndex = 16;
            // 
            // GVReservation2
            // 
            this.GVReservation2.Location = new System.Drawing.Point(0, -4);
            // 
            // 
            // 
            this.GVReservation2.MasterTemplate.AllowAddNewRow = false;
            gridViewDecimalColumn3.HeaderText = "ID";
            gridViewDecimalColumn3.Name = "column7";
            gridViewDecimalColumn3.Width = 30;
            gridViewTextBoxColumn5.HeaderText = "Дата и время";
            gridViewTextBoxColumn5.Name = "column1";
            gridViewTextBoxColumn5.Width = 80;
            gridViewTextBoxColumn6.HeaderText = "Услуга";
            gridViewTextBoxColumn6.Name = "column2";
            gridViewTextBoxColumn6.Width = 215;
            gridViewTextBoxColumn7.HeaderText = "Помещение";
            gridViewTextBoxColumn7.Name = "column6";
            gridViewTextBoxColumn7.Width = 80;
            gridViewDecimalColumn4.HeaderText = "Стоимость";
            gridViewDecimalColumn4.Name = "column8";
            gridViewDecimalColumn4.Width = 65;
            gridViewTextBoxColumn8.HeaderText = "Оплата";
            gridViewTextBoxColumn8.Name = "column4";
            this.GVReservation2.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewDecimalColumn3,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewDecimalColumn4,
            gridViewTextBoxColumn8});
            this.GVReservation2.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.GVReservation2.Name = "GVReservation2";
            this.GVReservation2.ReadOnly = true;
            this.GVReservation2.Size = new System.Drawing.Size(580, 200);
            this.GVReservation2.TabIndex = 18;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(397, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(188, 159);
            this.pictureBox2.TabIndex = 14;
            this.pictureBox2.TabStop = false;
            // 
            // pvPayment
            // 
            this.pvPayment.Controls.Add(this.spCountPayment);
            this.pvPayment.Controls.Add(this.radLabel16);
            this.pvPayment.Controls.Add(this.spToPayment);
            this.pvPayment.Controls.Add(this.radLabel17);
            this.pvPayment.Controls.Add(this.radLabel18);
            this.pvPayment.Controls.Add(this.spFromPayment);
            this.pvPayment.Controls.Add(this.btnPrevPayment);
            this.pvPayment.Controls.Add(this.btnNextPayment);
            this.pvPayment.Controls.Add(this.btnDeletePay);
            this.pvPayment.Controls.Add(this.radLabel7);
            this.pvPayment.Controls.Add(this.tbAmountPayments);
            this.pvPayment.Controls.Add(this.rbYear);
            this.pvPayment.Controls.Add(this.rbDay);
            this.pvPayment.Controls.Add(this.rbMonth);
            this.pvPayment.Controls.Add(this.GVPayments);
            this.pvPayment.Controls.Add(this.searchPay);
            this.pvPayment.Controls.Add(this.addPay);
            this.pvPayment.ItemSize = new System.Drawing.SizeF(91F, 29F);
            this.pvPayment.Location = new System.Drawing.Point(10, 38);
            this.pvPayment.Name = "pvPayment";
            this.pvPayment.Size = new System.Drawing.Size(773, 412);
            this.pvPayment.Text = "Платежи";
            // 
            // spCountPayment
            // 
            this.spCountPayment.Location = new System.Drawing.Point(717, 389);
            this.spCountPayment.Name = "spCountPayment";
            this.spCountPayment.Size = new System.Drawing.Size(53, 20);
            this.spCountPayment.TabIndex = 28;
            this.spCountPayment.TabStop = false;
            this.spCountPayment.ValueChanged += new System.EventHandler(this.spCountPayment_ValueChanged);
            // 
            // radLabel16
            // 
            this.radLabel16.Location = new System.Drawing.Point(533, 390);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(178, 18);
            this.radLabel16.TabIndex = 23;
            this.radLabel16.Text = "Количество записей на странице";
            // 
            // spToPayment
            // 
            this.spToPayment.Enabled = false;
            this.spToPayment.Location = new System.Drawing.Point(403, 361);
            this.spToPayment.Maximum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            0});
            this.spToPayment.Name = "spToPayment";
            this.spToPayment.Size = new System.Drawing.Size(53, 20);
            this.spToPayment.TabIndex = 29;
            this.spToPayment.TabStop = false;
            // 
            // radLabel17
            // 
            this.radLabel17.Location = new System.Drawing.Point(379, 362);
            this.radLabel17.Name = "radLabel17";
            this.radLabel17.Size = new System.Drawing.Size(18, 18);
            this.radLabel17.TabIndex = 24;
            this.radLabel17.Text = "из";
            // 
            // radLabel18
            // 
            this.radLabel18.Location = new System.Drawing.Point(258, 362);
            this.radLabel18.Name = "radLabel18";
            this.radLabel18.Size = new System.Drawing.Size(56, 18);
            this.radLabel18.TabIndex = 22;
            this.radLabel18.Text = "Страница";
            // 
            // spFromPayment
            // 
            this.spFromPayment.Location = new System.Drawing.Point(320, 361);
            this.spFromPayment.Maximum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            0});
            this.spFromPayment.Name = "spFromPayment";
            this.spFromPayment.Size = new System.Drawing.Size(53, 20);
            this.spFromPayment.TabIndex = 27;
            this.spFromPayment.TabStop = false;
            this.spFromPayment.ValueChanged += new System.EventHandler(this.spFrom_ValueChanged);
            // 
            // btnPrevPayment
            // 
            this.btnPrevPayment.Location = new System.Drawing.Point(4, 359);
            this.btnPrevPayment.Name = "btnPrevPayment";
            this.btnPrevPayment.Size = new System.Drawing.Size(110, 24);
            this.btnPrevPayment.TabIndex = 26;
            this.btnPrevPayment.Text = "<<";
            this.btnPrevPayment.Click += new System.EventHandler(this.btnPrevPayment_Click);
            // 
            // btnNextPayment
            // 
            this.btnNextPayment.Location = new System.Drawing.Point(660, 359);
            this.btnNextPayment.Name = "btnNextPayment";
            this.btnNextPayment.Size = new System.Drawing.Size(110, 24);
            this.btnNextPayment.TabIndex = 25;
            this.btnNextPayment.Text = ">>";
            this.btnNextPayment.Click += new System.EventHandler(this.btnNextPayment_Click);
            // 
            // btnDeletePay
            // 
            this.btnDeletePay.Location = new System.Drawing.Point(235, 3);
            this.btnDeletePay.Name = "btnDeletePay";
            this.btnDeletePay.Size = new System.Drawing.Size(110, 24);
            this.btnDeletePay.TabIndex = 6;
            this.btnDeletePay.Text = "Удалить платежи";
            this.btnDeletePay.Click += new System.EventHandler(this.btnDeletePay_Click);
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(564, 6);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(112, 18);
            this.radLabel7.TabIndex = 10;
            this.radLabel7.Text = "Суммарная выручка";
            // 
            // tbAmountPayments
            // 
            this.tbAmountPayments.Location = new System.Drawing.Point(682, 6);
            this.tbAmountPayments.Name = "tbAmountPayments";
            this.tbAmountPayments.Size = new System.Drawing.Size(88, 20);
            this.tbAmountPayments.TabIndex = 11;
            // 
            // rbYear
            // 
            this.rbYear.Location = new System.Drawing.Point(519, 6);
            this.rbYear.Name = "rbYear";
            this.rbYear.Size = new System.Drawing.Size(38, 18);
            this.rbYear.TabIndex = 9;
            this.rbYear.Text = "Год";
            this.rbYear.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbYear_ToggleStateChanged);
            // 
            // rbDay
            // 
            this.rbDay.Location = new System.Drawing.Point(407, 6);
            this.rbDay.Name = "rbDay";
            this.rbDay.Size = new System.Drawing.Size(46, 18);
            this.rbDay.TabIndex = 9;
            this.rbDay.Text = "День";
            this.rbDay.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbDay_ToggleStateChanged);
            // 
            // rbMonth
            // 
            this.rbMonth.Location = new System.Drawing.Point(459, 6);
            this.rbMonth.Name = "rbMonth";
            this.rbMonth.Size = new System.Drawing.Size(54, 18);
            this.rbMonth.TabIndex = 8;
            this.rbMonth.Text = "Месяц";
            this.rbMonth.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rdMonth_ToggleStateChanged);
            // 
            // GVPayments
            // 
            this.GVPayments.Location = new System.Drawing.Point(3, 33);
            // 
            // 
            // 
            this.GVPayments.MasterTemplate.AllowAddNewRow = false;
            gridViewDecimalColumn5.HeaderText = "ID";
            gridViewDecimalColumn5.Name = "column1";
            gridViewDecimalColumn5.Width = 30;
            gridViewTextBoxColumn14.HeaderText = "Дата платежа";
            gridViewTextBoxColumn14.Name = "date";
            gridViewTextBoxColumn14.Width = 80;
            gridViewTextBoxColumn15.HeaderText = "Услуга";
            gridViewTextBoxColumn15.Name = "servie";
            gridViewTextBoxColumn15.Width = 200;
            gridViewTextBoxColumn16.HeaderText = "Клиент";
            gridViewTextBoxColumn16.Name = "client";
            gridViewTextBoxColumn16.Width = 250;
            gridViewTextBoxColumn17.HeaderText = "Сумма";
            gridViewTextBoxColumn17.Name = "amount";
            gridViewTextBoxColumn17.Width = 80;
            gridViewTextBoxColumn18.HeaderText = "Статус";
            gridViewTextBoxColumn18.Name = "column2";
            gridViewTextBoxColumn18.Width = 60;
            gridViewCheckBoxColumn3.HeaderText = "Выбор";
            gridViewCheckBoxColumn3.Name = "column3";
            this.GVPayments.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewDecimalColumn5,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewCheckBoxColumn3});
            this.GVPayments.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.GVPayments.Name = "GVPayments";
            this.GVPayments.ReadOnly = true;
            this.GVPayments.Size = new System.Drawing.Size(767, 320);
            this.GVPayments.TabIndex = 7;
            this.GVPayments.Click += new System.EventHandler(this.GVPayments_Click);
            // 
            // searchPay
            // 
            this.searchPay.Location = new System.Drawing.Point(119, 3);
            this.searchPay.Name = "searchPay";
            this.searchPay.Size = new System.Drawing.Size(110, 24);
            this.searchPay.TabIndex = 6;
            this.searchPay.Text = "Поиск";
            this.searchPay.Click += new System.EventHandler(this.searchPay_Click);
            // 
            // addPay
            // 
            this.addPay.Location = new System.Drawing.Point(3, 3);
            this.addPay.Name = "addPay";
            this.addPay.Size = new System.Drawing.Size(110, 24);
            this.addPay.TabIndex = 5;
            this.addPay.Text = "Добавить платеж";
            this.addPay.Click += new System.EventHandler(this.addPay_Click);
            // 
            // pvSchedule
            // 
            this.pvSchedule.Controls.Add(this.spCountSchedule);
            this.pvSchedule.Controls.Add(this.radLabel12);
            this.pvSchedule.Controls.Add(this.spToSchedule);
            this.pvSchedule.Controls.Add(this.radLabel6);
            this.pvSchedule.Controls.Add(this.radLabel5);
            this.pvSchedule.Controls.Add(this.spFromSchedule);
            this.pvSchedule.Controls.Add(this.btnPrevSchedule);
            this.pvSchedule.Controls.Add(this.btnNextSchedule);
            this.pvSchedule.Controls.Add(this.btnClear);
            this.pvSchedule.Controls.Add(this.tbServiceInSchedule);
            this.pvSchedule.Controls.Add(this.radLabel8);
            this.pvSchedule.Controls.Add(this.delSchedule);
            this.pvSchedule.Controls.Add(this.CalendarSchedule);
            this.pvSchedule.Controls.Add(this.GVSchedule);
            this.pvSchedule.Controls.Add(this.addSchedule);
            this.pvSchedule.ItemSize = new System.Drawing.SizeF(108F, 29F);
            this.pvSchedule.Location = new System.Drawing.Point(10, 38);
            this.pvSchedule.Name = "pvSchedule";
            this.pvSchedule.Size = new System.Drawing.Size(773, 412);
            this.pvSchedule.Text = "Расписание";
            // 
            // spCountSchedule
            // 
            this.spCountSchedule.Location = new System.Drawing.Point(717, 389);
            this.spCountSchedule.Name = "spCountSchedule";
            this.spCountSchedule.Size = new System.Drawing.Size(53, 20);
            this.spCountSchedule.TabIndex = 13;
            this.spCountSchedule.TabStop = false;
            this.spCountSchedule.ValueChanged += new System.EventHandler(this.spCountSchedule_ValueChanged);
            // 
            // radLabel12
            // 
            this.radLabel12.Location = new System.Drawing.Point(533, 390);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(178, 18);
            this.radLabel12.TabIndex = 10;
            this.radLabel12.Text = "Количество записей на странице";
            // 
            // spToSchedule
            // 
            this.spToSchedule.Location = new System.Drawing.Point(482, 361);
            this.spToSchedule.Maximum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            0});
            this.spToSchedule.Name = "spToSchedule";
            this.spToSchedule.Size = new System.Drawing.Size(53, 20);
            this.spToSchedule.TabIndex = 13;
            this.spToSchedule.TabStop = false;
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(458, 362);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(18, 18);
            this.radLabel6.TabIndex = 10;
            this.radLabel6.Text = "из";
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(337, 362);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(56, 18);
            this.radLabel5.TabIndex = 9;
            this.radLabel5.Text = "Страница";
            // 
            // spFromSchedule
            // 
            this.spFromSchedule.Location = new System.Drawing.Point(399, 361);
            this.spFromSchedule.Maximum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            0});
            this.spFromSchedule.Name = "spFromSchedule";
            this.spFromSchedule.Size = new System.Drawing.Size(53, 20);
            this.spFromSchedule.TabIndex = 12;
            this.spFromSchedule.TabStop = false;
            this.spFromSchedule.ValueChanged += new System.EventHandler(this.spFromSchedule_ValueChanged);
            // 
            // btnPrevSchedule
            // 
            this.btnPrevSchedule.Location = new System.Drawing.Point(164, 359);
            this.btnPrevSchedule.Name = "btnPrevSchedule";
            this.btnPrevSchedule.Size = new System.Drawing.Size(110, 24);
            this.btnPrevSchedule.TabIndex = 11;
            this.btnPrevSchedule.Text = "<<";
            this.btnPrevSchedule.Click += new System.EventHandler(this.btnPrevSchedule_Click);
            // 
            // btnNextSchedule
            // 
            this.btnNextSchedule.Location = new System.Drawing.Point(660, 359);
            this.btnNextSchedule.Name = "btnNextSchedule";
            this.btnNextSchedule.Size = new System.Drawing.Size(110, 24);
            this.btnNextSchedule.TabIndex = 10;
            this.btnNextSchedule.Text = ">>";
            this.btnNextSchedule.Click += new System.EventHandler(this.btnNextSchedule_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(3, 359);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(155, 24);
            this.btnClear.TabIndex = 4;
            this.btnClear.Text = "Очистить";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // tbServiceInSchedule
            // 
            this.tbServiceInSchedule.Location = new System.Drawing.Point(3, 333);
            this.tbServiceInSchedule.Name = "tbServiceInSchedule";
            this.tbServiceInSchedule.Size = new System.Drawing.Size(155, 20);
            this.tbServiceInSchedule.TabIndex = 9;
            this.tbServiceInSchedule.TextChanged += new System.EventHandler(this.tbServiceInSchedule_TextChanged);
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(3, 309);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(39, 18);
            this.radLabel8.TabIndex = 8;
            this.radLabel8.Text = "Услуга";
            // 
            // delSchedule
            // 
            this.delSchedule.Location = new System.Drawing.Point(3, 33);
            this.delSchedule.Name = "delSchedule";
            this.delSchedule.Size = new System.Drawing.Size(155, 24);
            this.delSchedule.TabIndex = 5;
            this.delSchedule.Text = "Удалить записи";
            this.delSchedule.Click += new System.EventHandler(this.delSchedule_Click);
            // 
            // CalendarSchedule
            // 
            this.CalendarSchedule.Location = new System.Drawing.Point(3, 93);
            this.CalendarSchedule.Name = "CalendarSchedule";
            this.CalendarSchedule.Size = new System.Drawing.Size(155, 151);
            this.CalendarSchedule.TabIndex = 7;
            this.CalendarSchedule.SelectionChanged += new System.EventHandler(this.CalendarSchedule_SelectionChanged);
            // 
            // GVSchedule
            // 
            this.GVSchedule.Location = new System.Drawing.Point(164, 3);
            // 
            // 
            // 
            this.GVSchedule.MasterTemplate.AllowAddNewRow = false;
            gridViewDecimalColumn6.HeaderText = "ID";
            gridViewDecimalColumn6.Name = "column5";
            gridViewDecimalColumn6.Width = 35;
            gridViewTextBoxColumn19.HeaderText = "Дата и время";
            gridViewTextBoxColumn19.Name = "column1";
            gridViewTextBoxColumn19.Width = 90;
            gridViewTextBoxColumn20.HeaderText = "Услуга";
            gridViewTextBoxColumn20.Name = "column2";
            gridViewTextBoxColumn20.Width = 210;
            gridViewTextBoxColumn21.HeaderText = "Помещение";
            gridViewTextBoxColumn21.Name = "column6";
            gridViewTextBoxColumn21.Width = 90;
            gridViewTextBoxColumn22.HeaderText = "Своб. места";
            gridViewTextBoxColumn22.Name = "column4";
            gridViewTextBoxColumn22.Width = 70;
            gridViewTextBoxColumn23.HeaderText = "Кол. тренеров";
            gridViewTextBoxColumn23.Name = "column7";
            gridViewCheckBoxColumn4.HeaderText = "Удалить";
            gridViewCheckBoxColumn4.Name = "column3";
            this.GVSchedule.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewDecimalColumn6,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewCheckBoxColumn4});
            this.GVSchedule.MasterTemplate.ViewDefinition = tableViewDefinition5;
            this.GVSchedule.Name = "GVSchedule";
            this.GVSchedule.ReadOnly = true;
            this.GVSchedule.Size = new System.Drawing.Size(606, 350);
            this.GVSchedule.TabIndex = 6;
            this.GVSchedule.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.GVSchedule_CellDoubleClick);
            this.GVSchedule.Click += new System.EventHandler(this.GVSchedule_Click);
            // 
            // addSchedule
            // 
            this.addSchedule.Location = new System.Drawing.Point(3, 3);
            this.addSchedule.Name = "addSchedule";
            this.addSchedule.Size = new System.Drawing.Size(155, 24);
            this.addSchedule.TabIndex = 3;
            this.addSchedule.Text = "Создать запись";
            this.addSchedule.Click += new System.EventHandler(this.addSchedule_Click);
            // 
            // pvClients
            // 
            this.pvClients.Controls.Add(this.spCountClient);
            this.pvClients.Controls.Add(this.radLabel13);
            this.pvClients.Controls.Add(this.spToClient);
            this.pvClients.Controls.Add(this.radLabel14);
            this.pvClients.Controls.Add(this.radLabel15);
            this.pvClients.Controls.Add(this.spFromClient);
            this.pvClients.Controls.Add(this.btnPrevClient);
            this.pvClients.Controls.Add(this.btnNextClient);
            this.pvClients.Controls.Add(this.GVClients);
            this.pvClients.Controls.Add(this.searchClient);
            this.pvClients.Controls.Add(this.AddClient);
            this.pvClients.ItemSize = new System.Drawing.SizeF(90F, 29F);
            this.pvClients.Location = new System.Drawing.Point(10, 38);
            this.pvClients.Name = "pvClients";
            this.pvClients.Size = new System.Drawing.Size(773, 412);
            this.pvClients.Text = "Клиенты";
            // 
            // spCountClient
            // 
            this.spCountClient.Location = new System.Drawing.Point(716, 389);
            this.spCountClient.Name = "spCountClient";
            this.spCountClient.Size = new System.Drawing.Size(53, 20);
            this.spCountClient.TabIndex = 20;
            this.spCountClient.TabStop = false;
            this.spCountClient.ValueChanged += new System.EventHandler(this.spCountClient_ValueChanged);
            // 
            // radLabel13
            // 
            this.radLabel13.Location = new System.Drawing.Point(532, 390);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(178, 18);
            this.radLabel13.TabIndex = 15;
            this.radLabel13.Text = "Количество записей на странице";
            // 
            // spToClient
            // 
            this.spToClient.Enabled = false;
            this.spToClient.Location = new System.Drawing.Point(402, 361);
            this.spToClient.Maximum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            0});
            this.spToClient.Name = "spToClient";
            this.spToClient.Size = new System.Drawing.Size(53, 20);
            this.spToClient.TabIndex = 21;
            this.spToClient.TabStop = false;
            // 
            // radLabel14
            // 
            this.radLabel14.Location = new System.Drawing.Point(378, 362);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(18, 18);
            this.radLabel14.TabIndex = 16;
            this.radLabel14.Text = "из";
            // 
            // radLabel15
            // 
            this.radLabel15.Location = new System.Drawing.Point(257, 362);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(56, 18);
            this.radLabel15.TabIndex = 14;
            this.radLabel15.Text = "Страница";
            // 
            // spFromClient
            // 
            this.spFromClient.Location = new System.Drawing.Point(319, 361);
            this.spFromClient.Maximum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            0});
            this.spFromClient.Name = "spFromClient";
            this.spFromClient.Size = new System.Drawing.Size(53, 20);
            this.spFromClient.TabIndex = 19;
            this.spFromClient.TabStop = false;
            this.spFromClient.ValueChanged += new System.EventHandler(this.spFromClient_ValueChanged);
            // 
            // btnPrevClient
            // 
            this.btnPrevClient.Location = new System.Drawing.Point(3, 359);
            this.btnPrevClient.Name = "btnPrevClient";
            this.btnPrevClient.Size = new System.Drawing.Size(110, 24);
            this.btnPrevClient.TabIndex = 18;
            this.btnPrevClient.Text = "<<";
            this.btnPrevClient.Click += new System.EventHandler(this.btnPrevClient_Click);
            // 
            // btnNextClient
            // 
            this.btnNextClient.Location = new System.Drawing.Point(659, 359);
            this.btnNextClient.Name = "btnNextClient";
            this.btnNextClient.Size = new System.Drawing.Size(110, 24);
            this.btnNextClient.TabIndex = 17;
            this.btnNextClient.Text = ">>";
            this.btnNextClient.Click += new System.EventHandler(this.btnNextClient_Click);
            // 
            // GVClients
            // 
            this.GVClients.Location = new System.Drawing.Point(3, 33);
            // 
            // 
            // 
            this.GVClients.MasterTemplate.AllowAddNewRow = false;
            this.GVClients.MasterTemplate.AllowColumnReorder = false;
            gridViewDecimalColumn7.HeaderText = "ID";
            gridViewDecimalColumn7.Name = "column1";
            gridViewDecimalColumn7.Width = 30;
            gridViewTextBoxColumn24.HeaderText = "Фамилия";
            gridViewTextBoxColumn24.Name = "colSurname";
            gridViewTextBoxColumn24.Width = 150;
            gridViewTextBoxColumn25.HeaderText = "Имя";
            gridViewTextBoxColumn25.Name = "colName";
            gridViewTextBoxColumn25.Width = 150;
            gridViewTextBoxColumn26.HeaderText = "Отчество";
            gridViewTextBoxColumn26.Name = "colMiddleName";
            gridViewTextBoxColumn26.Width = 150;
            gridViewTextBoxColumn27.HeaderText = "Телефон";
            gridViewTextBoxColumn27.Name = "colPhone";
            gridViewTextBoxColumn27.Width = 150;
            gridViewTextBoxColumn28.HeaderText = "Дата добавления";
            gridViewTextBoxColumn28.Name = "column2";
            gridViewTextBoxColumn28.Width = 100;
            this.GVClients.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewDecimalColumn7,
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25,
            gridViewTextBoxColumn26,
            gridViewTextBoxColumn27,
            gridViewTextBoxColumn28});
            this.GVClients.MasterTemplate.FilterDescriptors.AddRange(new Telerik.WinControls.Data.FilterDescriptor[] {
            filterDescriptor1,
            filterDescriptor2,
            filterDescriptor3,
            filterDescriptor4,
            filterDescriptor5,
            filterDescriptor6,
            filterDescriptor7,
            filterDescriptor8,
            filterDescriptor9});
            this.GVClients.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor1});
            this.GVClients.MasterTemplate.ViewDefinition = tableViewDefinition6;
            this.GVClients.Name = "GVClients";
            this.GVClients.ReadOnly = true;
            this.GVClients.Size = new System.Drawing.Size(767, 320);
            this.GVClients.TabIndex = 8;
            this.GVClients.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.GVClients_CellDoubleClick);
            // 
            // searchClient
            // 
            this.searchClient.Location = new System.Drawing.Point(119, 3);
            this.searchClient.Name = "searchClient";
            this.searchClient.Size = new System.Drawing.Size(110, 24);
            this.searchClient.TabIndex = 2;
            this.searchClient.Text = "Поиск";
            this.searchClient.Click += new System.EventHandler(this.searchClient_Click);
            // 
            // AddClient
            // 
            this.AddClient.Location = new System.Drawing.Point(3, 3);
            this.AddClient.Name = "AddClient";
            this.AddClient.Size = new System.Drawing.Size(110, 24);
            this.AddClient.TabIndex = 1;
            this.AddClient.Text = "Создать клиента";
            this.AddClient.Click += new System.EventHandler(this.AddClient_Click);
            // 
            // pvCards
            // 
            this.pvCards.Controls.Add(this.spCountCard);
            this.pvCards.Controls.Add(this.radLabel19);
            this.pvCards.Controls.Add(this.spToCard);
            this.pvCards.Controls.Add(this.radLabel20);
            this.pvCards.Controls.Add(this.radLabel21);
            this.pvCards.Controls.Add(this.spFromCard);
            this.pvCards.Controls.Add(this.btnPrevCard);
            this.pvCards.Controls.Add(this.btnNextCard);
            this.pvCards.Controls.Add(this.GVCard);
            this.pvCards.Controls.Add(this.searchCard);
            this.pvCards.Controls.Add(this.addCard);
            this.pvCards.ItemSize = new System.Drawing.SizeF(75F, 29F);
            this.pvCards.Location = new System.Drawing.Point(10, 38);
            this.pvCards.Name = "pvCards";
            this.pvCards.Size = new System.Drawing.Size(773, 412);
            this.pvCards.Text = "Карты";
            // 
            // spCountCard
            // 
            this.spCountCard.Location = new System.Drawing.Point(717, 389);
            this.spCountCard.Name = "spCountCard";
            this.spCountCard.Size = new System.Drawing.Size(53, 20);
            this.spCountCard.TabIndex = 28;
            this.spCountCard.TabStop = false;
            this.spCountCard.ValueChanged += new System.EventHandler(this.spCountCard_ValueChanged);
            // 
            // radLabel19
            // 
            this.radLabel19.Location = new System.Drawing.Point(533, 390);
            this.radLabel19.Name = "radLabel19";
            this.radLabel19.Size = new System.Drawing.Size(178, 18);
            this.radLabel19.TabIndex = 23;
            this.radLabel19.Text = "Количество записей на странице";
            // 
            // spToCard
            // 
            this.spToCard.Enabled = false;
            this.spToCard.Location = new System.Drawing.Point(403, 361);
            this.spToCard.Maximum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            0});
            this.spToCard.Name = "spToCard";
            this.spToCard.Size = new System.Drawing.Size(53, 20);
            this.spToCard.TabIndex = 29;
            this.spToCard.TabStop = false;
            // 
            // radLabel20
            // 
            this.radLabel20.Location = new System.Drawing.Point(379, 362);
            this.radLabel20.Name = "radLabel20";
            this.radLabel20.Size = new System.Drawing.Size(18, 18);
            this.radLabel20.TabIndex = 24;
            this.radLabel20.Text = "из";
            // 
            // radLabel21
            // 
            this.radLabel21.Location = new System.Drawing.Point(258, 362);
            this.radLabel21.Name = "radLabel21";
            this.radLabel21.Size = new System.Drawing.Size(56, 18);
            this.radLabel21.TabIndex = 22;
            this.radLabel21.Text = "Страница";
            // 
            // spFromCard
            // 
            this.spFromCard.Location = new System.Drawing.Point(320, 361);
            this.spFromCard.Maximum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            0});
            this.spFromCard.Name = "spFromCard";
            this.spFromCard.Size = new System.Drawing.Size(53, 20);
            this.spFromCard.TabIndex = 27;
            this.spFromCard.TabStop = false;
            this.spFromCard.ValueChanged += new System.EventHandler(this.spFromCard_ValueChanged);
            // 
            // btnPrevCard
            // 
            this.btnPrevCard.Location = new System.Drawing.Point(4, 359);
            this.btnPrevCard.Name = "btnPrevCard";
            this.btnPrevCard.Size = new System.Drawing.Size(110, 24);
            this.btnPrevCard.TabIndex = 26;
            this.btnPrevCard.Text = "<<";
            this.btnPrevCard.Click += new System.EventHandler(this.btnPrevCard_Click);
            // 
            // btnNextCard
            // 
            this.btnNextCard.Location = new System.Drawing.Point(660, 359);
            this.btnNextCard.Name = "btnNextCard";
            this.btnNextCard.Size = new System.Drawing.Size(110, 24);
            this.btnNextCard.TabIndex = 25;
            this.btnNextCard.Text = ">>";
            this.btnNextCard.Click += new System.EventHandler(this.btnNextCard_Click);
            // 
            // GVCard
            // 
            this.GVCard.Location = new System.Drawing.Point(3, 33);
            // 
            // 
            // 
            this.GVCard.MasterTemplate.AllowColumnReorder = false;
            gridViewDecimalColumn8.HeaderText = "ID";
            gridViewDecimalColumn8.Name = "column2";
            gridViewDecimalColumn8.Width = 30;
            gridViewTextBoxColumn29.HeaderText = "Номер карты";
            gridViewTextBoxColumn29.Name = "cardID";
            gridViewTextBoxColumn29.Width = 100;
            gridViewTextBoxColumn30.HeaderText = "Тариф";
            gridViewTextBoxColumn30.Name = "tariff";
            gridViewTextBoxColumn30.Width = 100;
            gridViewTextBoxColumn31.HeaderText = "ID Клиента";
            gridViewTextBoxColumn31.Name = "columnID";
            gridViewTextBoxColumn31.Width = 65;
            gridViewTextBoxColumn32.HeaderText = "Фамилия";
            gridViewTextBoxColumn32.Name = "surname";
            gridViewTextBoxColumn32.Width = 150;
            gridViewTextBoxColumn33.HeaderText = "Имя";
            gridViewTextBoxColumn33.Name = "name";
            gridViewTextBoxColumn33.Width = 100;
            gridViewTextBoxColumn34.HeaderText = "Отчество";
            gridViewTextBoxColumn34.Name = "middleName";
            gridViewTextBoxColumn34.Width = 100;
            gridViewTextBoxColumn35.HeaderText = "Дата выдачи";
            gridViewTextBoxColumn35.Name = "date1";
            gridViewTextBoxColumn35.Width = 100;
            this.GVCard.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewDecimalColumn8,
            gridViewTextBoxColumn29,
            gridViewTextBoxColumn30,
            gridViewTextBoxColumn31,
            gridViewTextBoxColumn32,
            gridViewTextBoxColumn33,
            gridViewTextBoxColumn34,
            gridViewTextBoxColumn35});
            this.GVCard.MasterTemplate.FilterDescriptors.AddRange(new Telerik.WinControls.Data.FilterDescriptor[] {
            filterDescriptor10,
            filterDescriptor11,
            filterDescriptor12,
            filterDescriptor13,
            filterDescriptor14,
            filterDescriptor15,
            filterDescriptor16,
            filterDescriptor17,
            filterDescriptor18});
            this.GVCard.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor2});
            this.GVCard.MasterTemplate.ViewDefinition = tableViewDefinition7;
            this.GVCard.Name = "GVCard";
            this.GVCard.ReadOnly = true;
            this.GVCard.Size = new System.Drawing.Size(767, 320);
            this.GVCard.TabIndex = 9;
            this.GVCard.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.GVCard_CellDoubleClick);
            // 
            // searchCard
            // 
            this.searchCard.Location = new System.Drawing.Point(119, 3);
            this.searchCard.Name = "searchCard";
            this.searchCard.Size = new System.Drawing.Size(110, 24);
            this.searchCard.TabIndex = 4;
            this.searchCard.Text = "Поиск";
            this.searchCard.Click += new System.EventHandler(this.searchCard_Click);
            // 
            // addCard
            // 
            this.addCard.Location = new System.Drawing.Point(3, 3);
            this.addCard.Name = "addCard";
            this.addCard.Size = new System.Drawing.Size(110, 24);
            this.addCard.TabIndex = 3;
            this.addCard.Text = "Добавить карту";
            this.addCard.Click += new System.EventHandler(this.addCard_Click);
            // 
            // pvTrainers
            // 
            this.pvTrainers.Controls.Add(this.GVTrainers);
            this.pvTrainers.Controls.Add(this.searchWorker);
            this.pvTrainers.Controls.Add(this.addTrainer);
            this.pvTrainers.ItemSize = new System.Drawing.SizeF(88F, 29F);
            this.pvTrainers.Location = new System.Drawing.Point(10, 38);
            this.pvTrainers.Name = "pvTrainers";
            this.pvTrainers.Size = new System.Drawing.Size(773, 412);
            this.pvTrainers.Text = "Тренера";
            // 
            // GVTrainers
            // 
            this.GVTrainers.Location = new System.Drawing.Point(3, 33);
            // 
            // 
            // 
            this.GVTrainers.MasterTemplate.AllowColumnReorder = false;
            gridViewDecimalColumn9.HeaderText = "ID";
            gridViewDecimalColumn9.Name = "column2";
            gridViewDecimalColumn9.Width = 30;
            gridViewTextBoxColumn36.HeaderText = "Фамилия";
            gridViewTextBoxColumn36.Name = "colSurname";
            gridViewTextBoxColumn36.Width = 150;
            gridViewTextBoxColumn37.HeaderText = "Имя";
            gridViewTextBoxColumn37.Name = "colName";
            gridViewTextBoxColumn37.Width = 150;
            gridViewTextBoxColumn38.HeaderText = "Отчество";
            gridViewTextBoxColumn38.Name = "colMiddleName";
            gridViewTextBoxColumn38.Width = 150;
            gridViewTextBoxColumn39.HeaderText = "Телефон";
            gridViewTextBoxColumn39.Name = "colPhone";
            gridViewTextBoxColumn39.Width = 150;
            this.GVTrainers.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewDecimalColumn9,
            gridViewTextBoxColumn36,
            gridViewTextBoxColumn37,
            gridViewTextBoxColumn38,
            gridViewTextBoxColumn39});
            this.GVTrainers.MasterTemplate.FilterDescriptors.AddRange(new Telerik.WinControls.Data.FilterDescriptor[] {
            filterDescriptor19,
            filterDescriptor20,
            filterDescriptor21,
            filterDescriptor22,
            filterDescriptor23,
            filterDescriptor24,
            filterDescriptor25,
            filterDescriptor26,
            filterDescriptor27});
            this.GVTrainers.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor3});
            this.GVTrainers.MasterTemplate.ViewDefinition = tableViewDefinition8;
            this.GVTrainers.Name = "GVTrainers";
            this.GVTrainers.ReadOnly = true;
            this.GVTrainers.Size = new System.Drawing.Size(767, 376);
            this.GVTrainers.TabIndex = 7;
            this.GVTrainers.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.GVTrainers_CellDoubleClick);
            // 
            // searchWorker
            // 
            this.searchWorker.Location = new System.Drawing.Point(119, 3);
            this.searchWorker.Name = "searchWorker";
            this.searchWorker.Size = new System.Drawing.Size(110, 24);
            this.searchWorker.TabIndex = 6;
            this.searchWorker.Text = "Поиск";
            this.searchWorker.Click += new System.EventHandler(this.searchWorker_Click);
            // 
            // addTrainer
            // 
            this.addTrainer.Location = new System.Drawing.Point(3, 3);
            this.addTrainer.Name = "addTrainer";
            this.addTrainer.Size = new System.Drawing.Size(110, 24);
            this.addTrainer.TabIndex = 5;
            this.addTrainer.Text = "Создать работника";
            this.addTrainer.Click += new System.EventHandler(this.addTrainer_Click);
            // 
            // pvServices
            // 
            this.pvServices.Controls.Add(this.GVServices);
            this.pvServices.Controls.Add(this.searchService);
            this.pvServices.Controls.Add(this.addService);
            this.pvServices.ItemSize = new System.Drawing.SizeF(78F, 29F);
            this.pvServices.Location = new System.Drawing.Point(10, 38);
            this.pvServices.Name = "pvServices";
            this.pvServices.Size = new System.Drawing.Size(773, 412);
            this.pvServices.Text = "Услуги";
            // 
            // GVServices
            // 
            this.GVServices.Location = new System.Drawing.Point(3, 33);
            // 
            // 
            // 
            this.GVServices.MasterTemplate.AllowColumnReorder = false;
            gridViewDecimalColumn10.HeaderText = "ID";
            gridViewDecimalColumn10.Name = "column2";
            gridViewDecimalColumn10.Width = 30;
            gridViewTextBoxColumn40.HeaderText = "Название";
            gridViewTextBoxColumn40.Name = "colName";
            gridViewTextBoxColumn40.Width = 400;
            gridViewTextBoxColumn41.HeaderText = "Стоимость";
            gridViewTextBoxColumn41.Name = "colCost";
            gridViewTextBoxColumn41.Width = 100;
            gridViewTextBoxColumn42.HeaderText = "Кол-во человек";
            gridViewTextBoxColumn42.Name = "colPeople";
            gridViewTextBoxColumn42.Width = 100;
            this.GVServices.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewDecimalColumn10,
            gridViewTextBoxColumn40,
            gridViewTextBoxColumn41,
            gridViewTextBoxColumn42});
            this.GVServices.MasterTemplate.FilterDescriptors.AddRange(new Telerik.WinControls.Data.FilterDescriptor[] {
            filterDescriptor28,
            filterDescriptor29,
            filterDescriptor30});
            this.GVServices.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor4});
            this.GVServices.MasterTemplate.ViewDefinition = tableViewDefinition9;
            this.GVServices.Name = "GVServices";
            this.GVServices.ReadOnly = true;
            this.GVServices.Size = new System.Drawing.Size(767, 376);
            this.GVServices.TabIndex = 9;
            this.GVServices.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.GVServices_CellDoubleClick);
            // 
            // searchService
            // 
            this.searchService.Location = new System.Drawing.Point(119, 3);
            this.searchService.Name = "searchService";
            this.searchService.Size = new System.Drawing.Size(110, 24);
            this.searchService.TabIndex = 8;
            this.searchService.Text = "Поиск";
            this.searchService.Click += new System.EventHandler(this.searchService_Click);
            // 
            // addService
            // 
            this.addService.Location = new System.Drawing.Point(3, 3);
            this.addService.Name = "addService";
            this.addService.Size = new System.Drawing.Size(110, 24);
            this.addService.TabIndex = 7;
            this.addService.Text = "Добавить услугу";
            this.addService.Click += new System.EventHandler(this.addService_Click);
            // 
            // pvTariffs
            // 
            this.pvTariffs.Controls.Add(this.GVTariffs);
            this.pvTariffs.Controls.Add(this.searchTariff);
            this.pvTariffs.Controls.Add(this.addTariff);
            this.pvTariffs.ItemSize = new System.Drawing.SizeF(86F, 29F);
            this.pvTariffs.Location = new System.Drawing.Point(10, 38);
            this.pvTariffs.Name = "pvTariffs";
            this.pvTariffs.Size = new System.Drawing.Size(773, 412);
            this.pvTariffs.Text = "Тарифы";
            // 
            // GVTariffs
            // 
            this.GVTariffs.Location = new System.Drawing.Point(3, 33);
            // 
            // 
            // 
            this.GVTariffs.MasterTemplate.AllowAddNewRow = false;
            this.GVTariffs.MasterTemplate.AllowColumnReorder = false;
            gridViewDecimalColumn11.HeaderText = "ID";
            gridViewDecimalColumn11.Name = "column1";
            gridViewDecimalColumn11.Width = 30;
            gridViewTextBoxColumn43.HeaderText = "Название";
            gridViewTextBoxColumn43.Name = "colName";
            gridViewTextBoxColumn43.Width = 150;
            gridViewTextBoxColumn44.HeaderText = "Стоимость";
            gridViewTextBoxColumn44.Name = "colTotalCost";
            gridViewTextBoxColumn44.Width = 150;
            gridViewTextBoxColumn45.HeaderText = "Дата начала продаж";
            gridViewTextBoxColumn45.Name = "colDate1";
            gridViewTextBoxColumn45.Width = 150;
            gridViewTextBoxColumn46.HeaderText = "Дата последней продажи";
            gridViewTextBoxColumn46.Name = "colDate2";
            gridViewTextBoxColumn46.Width = 150;
            gridViewTextBoxColumn47.HeaderText = "Длительность";
            gridViewTextBoxColumn47.Name = "column2";
            gridViewTextBoxColumn47.Width = 100;
            this.GVTariffs.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewDecimalColumn11,
            gridViewTextBoxColumn43,
            gridViewTextBoxColumn44,
            gridViewTextBoxColumn45,
            gridViewTextBoxColumn46,
            gridViewTextBoxColumn47});
            this.GVTariffs.MasterTemplate.FilterDescriptors.AddRange(new Telerik.WinControls.Data.FilterDescriptor[] {
            filterDescriptor31,
            filterDescriptor32,
            filterDescriptor33});
            this.GVTariffs.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor5});
            this.GVTariffs.MasterTemplate.ViewDefinition = tableViewDefinition10;
            this.GVTariffs.Name = "GVTariffs";
            this.GVTariffs.ReadOnly = true;
            this.GVTariffs.Size = new System.Drawing.Size(767, 376);
            this.GVTariffs.TabIndex = 11;
            this.GVTariffs.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.GVTariffs_CellDoubleClick);
            // 
            // searchTariff
            // 
            this.searchTariff.Location = new System.Drawing.Point(119, 3);
            this.searchTariff.Name = "searchTariff";
            this.searchTariff.Size = new System.Drawing.Size(110, 24);
            this.searchTariff.TabIndex = 10;
            this.searchTariff.Text = "Поиск";
            this.searchTariff.Click += new System.EventHandler(this.searchTariff_Click);
            // 
            // addTariff
            // 
            this.addTariff.Location = new System.Drawing.Point(3, 3);
            this.addTariff.Name = "addTariff";
            this.addTariff.Size = new System.Drawing.Size(110, 24);
            this.addTariff.TabIndex = 9;
            this.addTariff.Text = "Создать тариф";
            this.addTariff.Click += new System.EventHandler(this.addTariff_Click);
            // 
            // pvRooms
            // 
            this.pvRooms.Controls.Add(this.searchRoom);
            this.pvRooms.Controls.Add(this.addRoom);
            this.pvRooms.Controls.Add(this.GVRooms);
            this.pvRooms.ItemSize = new System.Drawing.SizeF(109F, 29F);
            this.pvRooms.Location = new System.Drawing.Point(10, 38);
            this.pvRooms.Name = "pvRooms";
            this.pvRooms.Size = new System.Drawing.Size(773, 412);
            this.pvRooms.Text = "Помещения";
            // 
            // searchRoom
            // 
            this.searchRoom.Location = new System.Drawing.Point(119, 3);
            this.searchRoom.Name = "searchRoom";
            this.searchRoom.Size = new System.Drawing.Size(110, 24);
            this.searchRoom.TabIndex = 12;
            this.searchRoom.Text = "Поиск";
            this.searchRoom.Click += new System.EventHandler(this.searchRoom_Click);
            // 
            // addRoom
            // 
            this.addRoom.Location = new System.Drawing.Point(3, 3);
            this.addRoom.Name = "addRoom";
            this.addRoom.Size = new System.Drawing.Size(110, 24);
            this.addRoom.TabIndex = 11;
            this.addRoom.Text = "Новое помещение";
            this.addRoom.Click += new System.EventHandler(this.addRoom_Click);
            // 
            // GVRooms
            // 
            this.GVRooms.Location = new System.Drawing.Point(3, 33);
            // 
            // 
            // 
            this.GVRooms.MasterTemplate.AllowAddNewRow = false;
            gridViewDecimalColumn12.HeaderText = "ID";
            gridViewDecimalColumn12.Name = "column2";
            gridViewDecimalColumn12.Width = 30;
            gridViewTextBoxColumn48.HeaderText = "Название";
            gridViewTextBoxColumn48.Name = "Name";
            gridViewTextBoxColumn48.Width = 150;
            gridViewTextBoxColumn49.HeaderText = "Оборудование";
            gridViewTextBoxColumn49.Name = "equipment";
            gridViewTextBoxColumn49.Width = 468;
            gridViewTextBoxColumn50.HeaderText = "Вместимость";
            gridViewTextBoxColumn50.Name = "capacity";
            gridViewTextBoxColumn50.Width = 100;
            this.GVRooms.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewDecimalColumn12,
            gridViewTextBoxColumn48,
            gridViewTextBoxColumn49,
            gridViewTextBoxColumn50});
            this.GVRooms.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor6});
            this.GVRooms.MasterTemplate.ViewDefinition = tableViewDefinition11;
            this.GVRooms.Name = "GVRooms";
            this.GVRooms.ReadOnly = true;
            this.GVRooms.Size = new System.Drawing.Size(767, 376);
            this.GVRooms.TabIndex = 0;
            this.GVRooms.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.GVRooms_CellDoubleClick);
            // 
            // pvStatistics
            // 
            this.pvStatistics.Controls.Add(this.ChartStatistics);
            this.pvStatistics.Controls.Add(this.radGroupBox8);
            this.pvStatistics.Controls.Add(this.gbDateStatistics);
            this.pvStatistics.Controls.Add(this.gbServiceStatistics);
            this.pvStatistics.ItemSize = new System.Drawing.SizeF(106F, 29F);
            this.pvStatistics.Location = new System.Drawing.Point(10, 38);
            this.pvStatistics.Name = "pvStatistics";
            this.pvStatistics.Size = new System.Drawing.Size(773, 412);
            this.pvStatistics.Text = "Статистика";
            // 
            // ChartStatistics
            // 
            chartArea1.Name = "ChartArea1";
            this.ChartStatistics.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.ChartStatistics.Legends.Add(legend1);
            this.ChartStatistics.Location = new System.Drawing.Point(213, 15);
            this.ChartStatistics.Name = "ChartStatistics";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.ChartStatistics.Series.Add(series1);
            this.ChartStatistics.Size = new System.Drawing.Size(557, 394);
            this.ChartStatistics.TabIndex = 14;
            this.ChartStatistics.Text = "chart1";
            // 
            // radGroupBox8
            // 
            this.radGroupBox8.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox8.Controls.Add(this.rbDateStatistics);
            this.radGroupBox8.Controls.Add(this.rbServiceStatistics);
            this.radGroupBox8.HeaderText = "Посмотреть статистику по";
            this.radGroupBox8.Location = new System.Drawing.Point(3, 3);
            this.radGroupBox8.Name = "radGroupBox8";
            this.radGroupBox8.Size = new System.Drawing.Size(204, 50);
            this.radGroupBox8.TabIndex = 13;
            this.radGroupBox8.Text = "Посмотреть статистику по";
            // 
            // rbDateStatistics
            // 
            this.rbDateStatistics.Location = new System.Drawing.Point(10, 21);
            this.rbDateStatistics.Name = "rbDateStatistics";
            this.rbDateStatistics.Size = new System.Drawing.Size(44, 18);
            this.rbDateStatistics.TabIndex = 11;
            this.rbDateStatistics.Text = "Дате";
            this.rbDateStatistics.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbDateStatistics_ToggleStateChanged);
            // 
            // rbServiceStatistics
            // 
            this.rbServiceStatistics.Location = new System.Drawing.Point(62, 21);
            this.rbServiceStatistics.Name = "rbServiceStatistics";
            this.rbServiceStatistics.Size = new System.Drawing.Size(54, 18);
            this.rbServiceStatistics.TabIndex = 10;
            this.rbServiceStatistics.Text = "Услуге";
            this.rbServiceStatistics.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbServiceStatistics_ToggleStateChanged);
            // 
            // gbDateStatistics
            // 
            this.gbDateStatistics.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.gbDateStatistics.Controls.Add(this.radLabel10);
            this.gbDateStatistics.Controls.Add(this.toStatistics);
            this.gbDateStatistics.Controls.Add(this.fromStatistics);
            this.gbDateStatistics.Controls.Add(this.radLabel11);
            this.gbDateStatistics.HeaderText = "Интервал";
            this.gbDateStatistics.Location = new System.Drawing.Point(3, 56);
            this.gbDateStatistics.Name = "gbDateStatistics";
            this.gbDateStatistics.Size = new System.Drawing.Size(204, 74);
            this.gbDateStatistics.TabIndex = 13;
            this.gbDateStatistics.Text = "Интервал";
            // 
            // radLabel10
            // 
            this.radLabel10.Location = new System.Drawing.Point(10, 46);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(19, 18);
            this.radLabel10.TabIndex = 2;
            this.radLabel10.Text = "по";
            // 
            // toStatistics
            // 
            this.toStatistics.Location = new System.Drawing.Point(35, 44);
            this.toStatistics.Name = "toStatistics";
            this.toStatistics.Size = new System.Drawing.Size(164, 20);
            this.toStatistics.TabIndex = 1;
            this.toStatistics.TabStop = false;
            this.toStatistics.Text = "1 ноября 2018 г.";
            this.toStatistics.Value = new System.DateTime(2018, 11, 1, 13, 25, 2, 718);
            this.toStatistics.ValueChanged += new System.EventHandler(this.toStatistics_ValueChanged);
            // 
            // fromStatistics
            // 
            this.fromStatistics.Location = new System.Drawing.Point(35, 21);
            this.fromStatistics.Name = "fromStatistics";
            this.fromStatistics.Size = new System.Drawing.Size(164, 20);
            this.fromStatistics.TabIndex = 0;
            this.fromStatistics.TabStop = false;
            this.fromStatistics.Text = "1 ноября 2018 г.";
            this.fromStatistics.Value = new System.DateTime(2018, 11, 1, 13, 25, 2, 718);
            this.fromStatistics.ValueChanged += new System.EventHandler(this.fromStatistics_ValueChanged);
            // 
            // radLabel11
            // 
            this.radLabel11.Location = new System.Drawing.Point(10, 22);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(11, 18);
            this.radLabel11.TabIndex = 1;
            this.radLabel11.Text = "с";
            // 
            // gbServiceStatistics
            // 
            this.gbServiceStatistics.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.gbServiceStatistics.Controls.Add(this.cbService);
            this.gbServiceStatistics.HeaderText = "Услуги";
            this.gbServiceStatistics.Location = new System.Drawing.Point(3, 141);
            this.gbServiceStatistics.Name = "gbServiceStatistics";
            this.gbServiceStatistics.Size = new System.Drawing.Size(204, 268);
            this.gbServiceStatistics.TabIndex = 13;
            this.gbServiceStatistics.Text = "Услуги";
            // 
            // cbService
            // 
            this.cbService.CheckOnClick = true;
            this.cbService.FormattingEnabled = true;
            this.cbService.Location = new System.Drawing.Point(10, 21);
            this.cbService.Name = "cbService";
            this.cbService.Size = new System.Drawing.Size(189, 238);
            this.cbService.TabIndex = 12;
            this.cbService.SelectedIndexChanged += new System.EventHandler(this.cbService_SelectedIndexChanged);
            // 
            // radPageViewPage2
            // 
            this.radPageViewPage2.Location = new System.Drawing.Point(205, 4);
            this.radPageViewPage2.Name = "radPageViewPage2";
            this.radPageViewPage2.Size = new System.Drawing.Size(558, 398);
            this.radPageViewPage2.Text = "radPageViewPage2";
            // 
            // MenuOperations
            // 
            this.MenuOperations.Name = "MenuOperations";
            this.MenuOperations.Text = "Операции";
            this.MenuOperations.Click += new System.EventHandler(this.radMenuItemOperations_Click);
            // 
            // MenuSchedule
            // 
            this.MenuSchedule.Name = "MenuSchedule";
            this.MenuSchedule.Text = "Расписание";
            this.MenuSchedule.Click += new System.EventHandler(this.radMenuItemSchedule_Click);
            // 
            // MenuClients
            // 
            this.MenuClients.Name = "MenuClients";
            this.MenuClients.Text = "Клиенты";
            this.MenuClients.Click += new System.EventHandler(this.radMenuItemCLients_Click);
            // 
            // MenuCards
            // 
            this.MenuCards.Name = "MenuCards";
            this.MenuCards.Text = "Карты";
            this.MenuCards.Click += new System.EventHandler(this.radMenuItemCards_Click);
            // 
            // MenuWorkers
            // 
            this.MenuWorkers.Name = "MenuWorkers";
            this.MenuWorkers.Text = "Тренера";
            this.MenuWorkers.Click += new System.EventHandler(this.radMenuItemWorkers_Click);
            // 
            // MenuServices
            // 
            this.MenuServices.Name = "MenuServices";
            this.MenuServices.Text = "Услуги";
            this.MenuServices.Click += new System.EventHandler(this.radMenuItemServices_Click);
            // 
            // MenuTariffs
            // 
            this.MenuTariffs.Name = "MenuTariffs";
            this.MenuTariffs.Text = "Тарифы";
            this.MenuTariffs.Click += new System.EventHandler(this.radMenuItemTariffs_Click);
            // 
            // MenuRooms
            // 
            this.MenuRooms.Name = "MenuRooms";
            this.MenuRooms.Text = "Помещения";
            this.MenuRooms.Click += new System.EventHandler(this.radMenuItemRooms_Click);
            // 
            // MenuPay
            // 
            this.MenuPay.Name = "MenuPay";
            this.MenuPay.Text = "Платежи";
            this.MenuPay.Click += new System.EventHandler(this.radMenuItemPay_Click);
            // 
            // radMenu1
            // 
            this.radMenu1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.MenuOperations,
            this.MenuSchedule,
            this.MenuClients,
            this.MenuCards,
            this.MenuWorkers,
            this.MenuServices,
            this.MenuTariffs,
            this.MenuRooms,
            this.MenuPay,
            this.MenuOptions,
            this.MenuStatistics});
            this.radMenu1.Location = new System.Drawing.Point(0, 0);
            this.radMenu1.Name = "radMenu1";
            this.radMenu1.Size = new System.Drawing.Size(819, 40);
            this.radMenu1.TabIndex = 1;
            // 
            // MenuOptions
            // 
            this.MenuOptions.Name = "MenuOptions";
            this.MenuOptions.Text = "Настройки";
            this.MenuOptions.Click += new System.EventHandler(this.Options_Click);
            // 
            // MenuStatistics
            // 
            this.MenuStatistics.Name = "MenuStatistics";
            this.MenuStatistics.Text = "Статистика";
            this.MenuStatistics.Click += new System.EventHandler(this.radMenuItemStatistics_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(819, 499);
            this.Controls.Add(this.radMenu1);
            this.Controls.Add(this.PageView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Фитнес-центр";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RadForm1_FormClosing);
            this.Load += new System.EventHandler(this.RadForm1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PageView)).EndInit();
            this.PageView.ResumeLayout(false);
            this.radPageViewPageOperation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).EndInit();
            this.radPageView1.ResumeLayout(false);
            this.radPageViewPage1.ResumeLayout(false);
            this.radPageViewPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnGetCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbNCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVReservation.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVReservation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSelectClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lNCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSelectSchedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNameClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddVisit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.radPageViewPage5.ResumeLayout(false);
            this.radPageViewPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnGetCard2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbNCard2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSelectClient2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNameClient2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEndVisit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView4.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView4)).EndInit();
            this.radGridView4.ResumeLayout(false);
            this.radGridView4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVReservation2.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVReservation2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.pvPayment.ResumeLayout(false);
            this.pvPayment.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spCountPayment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spToPayment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spFromPayment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrevPayment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNextPayment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeletePay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAmountPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVPayments.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchPay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addPay)).EndInit();
            this.pvSchedule.ResumeLayout(false);
            this.pvSchedule.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spCountSchedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spToSchedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spFromSchedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrevSchedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNextSchedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbServiceInSchedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.delSchedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalendarSchedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVSchedule.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVSchedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addSchedule)).EndInit();
            this.pvClients.ResumeLayout(false);
            this.pvClients.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spCountClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spToClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spFromClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrevClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNextClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVClients.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVClients)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddClient)).EndInit();
            this.pvCards.ResumeLayout(false);
            this.pvCards.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spCountCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spToCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spFromCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrevCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNextCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVCard.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addCard)).EndInit();
            this.pvTrainers.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVTrainers.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVTrainers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchWorker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addTrainer)).EndInit();
            this.pvServices.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVServices.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVServices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchService)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addService)).EndInit();
            this.pvTariffs.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVTariffs.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVTariffs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchTariff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addTariff)).EndInit();
            this.pvRooms.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchRoom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addRoom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVRooms.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVRooms)).EndInit();
            this.pvStatistics.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChartStatistics)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox8)).EndInit();
            this.radGroupBox8.ResumeLayout(false);
            this.radGroupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbDateStatistics)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbServiceStatistics)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbDateStatistics)).EndInit();
            this.gbDateStatistics.ResumeLayout(false);
            this.gbDateStatistics.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toStatistics)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fromStatistics)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbServiceStatistics)).EndInit();
            this.gbServiceStatistics.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadPageView PageView;
        private Telerik.WinControls.UI.RadPageViewPage pvClients;
        private Telerik.WinControls.UI.RadPageViewPage pvCards;
        private Telerik.WinControls.UI.RadPageViewPage pvTrainers;
        private Telerik.WinControls.UI.RadPageViewPage pvServices;
        private Telerik.WinControls.UI.RadPageViewPage pvTariffs;
        private Telerik.WinControls.UI.RadButton searchClient;
        private Telerik.WinControls.UI.RadButton AddClient;
        private Telerik.WinControls.UI.RadButton searchCard;
        private Telerik.WinControls.UI.RadButton addCard;
        private Telerik.WinControls.UI.RadGridView GVTrainers;
        private Telerik.WinControls.UI.RadButton searchWorker;
        private Telerik.WinControls.UI.RadButton addTrainer;
        private Telerik.WinControls.UI.RadGridView GVServices;
        private Telerik.WinControls.UI.RadButton searchService;
        private Telerik.WinControls.UI.RadButton addService;
        private Telerik.WinControls.UI.RadGridView GVTariffs;
        private Telerik.WinControls.UI.RadButton searchTariff;
        private Telerik.WinControls.UI.RadButton addTariff;
        private Telerik.WinControls.UI.RadGridView GVClients;
        private Telerik.WinControls.UI.RadGridView GVCard;
        private Telerik.WinControls.UI.RadPageViewPage pvSchedule;
        private Telerik.WinControls.UI.RadPageViewPage pvRooms;
        private Telerik.WinControls.UI.RadMenuItem MenuOperations;
        private Telerik.WinControls.UI.RadMenuItem MenuSchedule;
        private Telerik.WinControls.UI.RadMenuItem MenuClients;
        private Telerik.WinControls.UI.RadMenuItem MenuCards;
        private Telerik.WinControls.UI.RadMenuItem MenuWorkers;
        private Telerik.WinControls.UI.RadMenuItem MenuServices;
        private Telerik.WinControls.UI.RadMenuItem MenuTariffs;
        private Telerik.WinControls.UI.RadMenuItem MenuRooms;
        private Telerik.WinControls.UI.RadButton searchRoom;
        private Telerik.WinControls.UI.RadButton addRoom;
        private Telerik.WinControls.UI.RadGridView GVRooms;
        private Telerik.WinControls.UI.RadGridView GVSchedule;
        private Telerik.WinControls.UI.RadButton addSchedule;
        private Telerik.WinControls.UI.RadPageViewPage pvPayment;
        private Telerik.WinControls.UI.RadGridView GVPayments;
        private Telerik.WinControls.UI.RadButton searchPay;
        private Telerik.WinControls.UI.RadButton addPay;
        private Telerik.WinControls.UI.RadMenuItem MenuPay;
        private Telerik.WinControls.UI.RadMenu radMenu1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPageOperation;
        private Telerik.WinControls.UI.RadPageView radPageView1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage1;
        private Telerik.WinControls.UI.RadButton btnSelectClient;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadLabel lNCard;
        private Telerik.WinControls.UI.RadTextBox tbNameClient;
        private Telerik.WinControls.UI.RadButton btnSelectSchedule;
        private Telerik.WinControls.UI.RadButton btnAddVisit;
        private Telerik.WinControls.UI.RadTextBox tbAmount;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage5;
        private Telerik.WinControls.UI.RadCalendar CalendarSchedule;
        private Telerik.WinControls.UI.RadButton btnEndVisit;
        private Telerik.WinControls.UI.RadGridView radGridView4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private Telerik.WinControls.UI.RadGridView GVReservation;
        private Telerik.WinControls.UI.RadPageViewPage pvStatistics;
        private Telerik.WinControls.UI.RadMenuItem MenuStatistics;
        private Telerik.WinControls.UI.RadButton delSchedule;
        private Telerik.WinControls.UI.RadRadioButton rbYear;
        private Telerik.WinControls.UI.RadRadioButton rbDay;
        private Telerik.WinControls.UI.RadRadioButton rbMonth;
        private Telerik.WinControls.UI.RadGroupBox gbServiceStatistics;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox8;
        private Telerik.WinControls.UI.RadRadioButton rbDateStatistics;
        private Telerik.WinControls.UI.RadRadioButton rbServiceStatistics;
        private Telerik.WinControls.UI.RadGroupBox gbDateStatistics;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadDateTimePicker toStatistics;
        private Telerik.WinControls.UI.RadDateTimePicker fromStatistics;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadMenuItem MenuOptions;
        private Telerik.WinControls.UI.RadTextBox tbServiceInSchedule;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadButton btnClear;
        private Telerik.WinControls.UI.RadDropDownList cbNCard;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadButton btnGetCard;
        private Telerik.WinControls.UI.RadButton btnGetCard2;
        private Telerik.WinControls.UI.RadDropDownList cbNCard2;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadButton btnSelectClient2;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox tbNameClient2;
        private Telerik.WinControls.UI.RadGridView GVReservation2;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadTextBox tbAmountPayments;
        private Telerik.WinControls.UI.RadButton btnDeletePay;
        private Telerik.WinControls.UI.RadSpinEditor spToSchedule;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadSpinEditor spFromSchedule;
        private Telerik.WinControls.UI.RadButton btnPrevSchedule;
        private Telerik.WinControls.UI.RadButton btnNextSchedule;
        private Telerik.WinControls.UI.RadSpinEditor spCountSchedule;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadSpinEditor spCountClient;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadSpinEditor spToClient;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.RadSpinEditor spFromClient;
        private Telerik.WinControls.UI.RadButton btnPrevClient;
        private Telerik.WinControls.UI.RadButton btnNextClient;
        private Telerik.WinControls.UI.RadSpinEditor spCountPayment;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        private Telerik.WinControls.UI.RadSpinEditor spToPayment;
        private Telerik.WinControls.UI.RadLabel radLabel17;
        private Telerik.WinControls.UI.RadLabel radLabel18;
        private Telerik.WinControls.UI.RadSpinEditor spFromPayment;
        private Telerik.WinControls.UI.RadButton btnPrevPayment;
        private Telerik.WinControls.UI.RadButton btnNextPayment;
        private Telerik.WinControls.UI.RadSpinEditor spCountCard;
        private Telerik.WinControls.UI.RadSpinEditor spToCard;
        private Telerik.WinControls.UI.RadSpinEditor spFromCard;
        private Telerik.WinControls.UI.RadButton btnPrevCard;
        private Telerik.WinControls.UI.RadButton btnNextCard;
        private Telerik.WinControls.UI.RadLabel radLabel19;
        private Telerik.WinControls.UI.RadLabel radLabel20;
        private Telerik.WinControls.UI.RadLabel radLabel21;
        private System.Windows.Forms.DataVisualization.Charting.Chart ChartStatistics;
        private System.Windows.Forms.CheckedListBox cbService;
    }
}