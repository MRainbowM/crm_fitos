﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace CRM
{
    public partial class SelectRoom : Telerik.WinControls.UI.RadForm
    {
        public SelectRoom()
        {
            InitializeComponent();
        }

        public DateTime From;
        public DateTime To;
        private void SelectRoom_Load(object sender, EventArgs e)
        {
            if (ForSchedule)
            {
                CreateGVRoomForSchedule();
                GVRooms.Columns[4].IsVisible = false;
            }
            else
            {
                CreateGV();
            }
            //search.Visible = false;
        }

        public static List<Room> RoomsInGV = new List<Room>();
        public static List<Room> RoomsList = new List<Room>(); //лист для добавления 
        public static Room SelectOneRoom;
        //public bool ManyRoom = true;

        public bool ForSchedule = false; //истина - если  выбираются для расписания

        private void CreateGVRoomForSchedule()
        {
            GVRooms.Rows.Clear();
            List<ServiceInRoom> Rooms = ServiceInRoom.GetRooms(ScheduleCard.ID_Service);
            List<Room> BusyRoom = ScheduleService.ScheduleServiceBusyRooms(From, To);
            for (int i = 0; i < Rooms.Count; i++)// создание таблицы с rooms
            {
                int k = 0;
                for (int j = 0; j < BusyRoom.Count; j++)
                {
                    if (Rooms[i].ID_Room == BusyRoom[j].id)
                    {
                        k++;
                    }
                }
                if (k == 0)
                {
                    GVRooms.Rows.Add(Rooms[i].ID_Room, Room.FindByID(Rooms[i].ID_Room).name, Room.FindByID(Rooms[i].ID_Room).equipment, Room.FindByID(Rooms[i].ID_Room).capacity);
                }
            }
            if (GVRooms.RowCount == 0)
            {
                DialogResult result = MessageBox.Show(
              "В данное время (" + From + ") нет свободных помещений для услуги: " + Service.FindByID(ScheduleCard.ID_Service).name,
              "Помещений не найдено",
              MessageBoxButtons.OK,
              MessageBoxIcon.Stop,
              MessageBoxDefaultButton.Button1,
              MessageBoxOptions.DefaultDesktopOnly);
                this.Close();
            }
        }

       

        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (ForSchedule)
            {
                SelectOneRoom = new Room();
                int row = GVRooms.CurrentCell.RowIndex;
                int d = Convert.ToInt32(GVRooms.Rows[row].Cells[0].Value);
                SelectOneRoom = Room.FindByID(Convert.ToInt32(d));
            }
            else
            {
                RoomsList.Clear();
                Room room = new Room();
                for (int i = 0; i < GVRooms.RowCount; i++)
                {
                    if (GVRooms.Rows[i].Cells[4].Value != null)
                    {
                        if ((bool)GVRooms.Rows[i].Cells[4].Value)
                        {
                            room = Room.FindByID(Convert.ToInt32(GVRooms.Rows[i].Cells[0].Value));
                            RoomsList.Add(room);
                        }
                    }
                }
            }
            
            //RoomsInGV.Clear();
            ForSchedule = false;
            this.Close();
        }

        private void search_Click(object sender, EventArgs e)
        {
            SearchRoom searchRoom = new SearchRoom();
            searchRoom.ShowDialog();
            List<Room> Rooms = Room.GetAll();
            if (Rooms.Count == MainForm.RoomList.Count)
            {
                CreateGV();
            }
            else
            {
                CreateGVFilter();
            }
        }
        private void CreateGVFilter()
        {
            GVRooms.Rows.Clear();
            List<Room> Rooms = MainForm.RoomList;
            int p = 0;

            for (int i = 0; i < Rooms.Count; i++)// создание таблицы с Комнатами
            {
                for (int x = 0; x < RoomsInGV.Count; x++)
                {
                    if (Rooms[i].id == RoomsInGV[x].id)
                    {
                        p++;
                    }
                }
                if (p == 0)
                {
                    GVRooms.Rows.Add(Rooms[i].id, Rooms[i].name, Rooms[i].equipment, Rooms[i].capacity);
                }
                p = 0;
            }
            MainForm.RoomList = Room.GetAll();
        }

        private void CreateGV()
        {
            GVRooms.Rows.Clear();
            List<Room> Rooms = Room.GetAll();
            int p = 0;

            for (int i = 0; i < Rooms.Count; i++)// создание таблицы с Комнатами
            {
                for (int x = 0; x < RoomsInGV.Count; x++)
                {
                    if (Rooms[i].id == RoomsInGV[x].id)
                    {
                        p++;
                    }
                }
                if (p == 0)
                {
                    GVRooms.Rows.Add(Rooms[i].id, Rooms[i].name, Rooms[i].equipment, Rooms[i].capacity);
                }
                p = 0;
            }
        }

        private void GVRooms_Click(object sender, EventArgs e)
        {
            int row = GVRooms.CurrentCell.RowIndex;
            if (GVRooms.Rows[row].Cells[4].Value != null)
            {
                if ((bool)GVRooms.Rows[row].Cells[4].Value == true)
                {
                    GVRooms.Rows[row].Cells[4].Value = false;
                }
                else
                {
                    GVRooms.Rows[row].Cells[4].Value = true;
                }
            }
            else
            {
                GVRooms.Rows[row].Cells[4].Value = true;
            }
        }
    }
}
