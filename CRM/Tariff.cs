﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using Json;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM
{

    //[Table("Tariffs")]
    public class Tariff
    {
        private int ID;
        private string Name;
        private int Duration;
        private decimal TotalCost;
        private DateTime StartDate;
        private DateTime ExpirationDate;
        private DateTime DateRemoved;
        private string DateDelete;
        private string Comment;
        public string TariffTime { get; set; }
        public List<TimeInTariff> Times = new List<TimeInTariff>();

        public int id
        {
            get { return ID; }
            set { ID = value; }
        }
        public string name
        {
            get { return Name; }
            set { Name = value; }
        }
        public int duration
        {
            get { return Duration; }
            set { Duration = value; }
        }
        public decimal totalCost
        {
            get { return TotalCost; }
            set { TotalCost = value; }
        }
        public DateTime startDate
        {
            get { return StartDate; }
            set { StartDate = value; }
        }
        public DateTime expirationDate
        {
            get { return ExpirationDate; }
            set { ExpirationDate = value; }
        }
        public DateTime dateRemoved
        {
            get { return DateRemoved; }
            set { DateRemoved = value; }
        }
        public string dateDelete
        {
            get { return DateDelete; }
            set { DateDelete = value; }
        }
        public string comment
        {
            get { return Comment; }
            set { Comment = value; }
        }

        public int Add(string Name, int Duration, decimal TotalCost, DateTime StartDate, DateTime ExpirationDate, DateTime DateRemoved, string Comment)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                db.Database.EnsureCreated();
                Tariff tariff = new Tariff()
                {
                    name = Name,
                    duration = Duration,
                    totalCost = TotalCost,
                    startDate = StartDate,
                    expirationDate = ExpirationDate,
                    dateRemoved = DateRemoved,
                    comment = Comment
                };
                db.Tariffs.Add(tariff);
                db.SaveChanges();
                int ID_Tariff = tariff.id;
                return ID_Tariff;
            }
        }
        public void Save(int ID_Tariff, string Name, int Duration, decimal TotalCost, DateTime StartDate, DateTime ExpirationDate, DateTime DateRemoved, string Comment)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                db.Database.EnsureCreated();
                Tariff tariff = db.Tariffs.Where(c => c.id == ID_Tariff).FirstOrDefault();
                tariff.name = Name;
                tariff.duration = Duration;
                tariff.totalCost = TotalCost;
                tariff.startDate = StartDate;
                tariff.expirationDate = ExpirationDate;
                tariff.dateRemoved = DateRemoved;
                tariff.comment = Comment;

                db.SaveChanges();
            }
        }

        public static void SaveTimeInTariff(int ID_Tariff, List<TimeInTariff> Tims)
        {
            string TariffTime1 = "";
            for (int i = 0; i < Tims.Count; i++)
            {
                TariffTime1 = TariffTime1 + Tims[i].WeekDay + '|';
                for (int j = 0; j < Tims[i].Time.Length; j++)
                {
                    TariffTime1 = TariffTime1 + Tims[i].Time[j] + '|';
                }
                TariffTime1 = TariffTime1 + '#';
            }
            using (ApplicationContext db = new ApplicationContext())
            {
                db.Database.EnsureCreated();
                Tariff tariff = db.Tariffs.Where(c => c.id == ID_Tariff).FirstOrDefault();
                tariff.TariffTime = TariffTime1;
                db.SaveChanges();
            }
        }




        public static Tariff FindByID(int ID_Tariff)
        {
            ApplicationContext db = new ApplicationContext();
            Tariff tariff = db.Tariffs.Where(x => x.id == ID_Tariff).FirstOrDefault();

            if (tariff != null)
            {
                if (tariff.TariffTime != null && tariff.TariffTime != "")
                {
                    string[] MasDays = tariff.TariffTime.Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries);

                    for (int j = 0; j < MasDays.Length; j++)
                    {
                        string[] MasTime = MasDays[j].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                        string[] Time = new string[MasTime.Length - 1];
                        for (int x = 1; x < MasTime.Length; x++)
                        {
                            Time[x - 1] = MasTime[x];
                        }
                        string WeekDay = MasTime[0];

                        tariff.Times.Add(new TimeInTariff(WeekDay, Time));
                    }
                }
            }
            return tariff;
        }

        public static List<Tariff> GetAll()
        {
            ApplicationContext db = new ApplicationContext();

           

            List<Tariff> TariffList = db.Tariffs
                    .Where(x => x.dateDelete == null)
                    .Select(x => new Tariff
                    {
                        id = x.id,
                        name = x.Name,
                        duration = x.Duration,
                        totalCost = x.TotalCost,
                        startDate = x.StartDate,
                        expirationDate = x.ExpirationDate,
                        dateRemoved = x.DateRemoved,
                        comment = x.Comment,
                        TariffTime = x.TariffTime
                        //tariffTime = JsonParser.Deserialize<string[]>(x.TariffTime),
                    }
                    ).ToList();

            
            for ( int i = 0; i < TariffList.Count; i++)
            {
                if (TariffList[i].TariffTime != null && TariffList[i].TariffTime != "")
                {
                    string[] MasDays = TariffList[i].TariffTime.Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries);
                    
                    for (int j = 0; j < MasDays.Length; j++)
                    {
                        string[] MasTime = MasDays[j].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                        string[] Time = new string[MasTime.Length - 1];
                        for (int x = 1; x < MasTime.Length; x++)
                        {
                            Time[x - 1] = MasTime[x];
                        }
                        string WeekDay = MasTime[0];

                        TariffList[i].Times.Add(new TimeInTariff(WeekDay, Time));
                    }
                }
            }
            return TariffList;
        }

        public static void Del(int ID)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                db.Database.EnsureCreated();
                Tariff tariff = db.Tariffs.Where(c => c.id == ID).FirstOrDefault();
                tariff.DateDelete = DateTime.Today.ToString();

                db.SaveChanges();
            }
        }

        public static void AddServiceInTariff(int ID_Service, int ID_Tariff, int Amount, int Periodicity) //добавить услуги в тариф
        {
            ServiceInTariff.Add(ID_Tariff, ID_Service, Amount, Periodicity);
        }

        public static bool CheckedTimeUnTariff(DateTime Date, int ID_Tariff)
        {
            string Day = ScheduleCard.Date.ToString("dddd");
            DateTime hour1111 = DateTime.MinValue.AddHours(Date.Hour).AddMinutes(Date.Minute);
            //date1.ToShortTimeString()
            switch (Day.ToLower())
            {
                case "понедельник":
                Day = "ПН";
                    break;
                case "вторник":
                    Day = "ВТ";
                    break;
                case "среда":
                    Day = "СР";
                    break;
                case "четверг":
                    Day = "ЧТ";
                    break;
                case "пятница":
                    Day = "ПТ";
                    break;
                case "суббота":
                    Day = "СБ";
                    break;
                case "воскресенье":
                    Day = "ВС";
                    break;
            }
            Tariff tariff = FindByID(ID_Tariff);
            bool STATE = false;
            for (int i = 0; i < tariff.Times.Count; i++)
            {
                if (tariff.Times[i].WeekDay == Day)
                {
                    for (int j = 0; j < tariff.Times[i].Time.Length; j++)
                    {
                        string[] Hours = tariff.Times[i].Time[j].Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);

                        string[] H1 = Hours[0].Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                        DateTime From = DateTime.MinValue.AddHours(Convert.ToInt32(H1[0])).AddMinutes(Convert.ToInt32(H1[1]));

                        string[] H2 = Hours[1].Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                        DateTime To = DateTime.MinValue.AddHours(Convert.ToInt32(H2[0])).AddMinutes(Convert.ToInt32(H2[1]));

                        if (hour1111 >= From)
                        {
                            if (hour1111 <= To)
                            {
                                STATE = true;
                            }
                        }
                    }
                    break;
                }
            }
            return STATE;
        }


    }

    public class TimeInTariff
    {
        public string WeekDay { get; set; }
        public string[] Time { get; set; }

        public TimeInTariff(string weekDay, string[] time)
        {
            WeekDay = weekDay;
            Time = time;
        }
    }

}
