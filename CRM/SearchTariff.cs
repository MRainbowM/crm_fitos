﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace CRM
{
    public partial class SearchTariff : Telerik.WinControls.UI.RadForm
    {
        public SearchTariff()
        {
            InitializeComponent();
        }

        public static List<Tariff> FilterTariff = new List<Tariff>();

        private void SearchTariff_Load(object sender, EventArgs e)
        {
            grName.Enabled = false;
            grCost.Enabled = false;
            grDate.Enabled = false;
            spFrom.Minimum = 0;
            spFrom.Maximum = 100000;
            spTo.Maximum = 100000;
        }

        private void cbName_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (cbName.Checked)
            {
                grName.Enabled = true;
            }
            else
            {
                grName.Enabled = false;
            }
        }

        private void cbCost_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (cbCost.Checked)
            {
                grCost.Enabled = true;
            }
            else
            {
                grCost.Enabled = false;
            }
        }

        private void cbDate_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (cbDate.Checked)
            {
                grDate.Enabled = true;
                rbStart.Enabled = true;
                rbEnd.Enabled = true;
                dtEndFrom.Enabled = false;
                dtEndTo.Enabled = false;
                dtStartFrom.Enabled = false;
                dtStartTo.Enabled = false;
            }
            else
            {
                grDate.Enabled = false;
               
            }
        }

        private void btSearch_Click(object sender, EventArgs e)
        {
            string NameTariff = tbName.Text;
            decimal From = spFrom.Value;
            decimal To = spTo.Value;



            int p = -1;

            MainForm.TariffList = Tariff.GetAll();
            FilterTariff.Clear();
            if (cbName.Checked && cbCost.Checked == false && cbDate.Checked == false)
            {
                for (int i = 0; i < MainForm.TariffList.Count; i++)
                {
                    p = MainForm.TariffList[i].name.ToLower().IndexOf(NameTariff.ToLower());
                    if (p > -1)
                    {
                        FilterTariff.Add(MainForm.TariffList[i]);
                    }
                    p = -1;
                }
            }
            if (cbName.Checked && cbCost.Checked && cbDate.Checked == false)
            {
                for (int i = 0; i < MainForm.TariffList.Count; i++)
                {
                    p = MainForm.TariffList[i].name.ToLower().IndexOf(NameTariff.ToLower());
                    if (p > -1 && MainForm.TariffList[i].totalCost >= From)
                    {
                        if (MainForm.TariffList[i].totalCost <= To)
                        {
                            FilterTariff.Add(MainForm.TariffList[i]);

                        }
                    }
                    p = -1;
                }
            }
            if (cbName.Checked && cbCost.Checked && cbDate.Checked)
            {
                if (rbStart.IsChecked)
                {
                    for (int i = 0; i < MainForm.TariffList.Count; i++)
                    {
                        p = MainForm.TariffList[i].name.ToLower().IndexOf(NameTariff.ToLower());
                        if (p > -1 && MainForm.TariffList[i].totalCost >= From)
                        {
                            if (MainForm.TariffList[i].totalCost <= To)
                            {
                                if (MainForm.TariffList[i].startDate >= dtStartFrom.Value)
                                {
                                    if (MainForm.TariffList[i].startDate <= dtStartTo.Value)
                                    {
                                        FilterTariff.Add(MainForm.TariffList[i]);
                                    }
                                }
                            }
                        }
                        p = -1;
                    }
                }
                else if (rbEnd.IsChecked)
                {
                    for (int i = 0; i < MainForm.TariffList.Count; i++)
                    {
                        p = MainForm.TariffList[i].name.ToLower().IndexOf(NameTariff.ToLower());
                        if (p > -1 && MainForm.TariffList[i].totalCost >= From)
                        {
                            if (MainForm.TariffList[i].totalCost <= To)
                            {
                                if (MainForm.TariffList[i].expirationDate >= dtEndFrom.Value)
                                {
                                    if (MainForm.TariffList[i].expirationDate <= dtEndTo.Value)
                                    {
                                        FilterTariff.Add(MainForm.TariffList[i]);
                                    }
                                }
                            }
                        }
                        p = -1;
                    }
                }
                
            }
            if (cbName.Checked == false && cbCost.Checked  && cbDate.Checked == false)
            {
                for (int i = 0; i < MainForm.TariffList.Count; i++)
                {
                    if (MainForm.TariffList[i].totalCost >= From)
                    {
                        if (MainForm.TariffList[i].totalCost <= To)
                        {
                            FilterTariff.Add(MainForm.TariffList[i]);

                        }
                    }
                }
            }
            if (cbName.Checked == false && cbCost.Checked == false && cbDate.Checked)
            {
                if (rbStart.IsChecked)
                {
                    for (int i = 0; i < MainForm.TariffList.Count; i++)
                    {
                        if (MainForm.TariffList[i].startDate >= dtStartFrom.Value)
                        {
                            if (MainForm.TariffList[i].startDate <= dtStartTo.Value)
                            {
                                FilterTariff.Add(MainForm.TariffList[i]);
                            }
                        }
                    }
                }
                else if (rbEnd.IsChecked)
                {
                    for (int i = 0; i < MainForm.TariffList.Count; i++)
                    {
                        if (MainForm.TariffList[i].expirationDate >= dtEndFrom.Value)
                        {
                            if (MainForm.TariffList[i].expirationDate <= dtEndTo.Value)
                            {
                                FilterTariff.Add(MainForm.TariffList[i]);
                            }
                        }
                    }
                }
            }
            if (cbName.Checked && cbCost.Checked == false && cbDate.Checked)
            {
                if (rbStart.IsChecked)
                {
                    for (int i = 0; i < MainForm.TariffList.Count; i++)
                    {
                        p = MainForm.TariffList[i].name.ToLower().IndexOf(NameTariff.ToLower());
                        if (MainForm.TariffList[i].startDate >= dtStartFrom.Value)
                        {
                            if (MainForm.TariffList[i].startDate <= dtStartTo.Value)
                            {
                                FilterTariff.Add(MainForm.TariffList[i]);
                            }
                        }
                        p = -1;
                    }
                }
                else if (rbEnd.IsChecked)
                {
                    for (int i = 0; i < MainForm.TariffList.Count; i++)
                    {
                        p = MainForm.TariffList[i].name.ToLower().IndexOf(NameTariff.ToLower());
                        if (MainForm.TariffList[i].expirationDate >= dtEndFrom.Value)
                        {
                            if (MainForm.TariffList[i].expirationDate <= dtEndTo.Value)
                            {
                                FilterTariff.Add(MainForm.TariffList[i]);
                            }
                        }
                        p = -1;
                    }
                }

            }
            if (FilterTariff.Count != 0)
            {
                MainForm.TariffList.Clear();
                for (int i = 0; i < FilterTariff.Count; i++)
                {
                    MainForm.TariffList.Add(FilterTariff[i]);
                }
            }
            else
            {
                MessageBox.Show(
                "Совпадений не найдено",
                "Результат поиска",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information,
                MessageBoxDefaultButton.Button1,
                MessageBoxOptions.DefaultDesktopOnly);
                MainForm.TariffList = Tariff.GetAll();
            }
            this.Close();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            tbName.Text = "";
            spFrom.Value = 0;
            spTo.Value = 0;
            dtStartFrom.Value = DateTime.MinValue;
            dtStartTo.Value = DateTime.MinValue;
            dtEndFrom.Value = DateTime.MinValue;
            dtEndTo.Value = DateTime.MinValue;
        }

        private void rbStart_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (rbStart.IsChecked)
            {
                dtStartFrom.Enabled = true;
                dtStartTo.Enabled = true;
            }
            else
            {
                dtStartFrom.Enabled = false;
                dtStartTo.Enabled = false;
            }
        }

        private void rbEnd_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (rbEnd.IsChecked)
            {
                dtEndFrom.Enabled = true;
                dtEndTo.Enabled = true;
            }
            else
            {
                dtEndFrom.Enabled = false;
                dtEndTo.Enabled = false;
            }
        }

        private void spFrom_ValueChanged(object sender, EventArgs e)
        {
            spTo.Minimum = spFrom.Value;
        }
    }
}
