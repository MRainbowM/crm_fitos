﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM
{
    public class Payment
    {
        public int ID { get; set; }
        //public int ID_Card { get; set; }
        public DateTime DateOfCreation { get; set; }
        public DateTime DateOfPayment { get; set; }
        public decimal Amount { get; set; }
        public decimal Paid { get; set; }
        public int ID_Reservation { get; set; }
        public int ID_User { get; set; }
        public string DateDelete { get; set; }

        public static int AddDebt(int ID_User, DateTime DateOfCreation, decimal Amount, int ID_Reservation)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                db.Database.EnsureCreated();
                Payment Payment = new Payment
                {
                    ID_User = ID_User,
                    DateOfCreation = DateOfCreation,
                    Amount = Amount,
                    ID_Reservation = ID_Reservation


                };
                db.Payments.Add(Payment);
                db.SaveChanges();
                int ID_Pay = Payment.ID;
                return ID_Pay;
            }
        }

        public static void AddPay(DateTime DateOfPayment, int ID_Payment)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                db.Database.EnsureCreated();
                Payment Payment = db.Payments.Where(c => c.ID == ID_Payment).FirstOrDefault();
                Payment.DateOfPayment = DateOfPayment;
                Payment.Paid = Payment.Amount;
                //Payment.

                db.SaveChanges();
            }

        }

        public static void DelDebt(int ID_Payment)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                db.Database.EnsureCreated();
                Payment Payment = db.Payments.Where(c => c.ID == ID_Payment).FirstOrDefault();
                Payment.DateDelete = DateTime.Today.ToString();

                db.SaveChanges();
            }
        }

        public static void DelPay(int ID_Payment) 
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                db.Database.EnsureCreated();
                Payment Payment = db.Payments.Where(c => c.ID == ID_Payment).FirstOrDefault();
                Payment.DateOfPayment = DateTime.MinValue;
                Payment.Paid = 0;
                //Payment.

                db.SaveChanges();
            }
        }

        public static Payment FindByIDReservationAndIDClient(int ID_Reservation, int ID_User)
        {
            ApplicationContext db = new ApplicationContext();
            Payment Payment = db.Payments.Where(x => x.DateDelete == null && x.ID_Reservation == ID_Reservation && x.ID_User == ID_User).FirstOrDefault();
            return Payment;
        }

        public static Payment FindByID(int ID_Payment)
        {
            ApplicationContext db = new ApplicationContext();
            Payment Payment = db.Payments.Find(ID_Payment);
            return Payment;
        }

        public static List<Payment> FindByIDClient(int ID_User)
        {
            ApplicationContext db = new ApplicationContext();

            List<Payment> PaymentList = db.Payments
                    .Where(x => x.DateDelete == null && x.ID_User == ID_User)
                    .Select(x => new Payment
                    {
                        ID = x.ID,
                        DateOfCreation = x.DateOfCreation,
                        DateOfPayment = x.DateOfPayment,
                        Amount = x.Amount,
                        Paid = x.Paid,
                        ID_Reservation = x.ID_Reservation,
                        ID_User = x.ID_User
                    }
                    ).ToList();
            return PaymentList;
        }

        public static List<Payment> GetAll()
        {
            ApplicationContext db = new ApplicationContext();

            List<Payment> PaymentList = db.Payments
                    //.Where(x => x.DateDelete == null)
                    .Select(x => new Payment
                    {
                        ID = x.ID,
                        DateOfCreation = x.DateOfCreation,
                        DateOfPayment = x.DateOfPayment,
                        Amount = x.Amount,
                        Paid = x.Paid,
                        ID_Reservation = x.ID_Reservation,
                        ID_User = x.ID_User,
                        DateDelete = x.DateDelete
                    }
                    ).ToList();
            return PaymentList;
        }
    }
}
