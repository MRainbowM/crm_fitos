﻿namespace CRM
{
    partial class SearchTariff
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grDate = new Telerik.WinControls.UI.RadGroupBox();
            this.dtEndTo = new Telerik.WinControls.UI.RadDateTimePicker();
            this.dtStartTo = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.dtEndFrom = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.rbEnd = new Telerik.WinControls.UI.RadRadioButton();
            this.rbStart = new Telerik.WinControls.UI.RadRadioButton();
            this.dtStartFrom = new Telerik.WinControls.UI.RadDateTimePicker();
            this.grCost = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.spTo = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.spFrom = new Telerik.WinControls.UI.RadSpinEditor();
            this.cbCost = new Telerik.WinControls.UI.RadCheckBox();
            this.grName = new Telerik.WinControls.UI.RadGroupBox();
            this.tbName = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.cbName = new Telerik.WinControls.UI.RadCheckBox();
            this.btSearch = new Telerik.WinControls.UI.RadButton();
            this.btnClear = new Telerik.WinControls.UI.RadButton();
            this.cbDate = new Telerik.WinControls.UI.RadCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.grDate)).BeginInit();
            this.grDate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStartTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStartFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grCost)).BeginInit();
            this.grCost.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grName)).BeginInit();
            this.grName.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // grDate
            // 
            this.grDate.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.grDate.Controls.Add(this.dtEndTo);
            this.grDate.Controls.Add(this.dtStartTo);
            this.grDate.Controls.Add(this.radLabel6);
            this.grDate.Controls.Add(this.radLabel5);
            this.grDate.Controls.Add(this.radLabel7);
            this.grDate.Controls.Add(this.dtEndFrom);
            this.grDate.Controls.Add(this.radLabel4);
            this.grDate.Controls.Add(this.rbEnd);
            this.grDate.Controls.Add(this.rbStart);
            this.grDate.Controls.Add(this.dtStartFrom);
            this.grDate.HeaderText = "По дате";
            this.grDate.Location = new System.Drawing.Point(12, 96);
            this.grDate.Name = "grDate";
            this.grDate.Size = new System.Drawing.Size(368, 154);
            this.grDate.TabIndex = 12;
            this.grDate.Text = "По дате";
            // 
            // dtEndTo
            // 
            this.dtEndTo.Location = new System.Drawing.Point(192, 119);
            this.dtEndTo.Name = "dtEndTo";
            this.dtEndTo.Size = new System.Drawing.Size(164, 20);
            this.dtEndTo.TabIndex = 8;
            this.dtEndTo.TabStop = false;
            this.dtEndTo.Text = "30 октября 2018 г.";
            this.dtEndTo.Value = new System.DateTime(2018, 10, 30, 14, 13, 13, 348);
            // 
            // dtStartTo
            // 
            this.dtStartTo.Location = new System.Drawing.Point(5, 119);
            this.dtStartTo.Name = "dtStartTo";
            this.dtStartTo.Size = new System.Drawing.Size(164, 20);
            this.dtStartTo.TabIndex = 1;
            this.dtStartTo.TabStop = false;
            this.dtStartTo.Text = "30 октября 2018 г.";
            this.dtStartTo.Value = new System.DateTime(2018, 10, 30, 14, 13, 13, 348);
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(192, 95);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(21, 18);
            this.radLabel6.TabIndex = 10;
            this.radLabel6.Text = "До";
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(5, 95);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(21, 18);
            this.radLabel5.TabIndex = 6;
            this.radLabel5.Text = "До";
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(192, 45);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(19, 18);
            this.radLabel7.TabIndex = 9;
            this.radLabel7.Text = "От";
            // 
            // dtEndFrom
            // 
            this.dtEndFrom.Location = new System.Drawing.Point(192, 69);
            this.dtEndFrom.Name = "dtEndFrom";
            this.dtEndFrom.Size = new System.Drawing.Size(164, 20);
            this.dtEndFrom.TabIndex = 7;
            this.dtEndFrom.TabStop = false;
            this.dtEndFrom.Text = "30 октября 2018 г.";
            this.dtEndFrom.Value = new System.DateTime(2018, 10, 30, 14, 13, 13, 348);
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(5, 45);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(19, 18);
            this.radLabel4.TabIndex = 5;
            this.radLabel4.Text = "От";
            // 
            // rbEnd
            // 
            this.rbEnd.Location = new System.Drawing.Point(192, 21);
            this.rbEnd.Name = "rbEnd";
            this.rbEnd.Size = new System.Drawing.Size(147, 18);
            this.rbEnd.TabIndex = 14;
            this.rbEnd.Text = "Дата окончания продаж";
            this.rbEnd.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbEnd_ToggleStateChanged);
            // 
            // rbStart
            // 
            this.rbStart.Location = new System.Drawing.Point(5, 21);
            this.rbStart.Name = "rbStart";
            this.rbStart.Size = new System.Drawing.Size(127, 18);
            this.rbStart.TabIndex = 13;
            this.rbStart.Text = "Дата начала продаж";
            this.rbStart.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbStart_ToggleStateChanged);
            // 
            // dtStartFrom
            // 
            this.dtStartFrom.Location = new System.Drawing.Point(5, 69);
            this.dtStartFrom.Name = "dtStartFrom";
            this.dtStartFrom.Size = new System.Drawing.Size(164, 20);
            this.dtStartFrom.TabIndex = 0;
            this.dtStartFrom.TabStop = false;
            this.dtStartFrom.Text = "30 октября 2018 г.";
            this.dtStartFrom.Value = new System.DateTime(2018, 10, 30, 14, 13, 13, 348);
            // 
            // grCost
            // 
            this.grCost.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.grCost.Controls.Add(this.radLabel1);
            this.grCost.Controls.Add(this.spTo);
            this.grCost.Controls.Add(this.radLabel3);
            this.grCost.Controls.Add(this.spFrom);
            this.grCost.HeaderText = "Стоимость";
            this.grCost.Location = new System.Drawing.Point(199, 12);
            this.grCost.Name = "grCost";
            this.grCost.Size = new System.Drawing.Size(181, 78);
            this.grCost.TabIndex = 13;
            this.grCost.Text = "Стоимость";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(106, 21);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(21, 18);
            this.radLabel1.TabIndex = 7;
            this.radLabel1.Text = "До";
            // 
            // spTo
            // 
            this.spTo.Location = new System.Drawing.Point(106, 45);
            this.spTo.Name = "spTo";
            this.spTo.Size = new System.Drawing.Size(63, 20);
            this.spTo.TabIndex = 8;
            this.spTo.TabStop = false;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(5, 21);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(19, 18);
            this.radLabel3.TabIndex = 6;
            this.radLabel3.Text = "От";
            // 
            // spFrom
            // 
            this.spFrom.Location = new System.Drawing.Point(5, 46);
            this.spFrom.Name = "spFrom";
            this.spFrom.Size = new System.Drawing.Size(63, 20);
            this.spFrom.TabIndex = 7;
            this.spFrom.TabStop = false;
            this.spFrom.ValueChanged += new System.EventHandler(this.spFrom_ValueChanged);
            // 
            // cbCost
            // 
            this.cbCost.Location = new System.Drawing.Point(204, 9);
            this.cbCost.Name = "cbCost";
            this.cbCost.Size = new System.Drawing.Size(116, 18);
            this.cbCost.TabIndex = 18;
            this.cbCost.Text = "Стоимость тарифа";
            this.cbCost.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.cbCost_ToggleStateChanged);
            // 
            // grName
            // 
            this.grName.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.grName.Controls.Add(this.tbName);
            this.grName.Controls.Add(this.radLabel2);
            this.grName.HeaderText = "Название тарифа";
            this.grName.Location = new System.Drawing.Point(12, 12);
            this.grName.Name = "grName";
            this.grName.Size = new System.Drawing.Size(181, 78);
            this.grName.TabIndex = 14;
            this.grName.Text = "Название тарифа";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(5, 46);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(164, 20);
            this.tbName.TabIndex = 8;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(5, 21);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(56, 18);
            this.radLabel2.TabIndex = 7;
            this.radLabel2.Text = "Название";
            // 
            // cbName
            // 
            this.cbName.Location = new System.Drawing.Point(20, 9);
            this.cbName.Name = "cbName";
            this.cbName.Size = new System.Drawing.Size(110, 18);
            this.cbName.TabIndex = 17;
            this.cbName.Text = "Название тарифа";
            this.cbName.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.cbName_ToggleStateChanged);
            // 
            // btSearch
            // 
            this.btSearch.Location = new System.Drawing.Point(270, 256);
            this.btSearch.Name = "btSearch";
            this.btSearch.Size = new System.Drawing.Size(110, 24);
            this.btSearch.TabIndex = 15;
            this.btSearch.Text = "Поиск";
            this.btSearch.Click += new System.EventHandler(this.btSearch_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(12, 256);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(110, 24);
            this.btnClear.TabIndex = 16;
            this.btnClear.Text = "Очистить";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // cbDate
            // 
            this.cbDate.Location = new System.Drawing.Point(20, 93);
            this.cbDate.Name = "cbDate";
            this.cbDate.Size = new System.Drawing.Size(46, 18);
            this.cbDate.TabIndex = 18;
            this.cbDate.Text = "Даты";
            this.cbDate.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.cbDate_ToggleStateChanged);
            // 
            // SearchTariff
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(390, 290);
            this.Controls.Add(this.cbCost);
            this.Controls.Add(this.cbName);
            this.Controls.Add(this.cbDate);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btSearch);
            this.Controls.Add(this.grName);
            this.Controls.Add(this.grCost);
            this.Controls.Add(this.grDate);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "SearchTariff";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Поиск тарифа";
            this.Load += new System.EventHandler(this.SearchTariff_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grDate)).EndInit();
            this.grDate.ResumeLayout(false);
            this.grDate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStartTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStartFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grCost)).EndInit();
            this.grCost.ResumeLayout(false);
            this.grCost.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grName)).EndInit();
            this.grName.ResumeLayout(false);
            this.grName.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox grDate;
        private Telerik.WinControls.UI.RadDateTimePicker dtEndTo;
        private Telerik.WinControls.UI.RadDateTimePicker dtStartTo;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadDateTimePicker dtEndFrom;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadRadioButton rbEnd;
        private Telerik.WinControls.UI.RadRadioButton rbStart;
        private Telerik.WinControls.UI.RadDateTimePicker dtStartFrom;
        private Telerik.WinControls.UI.RadGroupBox grCost;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadSpinEditor spTo;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadSpinEditor spFrom;
        private Telerik.WinControls.UI.RadGroupBox grName;
        private Telerik.WinControls.UI.RadTextBox tbName;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadButton btSearch;
        private Telerik.WinControls.UI.RadButton btnClear;
        private Telerik.WinControls.UI.RadCheckBox cbCost;
        private Telerik.WinControls.UI.RadCheckBox cbName;
        private Telerik.WinControls.UI.RadCheckBox cbDate;
    }
}
