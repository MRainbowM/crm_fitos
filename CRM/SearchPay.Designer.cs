﻿namespace CRM
{
    partial class SearchPay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grDate = new Telerik.WinControls.UI.RadGroupBox();
            this.dtTo = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.dtFrom = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.tbService = new Telerik.WinControls.UI.RadTextBox();
            this.radGroupBox3 = new Telerik.WinControls.UI.RadGroupBox();
            this.spTo = new Telerik.WinControls.UI.RadSpinEditor();
            this.spFrom = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radGroupBox4 = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.tbSurname = new Telerik.WinControls.UI.RadTextBox();
            this.tbMiddleName = new Telerik.WinControls.UI.RadTextBox();
            this.tbName = new Telerik.WinControls.UI.RadTextBox();
            this.btnSearch = new Telerik.WinControls.UI.RadButton();
            this.btnClear = new Telerik.WinControls.UI.RadButton();
            this.cbDate = new Telerik.WinControls.UI.RadCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.grDate)).BeginInit();
            this.grDate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbService)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).BeginInit();
            this.radGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox4)).BeginInit();
            this.radGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSurname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMiddleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // grDate
            // 
            this.grDate.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.grDate.Controls.Add(this.dtTo);
            this.grDate.Controls.Add(this.radLabel5);
            this.grDate.Controls.Add(this.radLabel4);
            this.grDate.Controls.Add(this.dtFrom);
            this.grDate.HeaderText = "Дата платежа";
            this.grDate.Location = new System.Drawing.Point(13, 13);
            this.grDate.Name = "grDate";
            this.grDate.Size = new System.Drawing.Size(176, 81);
            this.grDate.TabIndex = 0;
            this.grDate.Text = "Дата платежа";
            // 
            // dtTo
            // 
            this.dtTo.Location = new System.Drawing.Point(30, 46);
            this.dtTo.Name = "dtTo";
            this.dtTo.Size = new System.Drawing.Size(119, 20);
            this.dtTo.TabIndex = 8;
            this.dtTo.TabStop = false;
            this.dtTo.Text = "30 октября 2018 г.";
            this.dtTo.Value = new System.DateTime(2018, 10, 30, 14, 13, 13, 348);
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(5, 47);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(21, 18);
            this.radLabel5.TabIndex = 10;
            this.radLabel5.Text = "До";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(5, 21);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(19, 18);
            this.radLabel4.TabIndex = 9;
            this.radLabel4.Text = "От";
            // 
            // dtFrom
            // 
            this.dtFrom.Location = new System.Drawing.Point(30, 20);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Size = new System.Drawing.Size(119, 20);
            this.dtFrom.TabIndex = 7;
            this.dtFrom.TabStop = false;
            this.dtFrom.Text = "30 октября 2018 г.";
            this.dtFrom.Value = new System.DateTime(2018, 10, 30, 14, 13, 13, 348);
            this.dtFrom.ValueChanged += new System.EventHandler(this.dtFrom_ValueChanged);
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Controls.Add(this.radLabel3);
            this.radGroupBox2.Controls.Add(this.tbService);
            this.radGroupBox2.HeaderText = "Поиск по услуге";
            this.radGroupBox2.Location = new System.Drawing.Point(13, 100);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(351, 56);
            this.radGroupBox2.TabIndex = 11;
            this.radGroupBox2.Text = "Поиск по услуге";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(5, 21);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(56, 18);
            this.radLabel3.TabIndex = 11;
            this.radLabel3.Text = "Название";
            // 
            // tbService
            // 
            this.tbService.Location = new System.Drawing.Point(67, 21);
            this.tbService.Name = "tbService";
            this.tbService.Size = new System.Drawing.Size(264, 20);
            this.tbService.TabIndex = 0;
            // 
            // radGroupBox3
            // 
            this.radGroupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox3.Controls.Add(this.spTo);
            this.radGroupBox3.Controls.Add(this.spFrom);
            this.radGroupBox3.Controls.Add(this.radLabel1);
            this.radGroupBox3.Controls.Add(this.radLabel2);
            this.radGroupBox3.HeaderText = "Сумма платежа";
            this.radGroupBox3.Location = new System.Drawing.Point(195, 13);
            this.radGroupBox3.Name = "radGroupBox3";
            this.radGroupBox3.Size = new System.Drawing.Size(169, 81);
            this.radGroupBox3.TabIndex = 11;
            this.radGroupBox3.Text = "Сумма платежа";
            // 
            // spTo
            // 
            this.spTo.Location = new System.Drawing.Point(30, 46);
            this.spTo.Name = "spTo";
            this.spTo.Size = new System.Drawing.Size(119, 20);
            this.spTo.TabIndex = 13;
            this.spTo.TabStop = false;
            // 
            // spFrom
            // 
            this.spFrom.Location = new System.Drawing.Point(30, 20);
            this.spFrom.Name = "spFrom";
            this.spFrom.Size = new System.Drawing.Size(119, 20);
            this.spFrom.TabIndex = 12;
            this.spFrom.TabStop = false;
            this.spFrom.ValueChanged += new System.EventHandler(this.spFrom_ValueChanged);
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(5, 47);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(21, 18);
            this.radLabel1.TabIndex = 10;
            this.radLabel1.Text = "До";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(5, 21);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(19, 18);
            this.radLabel2.TabIndex = 9;
            this.radLabel2.Text = "От";
            // 
            // radGroupBox4
            // 
            this.radGroupBox4.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox4.Controls.Add(this.radLabel7);
            this.radGroupBox4.Controls.Add(this.radLabel8);
            this.radGroupBox4.Controls.Add(this.radLabel6);
            this.radGroupBox4.Controls.Add(this.tbSurname);
            this.radGroupBox4.Controls.Add(this.tbMiddleName);
            this.radGroupBox4.Controls.Add(this.tbName);
            this.radGroupBox4.HeaderText = "Поиск по клиенту";
            this.radGroupBox4.Location = new System.Drawing.Point(13, 162);
            this.radGroupBox4.Name = "radGroupBox4";
            this.radGroupBox4.Size = new System.Drawing.Size(351, 108);
            this.radGroupBox4.TabIndex = 12;
            this.radGroupBox4.Text = "Поиск по клиенту";
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(5, 48);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(28, 18);
            this.radLabel7.TabIndex = 13;
            this.radLabel7.Text = "Имя";
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(5, 74);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(54, 18);
            this.radLabel8.TabIndex = 13;
            this.radLabel8.Text = "Отчество";
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(5, 22);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(53, 18);
            this.radLabel6.TabIndex = 12;
            this.radLabel6.Text = "Фамилия";
            // 
            // tbSurname
            // 
            this.tbSurname.Location = new System.Drawing.Point(67, 21);
            this.tbSurname.Name = "tbSurname";
            this.tbSurname.Size = new System.Drawing.Size(264, 20);
            this.tbSurname.TabIndex = 1;
            // 
            // tbMiddleName
            // 
            this.tbMiddleName.Location = new System.Drawing.Point(67, 73);
            this.tbMiddleName.Name = "tbMiddleName";
            this.tbMiddleName.Size = new System.Drawing.Size(264, 20);
            this.tbMiddleName.TabIndex = 1;
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(67, 47);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(264, 20);
            this.tbName.TabIndex = 1;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(244, 276);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(120, 24);
            this.btnSearch.TabIndex = 1;
            this.btnSearch.Text = "Поиск";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(18, 276);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(120, 24);
            this.btnClear.TabIndex = 14;
            this.btnClear.Text = "Очистить";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // cbDate
            // 
            this.cbDate.Location = new System.Drawing.Point(18, 9);
            this.cbDate.Name = "cbDate";
            this.cbDate.Size = new System.Drawing.Size(90, 18);
            this.cbDate.TabIndex = 15;
            this.cbDate.Text = "Дата платежа";
            this.cbDate.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.cbDate_ToggleStateChanged);
            // 
            // SearchPay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(378, 315);
            this.Controls.Add(this.cbDate);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.radGroupBox4);
            this.Controls.Add(this.radGroupBox3);
            this.Controls.Add(this.radGroupBox2);
            this.Controls.Add(this.grDate);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "SearchPay";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Поиск платежа";
            this.Load += new System.EventHandler(this.SearchPay_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grDate)).EndInit();
            this.grDate.ResumeLayout(false);
            this.grDate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbService)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).EndInit();
            this.radGroupBox3.ResumeLayout(false);
            this.radGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox4)).EndInit();
            this.radGroupBox4.ResumeLayout(false);
            this.radGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSurname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMiddleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox grDate;
        private Telerik.WinControls.UI.RadDateTimePicker dtTo;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadDateTimePicker dtFrom;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox3;
        private Telerik.WinControls.UI.RadSpinEditor spTo;
        private Telerik.WinControls.UI.RadSpinEditor spFrom;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox4;
        private Telerik.WinControls.UI.RadButton btnSearch;
        private Telerik.WinControls.UI.RadButton btnClear;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTextBox tbService;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadTextBox tbSurname;
        private Telerik.WinControls.UI.RadTextBox tbMiddleName;
        private Telerik.WinControls.UI.RadTextBox tbName;
        private Telerik.WinControls.UI.RadCheckBox cbDate;
    }
}
