﻿namespace CRM
{
    partial class SelectSchedule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn1 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.GVSchedule = new Telerik.WinControls.UI.RadGridView();
            this.btnSelect = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.GVSchedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVSchedule.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // GVSchedule
            // 
            this.GVSchedule.Location = new System.Drawing.Point(12, 12);
            // 
            // 
            // 
            this.GVSchedule.MasterTemplate.AllowAddNewRow = false;
            gridViewDecimalColumn1.HeaderText = "ID";
            gridViewDecimalColumn1.Name = "column6";
            gridViewDecimalColumn1.Width = 30;
            gridViewTextBoxColumn1.HeaderText = "Дата и время";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.Width = 100;
            gridViewTextBoxColumn2.HeaderText = "Услуга";
            gridViewTextBoxColumn2.Name = "column2";
            gridViewTextBoxColumn2.Width = 230;
            gridViewTextBoxColumn3.HeaderText = "Помещение";
            gridViewTextBoxColumn3.Name = "column3";
            gridViewTextBoxColumn3.Width = 100;
            gridViewTextBoxColumn4.HeaderText = "Своб. места";
            gridViewTextBoxColumn4.Name = "column4";
            gridViewTextBoxColumn4.Width = 70;
            gridViewCheckBoxColumn1.HeaderText = "Выбор";
            gridViewCheckBoxColumn1.Name = "column5";
            gridViewCheckBoxColumn1.Width = 45;
            this.GVSchedule.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewDecimalColumn1,
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewCheckBoxColumn1});
            this.GVSchedule.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GVSchedule.Name = "GVSchedule";
            this.GVSchedule.ReadOnly = true;
            this.GVSchedule.Size = new System.Drawing.Size(673, 434);
            this.GVSchedule.TabIndex = 7;
            this.GVSchedule.Click += new System.EventHandler(this.GVSchedule_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.Location = new System.Drawing.Point(575, 452);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(110, 24);
            this.btnSelect.TabIndex = 8;
            this.btnSelect.Text = "Выбрать";
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // SelectSchedule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 488);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.GVSchedule);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "SelectSchedule";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Удалить записи в расписании";
            this.Load += new System.EventHandler(this.SelectSchedule_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GVSchedule.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVSchedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGridView GVSchedule;
        private Telerik.WinControls.UI.RadButton btnSelect;
    }
}
