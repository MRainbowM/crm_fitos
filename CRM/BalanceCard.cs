﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM
{
    [Table("BalanceCards")]
    public class BalanceCard
    {
        public int ID_Service { get; set; }

        public int ID_Card { get; set; }

        public int Balance { get; set; }

        public string LastDay { get; set; }

        public static void Add(int ID_Card, int ID_Service, int Balance)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                db.Database.EnsureCreated();
                BalanceCard BalanceCard = new BalanceCard
                {
                    ID_Card = ID_Card,
                    ID_Service = ID_Service,
                    Balance = Balance,
                };
                db.BalanceCards.Add(BalanceCard);
                db.SaveChanges();
            }
        }


        public static void AddUseService(int ID_Card, int ID_Service, int Amount)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                db.Database.EnsureCreated();
                BalanceCard BalanceCard1 = GetBalanceService(ID_Card, ID_Service);
                BalanceCard BalanceCard = db.BalanceCards.Where(c => c.ID_Card == ID_Card && c.ID_Service == ID_Service).FirstOrDefault();
                
                BalanceCard.Balance = BalanceCard1.Balance - Amount;
                BalanceCard.LastDay = DateTime.Now.AddHours(-DateTime.Now.Hour).AddMinutes(-DateTime.Now.Minute).AddSeconds(-DateTime.Now.Second).ToString();
                db.SaveChanges();
            }
        }

        public static List<BalanceCard> GetBalance(int ID_Card)
        {
            ApplicationContext db = new ApplicationContext();

            List<BalanceCard> BalanceCardList = db.BalanceCards
                    .Where(x =>  x.ID_Card == ID_Card)
                    .Select(x => new BalanceCard
                    {
                        ID_Card = x.ID_Card,
                        ID_Service = x.ID_Service,
                        Balance = x.Balance,
                        LastDay = x.LastDay
                    }
                    ).ToList();
            return BalanceCardList;
        }


        public static BalanceCard GetBalanceService(int ID_Card, int ID_Service)
        {
            ApplicationContext db = new ApplicationContext();
            BalanceCard BalanceCard = db.BalanceCards.Where(x => x.ID_Card == ID_Card && x.ID_Service == ID_Service).FirstOrDefault();
            return BalanceCard;
        }

    }
}
