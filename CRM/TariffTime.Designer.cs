﻿namespace CRM
{
    partial class TariffTime
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn2 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn3 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn4 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn5 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn6 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn7 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.GVTime = new Telerik.WinControls.UI.RadGridView();
            this.btnSave = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.GVTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVTime.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // GVTime
            // 
            this.GVTime.Location = new System.Drawing.Point(12, 12);
            // 
            // 
            // 
            this.GVTime.MasterTemplate.AllowAddNewRow = false;
            gridViewTextBoxColumn1.HeaderText = "Время";
            gridViewTextBoxColumn1.Name = "column9";
            gridViewTextBoxColumn1.Width = 85;
            gridViewCheckBoxColumn1.HeaderText = "ПН";
            gridViewCheckBoxColumn1.Name = "column1";
            gridViewCheckBoxColumn2.HeaderText = "ВТ";
            gridViewCheckBoxColumn2.Name = "column10";
            gridViewCheckBoxColumn3.HeaderText = "СР";
            gridViewCheckBoxColumn3.Name = "column11";
            gridViewCheckBoxColumn4.HeaderText = "ЧТ";
            gridViewCheckBoxColumn4.Name = "column12";
            gridViewCheckBoxColumn5.HeaderText = "ПТ";
            gridViewCheckBoxColumn5.Name = "column2";
            gridViewCheckBoxColumn6.HeaderText = "СБ";
            gridViewCheckBoxColumn6.Name = "column16";
            gridViewCheckBoxColumn7.HeaderText = "ВС";
            gridViewCheckBoxColumn7.Name = "column17";
            this.GVTime.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewCheckBoxColumn1,
            gridViewCheckBoxColumn2,
            gridViewCheckBoxColumn3,
            gridViewCheckBoxColumn4,
            gridViewCheckBoxColumn5,
            gridViewCheckBoxColumn6,
            gridViewCheckBoxColumn7});
            this.GVTime.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GVTime.Name = "GVTime";
            this.GVTime.ReadOnly = true;
            this.GVTime.Size = new System.Drawing.Size(466, 493);
            this.GVTime.TabIndex = 0;
            this.GVTime.Click += new System.EventHandler(this.GVTime_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(368, 511);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(110, 24);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Сохранить";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // TariffTime
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(489, 547);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.GVTime);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "TariffTime";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Время посещения по тарифу";
            this.Load += new System.EventHandler(this.TariffTime_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GVTime.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGridView GVTime;
        private Telerik.WinControls.UI.RadButton btnSave;
    }
}
