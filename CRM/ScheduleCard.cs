﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace CRM
{
    public partial class ScheduleCard : Telerik.WinControls.UI.RadForm
    {
        public ScheduleCard()
        {
            InitializeComponent();
        }

        public static int ID_Service;
        int ID_Room;

        public static int Count; //количество записей
        public static DateTime Date;

        public bool StateSave = false;
        List<Trainer> TrainerList = new List<Trainer>();

        private void ScheduleCard_Load(object sender, EventArgs e)
        {
            spPeople.Minimum = 1;
            //spMinuts.Minimum = 1440;
            spCost.Maximum = 10000;
            dtDate.MinDate = DateTime.Now;
            spCost.DecimalPlaces = 2;
            if (StateSave)
            {
                FillForm();
                addService.Visible = false;
                addRoom.Visible = false;
                dtDate.Enabled = false;
                tTime.Enabled = false;
            }
            else
            {
                btnDelete.Visible = false;
                addClient.Visible = false;
                addTrainer.Visible = false;
                addRoom.Visible = false;
            }
        }

        private void FillForm()
        {
            ScheduleService scheduleService = ScheduleService.FindByID(MainForm.ID_Schedule);
            ID_Service = scheduleService.ID_Service;
            ID_Room = scheduleService.ID_Room;
            spCost.Value = Service.FindByID(ID_Service).cost;
            spMinuts.Minimum = Service.FindByID(ID_Service).duration;
            tbService.Text = Service.FindByID(ID_Service).name;
            
            dtDate.Value = scheduleService.Date;
            tTime.Value = scheduleService.Date;
            spCost.Value = Service.FindByID(ID_Service).cost;
            spPeople.Value = scheduleService.NumberOfPeople;
            Date = scheduleService.Date;
            List<TrainerInSchedule> TrainerInScheduleList = TrainerInSchedule.FindByIDSchedule(MainForm.ID_Schedule);
            for (int i = 0; i < TrainerInScheduleList.Count; i++)
            {
                lTrainer.Items.Add(Trainer.FindByID(TrainerInScheduleList[i].ID_Trainer).surname + " " + Trainer.FindByID(TrainerInScheduleList[i].ID_Trainer).name + " " + Trainer.FindByID(TrainerInScheduleList[i].ID_Trainer).middleName);
                TrainerList.Add(Trainer.FindByID(TrainerInScheduleList[i].ID_Trainer));
            }

            if (scheduleService.Date > DateTime.Now)
            {
                CreateGVReservation();
            }
            else
            {
                lDelClient.Visible = false;
                addTrainer.Visible = false;
                spCost.Enabled = false;
                addClient.Visible = false;
                btnSave.Visible = false;
                btnReiteration.Visible = false;
                spMinuts.Enabled = false;
                spPeople.Enabled = false;
                CreateGVReservation2();
            }
            tbRoom.Text = Room.FindByID(ID_Room).name;
        }

        private void CreateGVReservation()
        {
            GVReservation.Rows.Clear();
            string fio = "";
            Client client = new Client();
            List<Reservation> ReservationList = Reservation.FindByIDSchudule(MainForm.ID_Schedule);
            Payment payment;
            for (int i = 0; i < ReservationList.Count; i++)
            {
                client = Client.FindByID(ReservationList[i].ID_User);
                fio = client.surname + " " + client.name + " " + client.middleName;
                payment = Payment.FindByIDReservationAndIDClient(ReservationList[i].ID, client.id);

                BalanceCard BalanceCard = BalanceCard.GetBalanceService(ReservationList[i].ID_Card, ScheduleService.FindByID(MainForm.ID_Schedule).ID_Service);
                bool TimeState = false;
                if (ReservationList[i].ID_Card != 0)
                {
                    TimeState = Tariff.CheckedTimeUnTariff(dtDate.Value, Card.FindByID(ReservationList[i].ID_Card).id_Tariff);
                }
                

                if (payment != null && payment.Paid > 0)
                {
                    GVReservation.Rows.Add(client.id, fio, ReservationList[i].ID_Card, "оплачено");
                }
                else if(payment != null && payment.Paid == 0 && BalanceCard == null)
                {
                    GVReservation.Rows.Add(client.id, fio, ReservationList[i].ID_Card, "не оплачено");
                }
                else if (payment == null && BalanceCard == null)
                {
                    GVReservation.Rows.Add(client.id, fio, ReservationList[i].ID_Card, "не оплачено");
                }
                else if (BalanceCard != null && BalanceCard.Balance == 0)
                {
                    GVReservation.Rows.Add(client.id, fio, ReservationList[i].ID_Card, "не оплачено");
                }
                else if(BalanceCard != null && BalanceCard.Balance != 0 && TimeState)
                {
                    GVReservation.Rows.Add(client.id, fio, ReservationList[i].ID_Card, "по тарифу");
                }
                else if (payment == null && BalanceCard != null && BalanceCard.Balance != 0 && !TimeState)
                {
                    GVReservation.Rows.Add(client.id, fio, ReservationList[i].ID_Card, "не оплачено");
                }
                else if (payment != null && payment.Paid == 0 && BalanceCard != null && BalanceCard.Balance != 0 && !TimeState)
                {
                    GVReservation.Rows.Add(client.id, fio, ReservationList[i].ID_Card, "не оплачено");
                }
            }
        }

        private void CreateGVReservation2()
        {
            GVReservation.Rows.Clear();
            string fio = "";
            Client client = new Client();
            List<Reservation> ReservationList = Reservation.FindByIDSchudule(MainForm.ID_Schedule);
            for (int i = 0; i < ReservationList.Count; i++)
            {
                Visitor_Log visit = Visitor_Log.FindByIDReservation(ReservationList[i].ID);
                if (visit != null)
                {
                    client = Client.FindByID(ReservationList[i].ID_User);
                    fio = client.surname + " " + client.name + " " + client.middleName;
                    GVReservation.Rows.Add(client.id, fio, ReservationList[i].ID_Card, visit.Payment);
                }
            }
        }

        private void tbService_TextChanged(object sender, EventArgs e)//изменяется услуга
        {
            tbMaxPeople.Text = Service.FindByID(ID_Service).numberOfPeople.ToString();
            spPeople.Maximum = Convert.ToInt32(tbMaxPeople.Text); 
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (Validation())
            {
                SaveForm();
                this.Close();
            }
        }

        private void SaveForm()
        {
            ScheduleService scheduleService = new ScheduleService();
            Reservation reservation = new Reservation();
            if (StateSave)
            {
                int ID_Schedule = scheduleService.Save(MainForm.ID_Schedule, Convert.ToInt32(spPeople.Value), spCost.Value);
                List<TrainerInSchedule> TrainerBefor = TrainerInSchedule.FindByIDSchedule(MainForm.ID_Schedule);
                for (int i = 0; i < TrainerBefor.Count; i++)
                {
                    TrainerInSchedule.Del(MainForm.ID_Schedule, TrainerBefor[i].ID_Trainer);//удаляет тренера из расписания
                }
                for (int x = 0; x < TrainerList.Count; x++)
                {
                    TrainerInSchedule.Add(TrainerList[x].id, MainForm.ID_Schedule);//добавляет тренера в расписание
                }
                for (int i = 0; i < GVReservation.RowCount; i++)
                {
                    var c = Reservation.FindByIDSchuduleAndIDClient(MainForm.ID_Schedule, Convert.ToInt32(GVReservation.Rows[i].Cells[0].Value));
                    if ( c == null)
                    {
                        int ID_Reservation = 0;
                        if (Convert.ToInt32(GVReservation.Rows[i].Cells[2].Value) != 0)
                        {
                            ID_Reservation = reservation.AddByCard(MainForm.ID_Schedule, Convert.ToInt32(GVReservation.Rows[i].Cells[2].Value));
                        }
                        else
                        {
                            ID_Reservation = reservation.AddByUser(MainForm.ID_Schedule, Convert.ToInt32(GVReservation.Rows[i].Cells[0].Value));
                        }
                        Payment payment = Payment.FindByIDReservationAndIDClient(ID_Reservation, Convert.ToInt32(GVReservation.Rows[i].Cells[0].Value));
                        //BalanceCard BalanceCard = BalanceCard.GetBalanceService(Convert.ToInt32(GVReservation.Rows[i].Cells[2].Value), ScheduleService.FindByID(RadForm1.ID_Schedule).ID_Service);
                        //bool TimeInTariff = Tariff.CheckedTimeUnTariff(ScheduleService.FindByID(ID_Schedule).Date, Card.FindByID(Convert.ToInt32(GVReservation.Rows[i].Cells[2].Value)).id_Tariff);

                        if (GVReservation.Rows[i].Cells[3].Value.ToString() == "не оплачено")
                        {
                            if (payment == null)
                            {
                                Payment.AddDebt(Convert.ToInt32(GVReservation.Rows[i].Cells[0].Value), DateTime.Now, spCost.Value, ID_Reservation);
                            }

                        }

                        //if (payment == null && BalanceCard == null)
                        //{
                        //    Payment.AddDebt(Convert.ToInt32(GVReservation.Rows[i].Cells[0].Value), DateTime.Now, spCost.Value, ID_Reservation);
                        //}
                        //else if (BalanceCard != null && BalanceCard.Balance == 0)
                        //{
                        //    Payment.AddDebt(Convert.ToInt32(GVReservation.Rows[i].Cells[0].Value), DateTime.Now, spCost.Value, ID_Reservation);
                        //}
                        //else if (BalanceCard != null && BalanceCard.Balance != 0 && !TimeInTariff)
                        //{
                        //    Payment.AddDebt(Convert.ToInt32(GVReservation.Rows[i].Cells[0].Value), DateTime.Now, spCost.Value, ID_Reservation);
                        //}
                    }
                }

               
            }
            else
            {

                // AddHours(double value): добавляет к текущей дате несколько часов
                //AddMinutes(double value): добавляет к текущей дате несколько минут
                DateTime Time = Convert.ToDateTime(tTime.Value);
                DateTime Date = dtDate.Value.AddHours(-dtDate.Value.Hour).AddMinutes(-dtDate.Value.Minute).AddSeconds(-dtDate.Value.Second);
                Date = Date.AddHours(Time.Hour).AddMinutes(Time.Minute);

                MainForm.ID_Schedule = scheduleService.Add(ID_Service, ID_Room, Date, Convert.ToInt32(spPeople.Value), Convert.ToInt32(spMinuts.Value), spCost.Value);

                for (int i = 0; i < TrainerList.Count; i++)
                {
                    TrainerInSchedule.Add(TrainerList[i].id, MainForm.ID_Schedule); //добавляет тренера в расписание
                }
            }
            MessageBox.Show(
            "Изменения успешно сохранены",
            "Результат сохранения",
            MessageBoxButtons.OK,
            MessageBoxIcon.Information,
            MessageBoxDefaultButton.Button1,
            MessageBoxOptions.DefaultDesktopOnly);

            StateSave = true;
            btnDelete.Visible = true;
            addClient.Visible = true;
            error.Clear();
            this.Show();
        }

        private bool Validation() //true - все норм false - ошибки
        {
            string message = "\n";
            bool v = true;

            if (tbService.Text == "") { error.SetError(addService, "Добавьте услугу!"); message += "Услуга \n"; v = false; }
            if (tbRoom.Text == "") { error.SetError(addRoom, "Добавьте помещение!"); message += "Помещение \n"; v = false; }
            //if (lTrainer.Items.Count == 0) { error.SetError(addTrainer, "Добавьте тренера!"); message += "Тренер \n"; v = false; }
            //if (spCost.Value == 0) { error.SetError(spCost, "Заполните поле!"); message += "Цена \n"; v = false; }
            //if (spPeople.Value == 0) { error.SetError(spPeople, "Заполните поле!"); message += "Количество людей \n"; v = false; }

            if (v == false)
            {
                DialogResult result = MessageBox.Show(
                "Заполните поля: " + message,
                "Ошибка сохранения!",
                MessageBoxButtons.OK,
                MessageBoxIcon.Stop,
                MessageBoxDefaultButton.Button1,
                MessageBoxOptions.DefaultDesktopOnly);
            }
            return v;
        }
        

        private void addService_Click(object sender, EventArgs e)
        {
            SelectService selectService = new SelectService();
            selectService.ManyServices = false;
            selectService.ShowDialog();
            if (SelectService.SelectOneService != null && SelectService.SelectOneService.name != null)
            {
                ID_Service = SelectService.SelectOneService.id;
                tbService.Text = SelectService.SelectOneService.name;
            }
            if (Service.FindByID(ID_Service) != null)
            {
                spMinuts.Minimum = Service.FindByID(ID_Service).duration;
                spMinuts.Value = Service.FindByID(ID_Service).duration;
                spPeople.Value = Service.FindByID(ID_Service).numberOfPeople;
                spCost.Value = Service.FindByID(ID_Service).cost;
                addRoom.Visible = true;
            }
            
        }

        private void sddTrainer_Click(object sender, EventArgs e)
        {
            SelectTrainer selectTrainer = new SelectTrainer();
            selectTrainer.From = dtDate.Value.AddHours(-dtDate.Value.Hour).AddMinutes(-dtDate.Value.Minute).AddSeconds(-dtDate.Value.Second).AddHours(Convert.ToDateTime(tTime.Value).Hour).AddMinutes(Convert.ToDateTime(tTime.Value).Minute);
            selectTrainer.To = dtDate.Value.AddHours(-dtDate.Value.Hour).AddMinutes(-dtDate.Value.Minute).AddSeconds(-dtDate.Value.Second).AddHours(Convert.ToDateTime(tTimeEnd.Value).Hour).AddMinutes(Convert.ToDateTime(tTimeEnd.Value).Minute);
            //if (clTrainers.Items.Count != 0)
            //{
            //    for (int i = 0; i < clTrainers.Items.Count; i++)
            //    {
            //        SelectTrainer.TrainersInGV.Add(Trainer.FindByID())
            //    }

            //}
            selectTrainer.ForSchedule = true;
            selectTrainer.ShowDialog();
            TrainerList.Clear();

            for (int i = 0; i < SelectTrainer.TrainerList.Count; i++)
            {
                lTrainer.Items.Add(SelectTrainer.TrainerList[i].surname + " " + SelectTrainer.TrainerList[i].name + " " + SelectTrainer.TrainerList[i].middleName);
                TrainerList.Add(SelectTrainer.TrainerList[i]);
            }
        }

        private void addClient_Click(object sender, EventArgs e)
        {
            if (GVReservation.RowCount < Convert.ToInt16(spPeople.Text))
            {
                SelectClient selectClient = new SelectClient();
                SelectClient.ClientInGV.Clear();
                for (int i = 0; i < GVReservation.RowCount; i++)
                {
                    SelectClient.ClientInGV.Add(Client.FindByID(Convert.ToInt32(GVReservation.Rows[i].Cells[0].Value)));
                }
                selectClient.ShowDialog();
                SelectCardClient selectCardClient = new SelectCardClient();
                Card card = new Card();
                if (SelectClient.ClientSelect != null)
                {
                    selectCardClient.CardList = Card.GetCardByClient(SelectClient.ClientSelect.id);
                    if (selectCardClient.CardList.Count > 0)
                    {
                        selectCardClient.ShowDialog();
                        card = selectCardClient.CardClient;
                    }
                }


                if (SelectClient.ClientSelect != null && card != null && card.id != 0)
                {
                    BalanceCard BalanceCard = BalanceCard.GetBalanceService(card.id, ID_Service);
                    bool TimeState = Tariff.CheckedTimeUnTariff(dtDate.Value, card.id_Tariff);

                    if (BalanceCard != null && BalanceCard.Balance != 0 && TimeState)
                    {
                        GVReservation.Rows.Add(SelectClient.ClientSelect.id, SelectClient.ClientSelect.surname + " " + SelectClient.ClientSelect.name + " " + SelectClient.ClientSelect.middleName, card.id, "по тарифу");
                    }

                    if (BalanceCard != null && BalanceCard.Balance != 0 && !TimeState)
                    {
                        GVReservation.Rows.Add(SelectClient.ClientSelect.id, SelectClient.ClientSelect.surname + " " + SelectClient.ClientSelect.name + " " + SelectClient.ClientSelect.middleName, card.id, "не оплачено");
                    }

                    if (BalanceCard != null && BalanceCard.Balance == 0)
                    {
                        GVReservation.Rows.Add(SelectClient.ClientSelect.id, SelectClient.ClientSelect.surname + " " + SelectClient.ClientSelect.name + " " + SelectClient.ClientSelect.middleName, card.id, "не оплачено");
                    }
                    if (BalanceCard == null)
                    {
                        GVReservation.Rows.Add(SelectClient.ClientSelect.id, SelectClient.ClientSelect.surname + " " + SelectClient.ClientSelect.name + " " + SelectClient.ClientSelect.middleName, card.id, "не оплачено");
                    }
                }
                else if (SelectClient.ClientSelect != null)
                {
                    GVReservation.Rows.Add(SelectClient.ClientSelect.id, SelectClient.ClientSelect.surname + " " + SelectClient.ClientSelect.name + " " + SelectClient.ClientSelect.middleName, card.id, "не оплачено");
                }
            }
            else
            {
                DialogResult result = MessageBox.Show(
               "Достигнуто максимальное количество клиентов. \nУвеличьте количество клиентов в записи или удалите существующих",
               "Ошибка добавления клиента",
               MessageBoxButtons.OK,
               MessageBoxIcon.Stop,
               MessageBoxDefaultButton.Button1,
               MessageBoxOptions.DefaultDesktopOnly);
                this.Show();
            }
        }

        private void addRoom_Click(object sender, EventArgs e)
        {
            SelectRoom selectRoom = new SelectRoom();
            selectRoom.ForSchedule = true;
            selectRoom.From = dtDate.Value.AddHours(-dtDate.Value.Hour).AddMinutes(-dtDate.Value.Minute).AddSeconds(-dtDate.Value.Second).AddHours(Convert.ToDateTime(tTime.Value).Hour).AddMinutes(Convert.ToDateTime(tTime.Value).Minute);
            selectRoom.To = dtDate.Value.AddHours(-dtDate.Value.Hour).AddMinutes(-dtDate.Value.Minute).AddSeconds(-dtDate.Value.Second).AddHours(Convert.ToDateTime(tTimeEnd.Value).Hour).AddMinutes(Convert.ToDateTime(tTimeEnd.Value).Minute);
            selectRoom.ShowDialog();
            if (SelectRoom.SelectOneRoom != null && SelectRoom.SelectOneRoom.name != null)
            {
                tbRoom.Text = SelectRoom.SelectOneRoom.name;
                ID_Room = SelectRoom.SelectOneRoom.id;
                addTrainer.Visible = true;
            }
            if (tbRoom.Text != "")
            {
                error.Clear();
            }
        }

        private void reiteration_Click(object sender, EventArgs e)
        {
            if (Validation())
            {
                SaveForm();
                ReiterationSchedule reiterationSchedule = new ReiterationSchedule();
                reiterationSchedule.ShowDialog();
                this.Close();
            }
        }

        private void dtDate_ValueChanged(object sender, EventArgs e)
        {
            DateTime Time = Convert.ToDateTime(tTime.Value);
            Date = dtDate.Value.AddHours(Time.Hour).AddMinutes(Time.Minute);

            List<Room> BusyRoom = ScheduleService.ScheduleServiceBusyRooms(Date, Date.AddMinutes(Convert.ToDouble(spMinuts.Value)));
            if (tbRoom.Text != "")
            {
                for (int i = 0; i < BusyRoom.Count; i++)
                {
                    if (ID_Room == BusyRoom[i].id)
                    {
                        DialogResult result = MessageBox.Show(
                          "В данное время помещение занято. Веберете другую дату или помещение." + Service.FindByID(ScheduleCard.ID_Service).name,
                          "Внимание",
                          MessageBoxButtons.OK,
                          MessageBoxIcon.Stop,
                          MessageBoxDefaultButton.Button1,
                          MessageBoxOptions.DefaultDesktopOnly);
                        tbRoom.Text = "";
                    }
                }
            }
        }

        private void tTime_ValueChanged(object sender, EventArgs e)
        {
            DateTime Time = Convert.ToDateTime(tTime.Value);
            Date = dtDate.Value.AddHours(Time.Hour).AddMinutes(Time.Minute);
            tTimeEnd.Value = Convert.ToDateTime(tTime.Value).AddMinutes(Convert.ToInt32(spMinuts.Value));
        }

        private void spMinuts_ValueChanged(object sender, EventArgs e)
        {
            tTimeEnd.Value = Convert.ToDateTime(tTime.Value).AddMinutes(Convert.ToInt32(spMinuts.Value));
        }

        private void GVReservation_CellDoubleClick(object sender, Telerik.WinControls.UI.GridViewCellEventArgs e)
        {
            ScheduleService scheduleService = ScheduleService.FindByID(MainForm.ID_Schedule);
            if (scheduleService.Date > DateTime.Now)
            {
                int row = GVReservation.CurrentCell.RowIndex;
                DialogResult result = MessageBox.Show(
                "Удалить клиента: " + GVReservation.Rows[row].Cells[1].Value.ToString() + "?",
                "Удаление клиента",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Stop,
                MessageBoxDefaultButton.Button1,
                MessageBoxOptions.DefaultDesktopOnly);
                if (result == DialogResult.Yes)
                {
                    SaveForm();
                    Payment pay = Payment.FindByIDReservationAndIDClient(Reservation.FindByIDSchuduleAndIDClient(MainForm.ID_Schedule, Convert.ToInt32(GVReservation.Rows[row].Cells[0].Value)).ID, Convert.ToInt32(GVReservation.Rows[row].Cells[0].Value));
                    if (pay != null)
                    {
                        Payment.DelDebt(pay.ID);
                        Reservation.Del(Reservation.FindByIDSchuduleAndIDClient(MainForm.ID_Schedule, Convert.ToInt32(GVReservation.Rows[row].Cells[0].Value)).ID);
                    }
                    else
                    {
                        Reservation.Del(Reservation.FindByIDSchuduleAndIDClient(MainForm.ID_Schedule, Convert.ToInt32(GVReservation.Rows[row].Cells[0].Value)).ID);
                    }

                    CreateGVReservation();
                }
                this.Show();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(
            "Вы уверены, что хотите удалить запись?",
            "Удаление записи",
            MessageBoxButtons.YesNo,
            MessageBoxIcon.Stop,
            MessageBoxDefaultButton.Button1,
            MessageBoxOptions.DefaultDesktopOnly);
            if (result == DialogResult.Yes)
            {
                ScheduleService.Del(MainForm.ID_Schedule);
            }
            this.Close();
        }







        //private void RefreshCard()
        //{
        //    if (SelectService.SelectOneService != null && SelectService.SelectOneService.name != null)
        //    {
        //        tbService.Text = SelectService.SelectOneService.name;
        //        ID_Service = SelectService.SelectOneService.id;
        //    }
        //    if (SelectRoom.SelectOneRoom != null && SelectRoom.SelectOneRoom.name != null)
        //    {
        //        tbRoom.Text = SelectRoom.SelectOneRoom.name;
        //        ID_Room = SelectRoom.SelectOneRoom.id;
        //    }
        //}


    }
}
