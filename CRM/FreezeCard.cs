﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace CRM
{
    public partial class FreezeCard : Telerik.WinControls.UI.RadForm
    {
        public FreezeCard()
        {
            InitializeComponent();
        }

        public static Card card; 

        private void FreezeCard_Load(object sender, EventArgs e)
        {
            tbIDCard.Enabled = false;
            tbNCard.Enabled = false;
            spDays.Enabled = false;
            //dtFrom.MinDate = DateTime.Now;
            dtFrom.Value = DateTime.Now;
            dtTo.MinDate = DateTime.Now;
            //dtFrom.Value = DateTime.MinValue;
            dtTo.Value = DateTime.MinValue;

            tbIDCard.Text = card.id.ToString();
            tbNCard.Text = card.n_Card.ToString();

            spDays.Value = BalanceCard.GetBalanceService(card.id, 1).Balance;

        }

        private void dtFrom_ValueChanged(object sender, EventArgs e)
        {
            dtTo.MinDate = dtFrom.Value;
            if (dtTo.Value != DateTime.MinValue)
            {
                lDays.Text = (dtTo.Value - dtFrom.Value).Days.ToString();
            }
        }

        private void dtTo_ValueChanged(object sender, EventArgs e)
        {
            if (dtFrom.Value != DateTime.MinValue)
            {
                lDays.Text = (dtTo.Value - dtFrom.Value).Days.ToString();
            }
        }

        private void btnFreeze_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(lDays.Text) <= spDays.Value)
            {
                DialogResult result = MessageBox.Show(
                "Заморозить карту на " + lDays.Text + " дней?",
                "Заморозка карты",
                MessageBoxButtons.OKCancel,
                MessageBoxIcon.Information,
                MessageBoxDefaultButton.Button1,
                MessageBoxOptions.DefaultDesktopOnly);
                if (result == DialogResult.OK)
                {
                    Card.FreezeCard(card.id, dtFrom.Value, dtTo.Value);
                    
                    this.Close();
                }
            }
            else
            {
                DialogResult result = MessageBox.Show(
               "Вы не можете заморозить карту на " + lDays.Text + " дн., так как Вам доступно " + spDays.Value + " дн. заморозки.",
               "Ошибка!",
               MessageBoxButtons.OK,
               MessageBoxIcon.Stop,
               MessageBoxDefaultButton.Button1,
               MessageBoxOptions.DefaultDesktopOnly);
            }
        }
    }
}
