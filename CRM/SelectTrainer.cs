﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace CRM
{
    public partial class SelectTrainer : Telerik.WinControls.UI.RadForm
    {
        public SelectTrainer()
        {
            InitializeComponent();
        }

        public static List<Trainer> TrainersInGV = new List<Trainer>(); //тренера, которые уже есть в таблице

        public static List<Trainer> TrainerList = new List<Trainer>(); //лист для добавления тренеров
        public DateTime From;
        public DateTime To;

        public bool ForSchedule = false; //истина - если тренера выбираются для расписания

        private void SelectTrainer_Load(object sender, EventArgs e)
        {
            if (ForSchedule)
            {
                CreateGVTrainerForSchedule();
            }
            else
            {
                CreateGVAllTrainers();
            }
            search.Visible = false;

        }
        private void CreateGVAllTrainers()
        {
            GVTrainers.Rows.Clear();
            int p = 0;
            List<Trainer> Trainers = Trainer.GetAll();
            for (int i = 0; i <Trainers.Count; i++)// создание таблицы с Тренерами
            {
                for (int x = 0; x < TrainersInGV.Count; x++)
                {
                    if (Trainers[i].id == TrainersInGV[x].id)
                    {
                        p++;
                    }
                }
                if (p == 0)
                {
                    GVTrainers.Rows.Add(Trainers[i].id, Trainers[i].surname, Trainers[i].name, Trainers[i].middleName);
                }
                p = 0;
            }
        }

        private void CreateGVTrainerForSchedule()
        {
            GVTrainers.Rows.Clear();

            List<Trainer> BusyTrainers = ScheduleService.ScheduleServiceBusyTrainer(From, To);
            List<TrainerService> Trainers = TrainerService.GetTrainers(ScheduleCard.ID_Service);
            for (int i = 0; i < Trainers.Count; i++)// создание таблицы с Тренерами
            {
                int k = 0;
                for (int j = 0; j < BusyTrainers.Count; j++)
                {
                    if (Trainers[i].ID_Trainer == BusyTrainers[j].id)
                    {
                        k++;
                    }
                }
                if (k == 0)
                {
                    GVTrainers.Rows.Add(Trainers[i].ID_Trainer, Trainer.FindByID(Trainers[i].ID_Trainer).surname, Trainer.FindByID(Trainers[i].ID_Trainer).name, Trainer.FindByID(Trainers[i].ID_Trainer).middleName);
                }
            }
            if (GVTrainers.RowCount == 0)
            {
                DialogResult result = MessageBox.Show(
              "В данное время (" + From + ") нет свободных тренеров для услуги: " + Service.FindByID(ScheduleCard.ID_Service).name,
              "Помещений не найдено",
              MessageBoxButtons.OK,
              MessageBoxIcon.Stop,
              MessageBoxDefaultButton.Button1,
              MessageBoxOptions.DefaultDesktopOnly);
                this.Close();
            }
        }





        private void search_Click(object sender, EventArgs e)
        {
            SearchTrainer SearchTrainer = new SearchTrainer();
            SearchTrainer.ShowDialog();
            List<Trainer> Trainer1 = Trainer.GetAll();
            if (Trainer1.Count == MainForm.TrainerList.Count)
            {
                if (ForSchedule)
                {
                    CreateGVTrainerForSchedule();
                }
                else
                {
                    CreateGVAllTrainers();
                }
            }
            else
            {
                CreateGVFilter();
            }
        }

        private void CreateGVFilter()
        {
            GVTrainers.Rows.Clear();
            List<Trainer> Trainers = MainForm.TrainerList;
            List<Trainer> List1 = Trainer.GetAll();
            if (List1.Count != Trainers.Count)
            {
                int p = 0;

                for (int i = 0; i < Trainers.Count; i++)
                {
                    for (int x = 0; x < TrainersInGV.Count; x++)
                    {
                        if (Trainers[i].id == TrainersInGV[x].id)
                        {
                            p++;
                        }
                    }
                    if (p == 0)
                    {
                        GVTrainers.Rows.Add(Trainers[i].id, Trainers[i].surname, Trainers[i].name, Trainers[i].middleName);
                    }
                    p = 0;
                }
                MainForm.TrainerList = Trainer.GetAll();
            }
            else if(ForSchedule)
            {
                CreateGVTrainerForSchedule();
            }
            else
            {
                CreateGVAllTrainers();
            }
        }


        private void btnSelect_Click(object sender, EventArgs e)
        {
            TrainerList.Clear();
            Trainer trainer = new Trainer();
            for (int i = 0; i < GVTrainers.RowCount; i++)
            {
                if (GVTrainers.Rows[i].Cells[4].Value != null)
                {
                    bool u = (bool)GVTrainers.Rows[i].Cells[4].Value;
                    if (u)
                    {
                        trainer = Trainer.FindByID(Convert.ToInt32(GVTrainers.Rows[i].Cells[0].Value));
                        TrainerList.Add(trainer);

                    }
                }
            }
            this.Close();
        }

        private void GVTrainers_Click(object sender, EventArgs e)
        {
            int row = GVTrainers.CurrentCell.RowIndex;
            if (GVTrainers.Rows[row].Cells[4].Value != null)
            {
                if ((bool)GVTrainers.Rows[row].Cells[4].Value == true)
                {
                    GVTrainers.Rows[row].Cells[4].Value = false;
                }
                else
                {
                    GVTrainers.Rows[row].Cells[4].Value = true;
                }
            }
            else
            {
                GVTrainers.Rows[row].Cells[4].Value = true;
            }
        }
    }
}
