﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace CRM
{
    public partial class SelectCardClient : Telerik.WinControls.UI.RadForm
    {
        public SelectCardClient()
        {
            InitializeComponent();
        }

        public List<Card> CardList = new List<Card>();
        public Card CardClient = new Card();

        private void SelectCardClient_Load(object sender, EventArgs e)
        {
            tbName.Enabled = false;
            tbTariff.Enabled = false;
            tbService.Enabled = false;
            tbService.Text = Service.FindByID(ScheduleService.FindByID(MainForm.ID_Schedule).ID_Service).name;
            tbName.Text = Client.FindByID(SelectClient.ClientSelect.id).surname + " " + Client.FindByID(SelectClient.ClientSelect.id).name + " " + Client.FindByID(SelectClient.ClientSelect.id).middleName;
            dtDate.Value = ScheduleService.FindByID(MainForm.ID_Schedule).Date;
            tTime.Value = ScheduleService.FindByID(MainForm.ID_Schedule).Date;
            CardList = Card.GetCardByClient(SelectClient.ClientSelect.id);
            for (int i = 0; i < CardList.Count; i++)
            {
                cbCard.Items.Add(CardList[i].n_Card);
            }
            cbCard.SelectedIndex = 0;


        }

        private void cbCard_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            for (int i = 0; i < CardList.Count; i++)
            {
                if (cbCard.Text == CardList[i].n_Card)
                {
                    tbTariff.Text = Tariff.FindByID(CardList[i].id_Tariff).name;

                    BalanceCard BalanceCard = BalanceCard.GetBalanceService(CardList[i].id, ScheduleService.FindByID(MainForm.ID_Schedule).ID_Service);

                    bool TimeState = Tariff.CheckedTimeUnTariff(dtDate.Value, CardList[i].id_Tariff);

                    if (BalanceCard != null && BalanceCard.Balance != 0 && TimeState)
                    {
                        lMas.Visible = true;
                        lMas.Text = "Услуга входит в тариф";
                        lTime.Visible = false;
                    }
                    else if (BalanceCard != null && BalanceCard.Balance != 0 && !TimeState)
                    {
                        lMas.Visible = false;
                        lTime.Visible = true;
                    }
                    else if(BalanceCard != null && BalanceCard.Balance == 0)
                    {
                        lMas.Text = "Услуга по тарифу недоступна";
                        lTime.Visible = false;
                        lMas.Visible = true;

                    }
                    else if(BalanceCard == null)
                    {
                        lMas.Text = "Услуга не входит в тариф";
                        lTime.Visible = false;
                        lMas.Visible = true;
                    }
                }
            }

            

        }

        bool save = false;
        private void btnSelect_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < CardList.Count; i++)
            {
                if (cbCard.Text == CardList[i].n_Card)
                {
                    CardClient = CardList[i];
                }
            }
            save = true;
            this.Close();
            
        }

        private void SelectCardClient_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!save)
            {
                SelectClient selectClient = new SelectClient();
                SelectClient.ClientSelect = null;
            }
            
        }
    }
}
