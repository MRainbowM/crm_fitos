﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM
{
    public class ScheduleService
    {
        public int ID { get; set; }
        public int ID_Service { get; set; }
        public int ID_Room { get; set; }
        public int NumberOfPeople { get; set; }
        public decimal Cost { get; set; }
        public int Duration { get; set; }
        public DateTime Date { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string DateDelete { get; set; }

        public int Add(int ID_Service, int ID_Room, DateTime Date, int NumberOfPeople, int Duration, decimal Cost)
        {
            using (ApplicationContext db = new ApplicationContext())
            {

                db.Database.EnsureCreated();
                ScheduleService ScheduleService = new ScheduleService
                {
                    ID_Service = ID_Service,
                    ID_Room = ID_Room,
                    Date = Date,
                    NumberOfPeople = NumberOfPeople,
                    Duration = Duration,
                    Cost = Cost
                };
                db.ScheduleServices.Add(ScheduleService);
                db.SaveChanges();
                int ID = ScheduleService.ID;
                return ID;
            }
        }

        public int Save(int ID_Schedule, int NumberOfPeople, decimal Cost)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                db.Database.EnsureCreated();
                ScheduleService ScheduleService = db.ScheduleServices.Where(c => c.ID == ID_Schedule).FirstOrDefault();
                ScheduleService.NumberOfPeople = NumberOfPeople;
                ScheduleService.Cost = Cost;

                db.SaveChanges();

                int ID = ScheduleService.ID;
                return ID;
            }
        }


        

        //public static ScheduleService FindByID(int ID_Card)
        //{
        //    ApplicationContext db = new ApplicationContext();
        //    Card card = db.Cards.Where(x => x.DateDelete == null && x.ID == ID_Card).FirstOrDefault();
        //    return card;
        //}

        public static ScheduleService FindByID(int ID_Schedule)
        {
            ApplicationContext db = new ApplicationContext();
            ScheduleService ScheduleService = db.ScheduleServices.Find(ID_Schedule);
            return ScheduleService;
        }

        public static List<ScheduleService> FindByIDRoom(int ID_Room)
        {
            ApplicationContext db = new ApplicationContext();
            List<ScheduleService> ScheduleService = db.ScheduleServices
                    .Where(x => x.DateDelete == null && x.ID_Room == ID_Room) 
                    .Select(x => new ScheduleService
                    {
                        ID = x.ID,
                        ID_Service = x.ID_Service,
                        ID_Room = x.ID_Room,
                        Date = x.Date,
                        NumberOfPeople = x.NumberOfPeople,
                        Duration = x.Duration,
                        Cost = x.Cost
                    }).ToList();
            return ScheduleService;
        }

        public static List<ScheduleService> FindByIDService(int ID_Service)
        {
            ApplicationContext db = new ApplicationContext();
            List<ScheduleService> ScheduleService = db.ScheduleServices
                    .Where(x => x.DateDelete == null && x.ID_Service == ID_Service)
                    .Select(x => new ScheduleService
                    {
                        ID = x.ID,
                        ID_Service = x.ID_Service,
                        ID_Room = x.ID_Room,
                        Date = x.Date,
                        NumberOfPeople = x.NumberOfPeople,
                        Duration = x.Duration,
                        Cost = x.Cost,
                        StartDate = x.StartDate,
                        EndDate = x.EndDate
                    }).ToList();
            return ScheduleService;
        }

        public static List<ScheduleService> ScheduleServiceNow()
        {
            DateTime DateNow = DateTime.Now.AddHours(-DateTime.Now.Hour).AddMinutes(-DateTime.Now.Minute);
            DateTime DateTommorow = DateNow.AddDays(1);
            ApplicationContext db = new ApplicationContext();
            List<ScheduleService> ScheduleService = db.ScheduleServices
                    .Where(x => x.DateDelete == null && x.ID_Service != 1 && x.Date >= DateNow && x.Date < DateTommorow)
                    .Select(x => new ScheduleService
                    {
                        ID = x.ID,
                        ID_Service = x.ID_Service,
                        ID_Room = x.ID_Room,
                        Date = x.Date,
                        NumberOfPeople = x.NumberOfPeople,
                        Duration = x.Duration,
                        Cost = x.Cost,
                        StartDate = x.StartDate,
                        EndDate = x.EndDate
                    }).ToList();
            return ScheduleService;
        }

        public static List<Room> ScheduleServiceBusyRooms(DateTime From, DateTime To)
        {
            ApplicationContext db = new ApplicationContext();
            List<ScheduleService> ScheduleService = db.ScheduleServices
                    .Where(x => x.DateDelete == null && x.ID_Service != 1 && x.Date >= From && x.Date < To)
                    .Select(x => new ScheduleService
                    {
                        ID = x.ID,
                        ID_Service = x.ID_Service,
                        ID_Room = x.ID_Room,
                        Date = x.Date,
                        NumberOfPeople = x.NumberOfPeople,
                        Duration = x.Duration,
                        Cost = x.Cost,
                        StartDate = x.StartDate,
                        EndDate = x.EndDate
                    }).ToList();
            List<Room> Rooms = new List<Room>();
            for (int i = 0; i < ScheduleService.Count; i++)
            {
                Rooms.Add(Room.FindByID(ScheduleService[i].ID_Room));
            }
            return Rooms;
        }

        public static List<Trainer> ScheduleServiceBusyTrainer(DateTime From, DateTime To)
        {
            ApplicationContext db = new ApplicationContext();
            List<ScheduleService> ScheduleService = db.ScheduleServices
                    .Where(x => x.DateDelete == null && x.ID_Service != 1 && x.Date >= From && x.Date < To)
                    .Select(x => new ScheduleService
                    {
                        ID = x.ID,
                        ID_Service = x.ID_Service,
                        ID_Room = x.ID_Room,
                        Date = x.Date,
                        NumberOfPeople = x.NumberOfPeople,
                        Duration = x.Duration,
                        Cost = x.Cost,
                        StartDate = x.StartDate,
                        EndDate = x.EndDate
                    }).ToList();
            List<Trainer> Trainers = new List<Trainer>();
            for (int i = 0; i < ScheduleService.Count; i++)
            {
                List<TrainerInSchedule> BusyTrainers = TrainerInSchedule.FindByIDSchedule(ScheduleService[i].ID);
                for (int j = 0; j < BusyTrainers.Count; j++)
                {
                    Trainers.Add(Trainer.FindByID(BusyTrainers[j].ID_Trainer));
                }
            }
            return Trainers;
        }


        public static List<ScheduleService> GetAll()
        {
            ApplicationContext db = new ApplicationContext();

            List<ScheduleService> ScheduleServiceList = db.ScheduleServices
                    .Where(x => x.DateDelete == null && x.ID_Service != 1) /*&& x.Date > DateTime.Now.Subtract(new TimeSpan(1, 0, 0, 0)))*/
                    .Select(x => new ScheduleService
                    {
                        ID = x.ID,
                        ID_Service = x.ID_Service,
                        ID_Room = x.ID_Room,
                        Date = x.Date,
                        NumberOfPeople = x.NumberOfPeople,
                        Duration = x.Duration,
                        Cost = x.Cost
                    }).ToList();
            return ScheduleServiceList;
        }

        public static void Del(int ID)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                db.Database.EnsureCreated();
                ScheduleService ScheduleService = db.ScheduleServices.Where(c => c.ID == ID).FirstOrDefault();
                ScheduleService.DateDelete = DateTime.Today.ToString();

                db.SaveChanges();
            }
        }
    }
}
