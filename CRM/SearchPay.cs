﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace CRM
{
    public partial class SearchPay : Telerik.WinControls.UI.RadForm
    {
        public SearchPay()
        {
            InitializeComponent();
        }

        public static List<Payment> FilterPayment = new List<Payment>();

        private void SearchPay_Load(object sender, EventArgs e)
        {
            dtFrom.Value = DateTime.MinValue;
            dtTo.Value = DateTime.MinValue;
            spFrom.Minimum = 0;
            spTo.Minimum = 0;
            spFrom.Maximum = 100000;
            spTo.Maximum = 100000;
            spTo.Value = 10000;
            grDate.Enabled = false;
        }


        private void selectService_Click(object sender, EventArgs e)
        {
            SelectService selectService = new SelectService();
            selectService.ShowDialog();
        }

        private void selectClient_Click(object sender, EventArgs e)
        {
            SelectClient selectClient = new SelectClient();
            selectClient.ShowDialog();
        }

        private void spFrom_ValueChanged(object sender, EventArgs e)
        {
            spTo.Minimum = spFrom.Value;
        }

        private void dtFrom_ValueChanged(object sender, EventArgs e)
        {
            dtTo.MinDate = dtFrom.Value;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            tbSurname.Text = "";
            tbName.Text = "";
            tbMiddleName.Text = "";
            tbService.Text = "";
            spFrom.Value = 0;
            spTo.Value = 100000;
            dtFrom.Value = DateTime.MinValue;
            dtTo.Value = DateTime.MinValue;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string Surname = tbSurname.Text;
            string Name = tbName.Text;
            string MiddleName = tbMiddleName.Text;

            string ServiceName = tbService.Text;

            decimal From = spFrom.Value;
            decimal To = spTo.Value;

            DateTime DateFrom = dtFrom.Value;
            DateTime DateTo = dtTo.Value;

            int p = -1;
            
            MainForm.PaymentList = Payment.GetAll();
            FilterPayment.Clear();
            Client client;
            string service;
            for (int i = 0; i < MainForm.PaymentList.Count; i++)
            {
                p = -1;
                client = Client.FindByID(MainForm.PaymentList[i].ID_User);
                p = client.surname.ToLower().IndexOf(Surname.ToLower());
                if (p > -1)
                {
                    p = -1;
                    p = client.name.ToLower().IndexOf(Name.ToLower());
                    if (p > -1)
                    {
                        p = -1;
                        p = client.middleName.ToLower().IndexOf(MiddleName.ToLower());
                        if (p > -1)
                        {
                            p = -1;
                            service = Service.FindByID(ScheduleService.FindByID((Reservation.FindByID(MainForm.PaymentList[i].ID_Reservation).ID_ScheduleService)).ID_Service).name;
                            p = service.ToLower().IndexOf(ServiceName.ToLower());
                            if (p > -1)
                            {
                                if (MainForm.PaymentList[i].Paid >= From)
                                {
                                    if (MainForm.PaymentList[i].Paid <= To)
                                    {
                                        if (cbDate.Checked)
                                        {
                                            if (MainForm.PaymentList[i].DateOfPayment >= DateFrom)
                                            {
                                                if (MainForm.PaymentList[i].DateOfPayment <= DateTo)
                                                {
                                                    FilterPayment.Add(MainForm.PaymentList[i]);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            FilterPayment.Add(MainForm.PaymentList[i]);
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }

            MainForm.PaymentList.Clear();
            if (FilterPayment.Count != 0)
            {
                for (int i = 0; i < FilterPayment.Count; i++)
                {
                    MainForm.PaymentList.Add(FilterPayment[i]);
                }
            }

            else if (MainForm.PaymentList.Count == 0)
            {
                MessageBox.Show(
                "Совпадений не найдено",
                "Результат поиска",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information,
                MessageBoxDefaultButton.Button1,
                MessageBoxOptions.DefaultDesktopOnly);
                MainForm.PaymentList = Payment.GetAll();
            }
            this.Close();
        }

        private void cbDate_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (cbDate.Checked)
            {
                grDate.Enabled = true;
            }
            else
            {
                grDate.Enabled = false;
            }
        }
    }
}
