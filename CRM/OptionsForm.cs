﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace CRM
{
    public partial class OptionsForm : Telerik.WinControls.UI.RadForm
    {
        public OptionsForm()
        {
            InitializeComponent();
        }

        private void OptionsForm_Load(object sender, EventArgs e)
        {
            if (Options.FindByID(1) != null)
            {
                tbWayPhoto.Text = Options.FindByID(1).Value;
            }
            else
            {
                tbWayPhoto.Text = Environment.CurrentDirectory + "\\Photo";
                Options.Add(1, "WayToPhoto", tbWayPhoto.Text);
            }
            //string g= Environment.CurrentDirectory + "\\Photo";
            tbPassword.PasswordChar = '*';
            tbPassword.Enabled = true;
        }

        private void btnWay_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog FBD = new FolderBrowserDialog();
            if (FBD.ShowDialog() == DialogResult.OK)
            {
                MessageBox.Show(FBD.SelectedPath);
                tbWayPhoto.Text = FBD.SelectedPath;
            }
            Options options = Options.FindByID(1);
            if (options != null)
            {
                Options.Save(1, "WayToPhoto", FBD.SelectedPath);
            }
            else
            {
                Options.Add(1, "WayToPhoto", FBD.SelectedPath);
            }
        }

        private void dtnPassword_Click(object sender, EventArgs e)
        {
            Options options = Options.FindByID(2);
            if (options != null)
            {
                Options.Save(2, "PasswordAdmin", tbPassword.Text);
            }
            else
            {
                Options.Add(2, "PasswordAdmin", tbPassword.Text);
            }
            MessageBox.Show(
               "Пароль изменен",
               "Результат сохранения",
               MessageBoxButtons.OK,
               MessageBoxIcon.Information,
               MessageBoxDefaultButton.Button1,
               MessageBoxOptions.DefaultDesktopOnly);
            this.Show();

        }
    }
}
