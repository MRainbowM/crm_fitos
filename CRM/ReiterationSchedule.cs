﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace CRM
{
    public partial class ReiterationSchedule : Telerik.WinControls.UI.RadForm
    {
        public ReiterationSchedule()
        {
            InitializeComponent();
        }

        public bool StateEnd; //ложь - дата, истина - кол-во повторений
        List<string> DayList = new List<string>();
        DateTime Date1;
        int DurationService;
        List<DateTime> NewScheduleDateTime = new List<DateTime>();


        private void ReiterationSchedule_Load(object sender, EventArgs e)
        {
            tbCount.Text = "0";
            spCount1.Minimum = 0;
            cbPeriod.Items.Add("Час");
            cbPeriod.Items.Add("День");
            //cbPeriod.Items.Add("Месяц");
            //cbPeriod.Items.Add("Год");
            cbPeriod.SelectedIndex = 0;

            //dtDate.Value = ScheduleCard.Date;
            //tTime.Value = ScheduleCard.Date;

            dtDate.Value = ScheduleService.FindByID(MainForm.ID_Schedule).Date;
            tTime.Value = ScheduleService.FindByID(MainForm.ID_Schedule).Date;

            tbService.Text = Service.FindByID(ScheduleCard.ID_Service).name;

            dtEndDate.MinDate = ScheduleCard.Date;

            tbDuration.Text = ScheduleService.FindByID(MainForm.ID_Schedule).Duration.ToString();
            DurationService = ScheduleService.FindByID(MainForm.ID_Schedule).Duration;
            tbRoom.Text = Room.FindByID(ScheduleService.FindByID(MainForm.ID_Schedule).ID_Room).name;

            Date1 = ScheduleCard.Date;

            string Day = ScheduleCard.Date.ToString("dddd");

            clDay.Items.Add("понедельник");
            clDay.Items.Add("вторник");
            clDay.Items.Add("среда");
            clDay.Items.Add("четверг");
            clDay.Items.Add("пятница");
            clDay.Items.Add("суббота");
            clDay.Items.Add("воскресенье");


            for (int i = 0; i < clDay.Items.Count; i++)
            {
                if (Convert.ToString(clDay.Items[i]) == Day)
                {
                    clDay.SetItemChecked(i, true);
                }
            }

            spEndCount.Enabled = false;
            dtEndDate.Enabled = false;
            btnDelete.Enabled = false;
            btnDeleteBusy.Enabled = false;
        }


        

        private void rbDate_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (rbDate.IsChecked)
            {
                dtEndDate.Enabled = true;
            }
            else
            {
                dtEndDate.Enabled = false;
            }
        }

        private void rbCount_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (rbCount.IsChecked)
            {
                spEndCount.Enabled = true;
            }
            else
            {
                spEndCount.Enabled = false;
                
            }
        }

        private void clDay_SelectedItemChanged(object sender, EventArgs e)
        {

            //DayList.Clear();
            //for (int i = 0; i < 6; i++)
            //{
            //    var s = clDay.Items[i].CheckState;
            //    if (clDay.Items.CheckAllItems)
            //    {

            //    }
            //    clDay.Items[i].
            //}
        }

        private void btnCount_Click(object sender, EventArgs e)
        {
            int Interval = Convert.ToInt32(spCount1.Value);
            List<string> WeekDays = new List<string>();
            for (int i = 0; i < clDay.Items.Count; i++) //выбранные дни недели
            {
                if (clDay.GetItemCheckState(i) == CheckState.Checked)
                {
                    WeekDays.Add(Convert.ToString(clDay.Items[i]));
                }
            }
            NewScheduleDateTime.Clear();
            if (rbDate.IsChecked)
            {
                int Days = Convert.ToInt32((dtEndDate.Value - dtDate.Value.AddHours(-dtDate.Value.Hour).AddMinutes(-dtDate.Value.Minute).AddSeconds(-dtDate.Value.Second)).TotalDays); //разница между конечной датой и начальной
                DateTime OneDay = dtDate.Value;
                switch (cbPeriod.Text)
                {
                    case "Час":
                        int HoursInDay = Convert.ToInt32((Convert.ToDateTime(tTimeEnd.Value).Hour - Convert.ToDateTime(tTime.Value).Hour)); //количество часов
                        int numberOfClasses = HoursInDay / (DurationService / 60); //количество возможных занятий в день

                        //DateTime Time = Convert.ToDateTime(tTime.Value);

                        //OneDay = OneDay.AddMinutes(-DurationService);/*.AddHours(Time.Hour).AddMinutes(Time.Minute);*/

                        for (int i = 1; i < Days + 2; i++)
                        {
                            for (int j = 0; j < WeekDays.Count; j++)
                            {
                                if (OneDay.ToString("dddd") == WeekDays[j])/* && (OneDay.Hour >= Convert.ToDateTime(tTime.Value).Hour || OneDay.Hour <= Convert.ToDateTime(tTimeEnd.Value).Hour*/
                                {
                                    if (OneDay.Hour >= Convert.ToDateTime(tTime.Value).Hour)
                                    {
                                        if (OneDay.Hour <= Convert.ToDateTime(tTimeEnd.Value).Hour)
                                        {
                                            for (int x = 0; x < numberOfClasses; x++)
                                            {
                                                NewScheduleDateTime.Add(OneDay);
                                                OneDay = OneDay.AddMinutes(DurationService/* * Convert.ToInt32(spCount1.Value)*/);
                                                
                                            }
                                        }
                                    }
                                   
                                }
                            }
                            OneDay = dtDate.Value.AddDays(i);
                        }
                        
                        break;

                    case "День":
                        OneDay = OneDay.AddDays(1);
                        for (int i = 1; i < Days + 1; i++)
                        {
                            for (int j = 0; j < WeekDays.Count; j++)
                            {
                                if (OneDay.ToString("dddd") == WeekDays[j])
                                {
                                    NewScheduleDateTime.Add(OneDay);
                                }
                            }
                            OneDay = OneDay.AddDays(1);
                        }
                        break;
                }
            }
            else if (rbCount.IsChecked)
            {
                DateTime OneDay = dtDate.Value;
                switch (cbPeriod.Text)
                {
                    case "Час":
                        int HoursInDay = Convert.ToInt32((Convert.ToDateTime(tTimeEnd.Value).Hour - Convert.ToDateTime(tTime.Value).Hour)); //количество часов
                        int numberOfClasses = HoursInDay / (DurationService / 60); //количество возможных занятий в день

                        //DateTime Time = Convert.ToDateTime(tTime.Value);

                        OneDay = OneDay.AddMinutes(-DurationService);/*.AddHours(Time.Hour).AddMinutes(Time.Minute);*/

                        for (int i = 1; i < spEndCount.Value; i++)
                        {
                            for (int j = 0; j < WeekDays.Count; j++)
                            {
                                if (OneDay.ToString("dddd") == WeekDays[j])/* && (OneDay.Hour >= Convert.ToDateTime(tTime.Value).Hour || OneDay.Hour <= Convert.ToDateTime(tTimeEnd.Value).Hour*/
                                {
                                    if (OneDay.Hour >= Convert.ToDateTime(tTime.Value).Hour/* + Convert.ToDecimal(tbDuration.Text) / 60*/)
                                    {
                                        if (OneDay.Hour <= Convert.ToDateTime(tTimeEnd.Value).Hour)
                                        {
                                            for (int x = 0; x < numberOfClasses; x++)
                                            {
                                                if (NewScheduleDateTime.Count < spEndCount.Value)
                                                {
                                                    NewScheduleDateTime.Add(OneDay);
                                                    OneDay = OneDay.AddMinutes(DurationService/* * Convert.ToInt32(spCount1.Value)*/);
                                                    
                                                }
                                                else
                                                {
                                                    break;
                                                }
                                               
                                            }
                                        }
                                    }
                                }
                            }
                            OneDay = dtDate.Value.AddDays(i);
                        }

                        break;

                    case "День":
                        OneDay = OneDay.AddDays(1);
                        for (int i = 1; i < spEndCount.Value; i++)
                        {
                            for (int j = 0; j < WeekDays.Count; j++)
                            {
                                if (OneDay.ToString("dddd") == WeekDays[j])
                                {
                                    NewScheduleDateTime.Add(OneDay);
                                }
                            }
                            OneDay = OneDay.AddDays(1);
                        }
                        break;
                }

            }
            GVSchedule.Rows.Clear();
            tbCount.Text = "0";
            
            for (int i = 0; i < NewScheduleDateTime.Count; i++)
            {

                List<Room> BusyRoom = ScheduleService.ScheduleServiceBusyRooms(NewScheduleDateTime[i], NewScheduleDateTime[i].AddMinutes(Convert.ToInt32(tbDuration.Text)));
                int k = 0;
                for (int j = 0; j < BusyRoom.Count; j++)
                {
                    if (BusyRoom[j].id == ScheduleService.FindByID(MainForm.ID_Schedule).ID_Room )
                    {
                        k++;
                    }
                }
                if (k == 0)
                {
                    GVSchedule.Rows.Add(NewScheduleDateTime[i].ToString("dddd"), NewScheduleDateTime[i], "свободно");
                }
                else
                {
                    GVSchedule.Rows.Add(NewScheduleDateTime[i].ToString("dddd"), NewScheduleDateTime[i], "занято");
                }

            }
            tbCount.Text = NewScheduleDateTime.Count.ToString();
        }

        private void cbPeriod_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (cbPeriod.Text == "Час")
            {
                lbEnd.Visible = true;
                tTimeEnd.Visible = true;
            }
            else
            {
                lbEnd.Visible = false;
                tTimeEnd.Visible = false;
            }
        }

        private void GVSchedule_Click(object sender, EventArgs e)
        {
            if (GVSchedule.RowCount!= 0)
            {
                int row = GVSchedule.CurrentCell.RowIndex;
                if (GVSchedule.Rows[row].Cells[3].Value != null)
                {
                    if ((bool)GVSchedule.Rows[row].Cells[3].Value == true)
                    {
                        GVSchedule.Rows[row].Cells[3].Value = false;
                    }
                    else
                    {
                        GVSchedule.Rows[row].Cells[3].Value = true;
                        //tbCount.Text = (Convert.ToInt32(tbCount.Text) + 1).ToString();
                    }
                }
                else
                {
                    GVSchedule.Rows[row].Cells[3].Value = true;
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            while (true)
            {
                int k = 0;
                for (int i = 0; i < GVSchedule.RowCount; i++)
                {
                    if (GVSchedule.Rows[i].Cells[3].Value != null)
                    {
                        if ((bool)GVSchedule.Rows[i].Cells[3].Value)
                        {
                            GVSchedule.Rows[i].Delete();
                        }
                    }
                }
                for (int i = 0; i < GVSchedule.RowCount; i++)
                {
                    if (GVSchedule.Rows[i].Cells[3].Value != null)
                    {
                        if ((bool)GVSchedule.Rows[i].Cells[3].Value)
                        {
                            k++;
                        }
                    }
                }
                if (k == 0)
                {
                    break;
                }
            }
        }

        private void btnCreate_Click(object sender, EventArgs e) //СОЗДАТЬ
        {
            ScheduleService scheduleService = new ScheduleService();
            ScheduleService service = ScheduleService.FindByID(MainForm.ID_Schedule);
            MessageBox.Show(
            "Записи, в котрых помещение занято не будут созданы! ",
            "Внимание!",
            MessageBoxButtons.OK,
            MessageBoxIcon.Exclamation,
            MessageBoxDefaultButton.Button1,
            MessageBoxOptions.DefaultDesktopOnly);
            for (int i = 0; i < GVSchedule.RowCount; i++)
            {
                if (GVSchedule.Rows[i].Cells[2].Value.ToString() == "свободно")
                {
                    scheduleService.Add(service.ID_Service, service.ID_Room, Convert.ToDateTime(GVSchedule.Rows[i].Cells[1].Value), service.NumberOfPeople, service.Duration, service.Cost);
                }

            }
            this.Close();
        }

        private void btnDeleteBusy_Click(object sender, EventArgs e)
        {
            while(true)
            {
                int k = 0;
                for (int i = 0; i < GVSchedule.RowCount; i++)
                {
                    if (GVSchedule.Rows[i].Cells[2].Value.ToString() == "занято")
                    {
                        GVSchedule.Rows[i].Delete();
                    }
                }
                for (int i = 0; i < GVSchedule.RowCount; i++)
                {
                    if (GVSchedule.Rows[i].Cells[2].Value.ToString() == "занято")
                    {
                        k++;
                    }
                }
                if (k == 0)
                {
                    break;
                }
                
            }
        }

        private void GVSchedule_RowsChanged(object sender, Telerik.WinControls.UI.GridViewCollectionChangedEventArgs e)
        {
            if (GVSchedule.RowCount > 0)
            {
                btnDelete.Enabled = true;
                btnDeleteBusy.Enabled = true;
            }
            else
            {
                btnDelete.Enabled = false;
                btnDeleteBusy.Enabled = false;
            }
        }
    }
}
