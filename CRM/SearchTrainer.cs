﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace CRM
{
    public partial class SearchTrainer : Telerik.WinControls.UI.RadForm
    {
        public SearchTrainer()
        {
            InitializeComponent();
        }

        public static List<Trainer> FilterTrainer = new List<Trainer>();

        private void btnClear_Click(object sender, EventArgs e)
        {
            tbSurname.Text = "";
            tbName.Text = "";
            tbMiddleName.Text = "";
            tbPhone.Text = "";
        }

        private void btnSearch_Click(object sender, EventArgs e) //поиск тренеров
        {
            string Surname = tbSurname.Text;
            string Name = tbName.Text;
            string MiddleName = tbMiddleName.Text;
            string Phone = tbPhone.Text;
            int p = -1;
            MainForm.TrainerList = Trainer.GetAll();
            FilterTrainer.Clear();
            for (int i = 0; i < MainForm.TrainerList.Count; i++) //совпадения по фио и нмеру телефона
            {
                p = MainForm.TrainerList[i].surname.ToLower().IndexOf(Surname.ToLower());
                if (p > -1)
                {
                    p = -1;
                    p = MainForm.TrainerList[i].name.ToLower().IndexOf(Name.ToLower());
                    if (p > -1)
                    {
                        p = -1;
                        p = MainForm.TrainerList[i].middleName.ToLower().IndexOf(MiddleName.ToLower());
                        if (p > -1)
                        {
                            p = -1;
                            p = MainForm.TrainerList[i].phone.ToLower().IndexOf(Phone.ToLower());
                            if (p > -1)
                            {
                                FilterTrainer.Add(MainForm.TrainerList[i]);
                                p = -1;
                            }
                        }
                    }
                }
            }
            MainForm.TrainerList.Clear();
            if (FilterTrainer.Count != 0)
            {
                for (int i = 0; i < FilterTrainer.Count; i++)
                {
                    MainForm.TrainerList.Add(FilterTrainer[i]);
                }
            }
            else if (MainForm.TrainerList.Count == 0)
            {
                MessageBox.Show(
                "Совпадений не найдено",
                "Результат поиска",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information,
                MessageBoxDefaultButton.Button1,
                MessageBoxOptions.DefaultDesktopOnly);
                MainForm.TrainerList = Trainer.GetAll();
            }
            this.Close();
        }
    }
}
