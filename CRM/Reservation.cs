﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM
{
    public class Reservation
    {
        public int ID { get; set; }
        public int ID_ScheduleService { get; set; }
        public int ID_Card { get; set; }
        public int ID_User { get; set; }
        public string DateDelete { get; set; }


        public int AddByCard(int ID_ScheduleService, int ID_Card)
        {
            int ID_User = Card.FindByID(ID_Card).id_User;
            using (ApplicationContext db = new ApplicationContext())
            {
                db.Database.EnsureCreated();
                Reservation Reservation = new Reservation()
                {
                    ID_ScheduleService = ID_ScheduleService,
                    ID_Card = ID_Card,
                    ID_User = ID_User
                };
                db.Reservations.Add(Reservation);
                db.SaveChanges();
                int ID = Reservation.ID;
                return ID;
            }
        }

        
        public int AddByUser(int ID_ScheduleService, int ID_User)
        {
            //int ID_User = Card.FindByID(ID_Card).id_User;
            using (ApplicationContext db = new ApplicationContext())
            {
                db.Database.EnsureCreated();
                Reservation Reservation = new Reservation()
                {
                    ID_ScheduleService = ID_ScheduleService,
                    //ID_Card = ID_Card,
                    ID_User = ID_User
                };
                db.Reservations.Add(Reservation);
                db.SaveChanges();
                int ID = Reservation.ID;
                return ID;
            }
        }


        public static List<Reservation> FindByIDSchudule(int ID_ScheduleService)
        {
            ApplicationContext db = new ApplicationContext();

            List<Reservation> ReservationList = db.Reservations
                    .Where(x => x.DateDelete == null && x.ID_ScheduleService == ID_ScheduleService)
                    .Select(x => new Reservation
                    {
                        ID = x.ID,
                        ID_ScheduleService = x.ID_ScheduleService,
                        ID_Card = x.ID_Card,
                        ID_User = x.ID_User
                    }
                    ).ToList();
            return ReservationList;
        }

        public static Reservation FindByIDSchuduleAndIDClient(int ID_ScheduleService, int ID_User)
        {
            ApplicationContext db = new ApplicationContext();
            Reservation Reservation = db.Reservations.Where(x => x.DateDelete == null && x.ID_ScheduleService == ID_ScheduleService && x.ID_User == ID_User).FirstOrDefault();
            return Reservation;
        }

        public static List<ScheduleService> FindReservationScheduleNowByIDClient(int ID_User)
        {
            List<ScheduleService> schedule = ScheduleService.ScheduleServiceNow();

            List<Reservation> Reservation;
            List<Reservation> ReservationClient = new List<Reservation>();
            for (int i = 0; i < schedule.Count; i++)
            {
                Reservation = FindByIDSchudule(schedule[i].ID);
                for (int j = 0; j < Reservation.Count; j++)
                {
                    if (Reservation[j].ID_User == ID_User && Reservation[j].DateDelete == null)
                    {
                        ReservationClient.Add(Reservation[j]);
                    }
                }
            }
            List<ScheduleService> ScheduleClient = new List<ScheduleService>();
            for (int i = 0; i < ReservationClient.Count; i++)
            {
                ScheduleClient.Add(ScheduleService.FindByID(ReservationClient[i].ID_ScheduleService));
            }
            return ScheduleClient;
        }






        public static Reservation FindByID(int ID_Reservation)
        {
            ApplicationContext db = new ApplicationContext();
            Reservation Reservation = db.Reservations.Find(ID_Reservation);
            return Reservation;
        }

        public static void Del(int ID)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                db.Database.EnsureCreated();
                Reservation Reservation = db.Reservations.Where(c => c.ID == ID).FirstOrDefault();
                Reservation.DateDelete = DateTime.Today.ToString();

                db.SaveChanges();
            }
        }
    }
}
