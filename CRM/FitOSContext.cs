﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.EntityFrameworkCore;

using MySql.Data.EntityFrameworkCore.Extensions;



namespace CRM
{
    public partial class ApplicationContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<Tariff> Tariffs { get; set; }
        public DbSet<ServiceInTariff> ServicesInTariffs { get; set; }
        public DbSet<Card> Cards { get; set; }
        public DbSet<TrainerService> TrainerServices { get; set; }
        public DbSet<BalanceCard> BalanceCards { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<ServiceInRoom> ServiceInRooms { get; set; }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<ScheduleService> ScheduleServices { get; set; }
        public DbSet<TrainerInSchedule> TrainersInSchedule { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Reservation> Reservations { get; set; }
        public DbSet<Visitor_Log> Visitors_Log { get; set; }

        public DbSet<Options> Options { get; set; }


        public ApplicationContext()
        {
            Database.EnsureCreated();
        }

        public static void Check()
        {
            DbConnection db = new MySql.Data.MySqlClient.MySqlConnection("server=127.0.0.1;;UserId=root;Password='root';Database=FitOS;");
            try
            {
                db.Open();
            }
            catch (SystemException)
            {
                DialogResult result = MessageBox.Show(
              "Невозможно подключиться к базе данных!" + "\n\n\n\n" + "Подключиться заново?"
              + "\n\n\n\n\n\n" + "*(Структура базы данных находится по адресу: " + Environment.CurrentDirectory + "\\fitos.sql)",
              "Ошибка",
              MessageBoxButtons.YesNo,
              MessageBoxIcon.Stop,
              MessageBoxDefaultButton.Button1,
              MessageBoxOptions.DefaultDesktopOnly);
                if (result == DialogResult.Yes)
                {
                    Check();
                }
                else
                {
                    Environment.Exit(0);
                }
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ServiceInTariff>().HasKey(u => new { u.ID_Service, u.ID_Tariff });
            modelBuilder.Entity<TrainerService>().HasKey(u => new { u.ID_Service, u.ID_Trainer });
            modelBuilder.Entity<BalanceCard>().HasKey(u => new { u.ID_Service, u.ID_Card });
            modelBuilder.Entity<ServiceInRoom>().HasKey(u => new { u.ID_Service, u.ID_Room });
            modelBuilder.Entity<TrainerInSchedule>().HasKey(u => new { u.ID_Trainer, u.ID_Schedule });
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySQL("server=127.0.0.1;;UserId=root;Password='root';Database=FitOS;");
        }
    }
}
