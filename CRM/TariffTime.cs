﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace CRM
{
    public partial class TariffTime : Telerik.WinControls.UI.RadForm
    {
        public TariffTime()
        {
            InitializeComponent();
        }
        public static List<TimeInTariff> timeInTariffs = new List<TimeInTariff>();

        private void TariffTime_Load(object sender, EventArgs e)
        {
            CreateGV();
        }


        private void CreateGV()
        {
            GVTime.Rows.Add("06:00-07:00");
            GVTime.Rows.Add("07:00-08:00");
            GVTime.Rows.Add("08:00-09:00");
            GVTime.Rows.Add("09:00-10:00");
            GVTime.Rows.Add("10:00-11:00");
            GVTime.Rows.Add("11:00-12:00");
            GVTime.Rows.Add("12:00-13:00");
            GVTime.Rows.Add("13:00-14:00");
            GVTime.Rows.Add("14:00-15:00");
            GVTime.Rows.Add("15:00-16:00");
            GVTime.Rows.Add("16:00-17:00");
            GVTime.Rows.Add("17:00-18:00");
            GVTime.Rows.Add("18:00-19:00");
            GVTime.Rows.Add("19:00-20:00");
            GVTime.Rows.Add("20:00-21:00");
            GVTime.Rows.Add("21:00-22:00");
            GVTime.Rows.Add("22:00-23:00");
            GVTime.Rows.Add("23:00-00:00");
           
            for (int col = 1; col < GVTime.ColumnCount; col++)
            {
                for (int row = 0; row < GVTime.RowCount; row++)
                {
                    for (int i = 0; i < timeInTariffs.Count; i++)
                    {
                        if (timeInTariffs[i].WeekDay == GVTime.Columns[col].HeaderText)
                        {
                            for (int j = 0; j < timeInTariffs[i].Time.Length; j++)
                            {
                                //for (int c = 0; c < GVTime.RowCount; c++)
                                //{
                                    if (GVTime.Rows[row].Cells[0].Value.ToString() == timeInTariffs[i].Time[j])
                                    {
                                        GVTime.Rows[row].Cells[col].Value = true;
                                    }
                                //}

                               
                            }
                        }
                    }
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            List<TimeInTariff> timeInTariffs = new List<TimeInTariff>();
            
            for (int i = 1; i < GVTime.ColumnCount; i++)
            {
                string WeekDay = "";
                WeekDay = GVTime.Columns[i].HeaderText;
                int k = 0;

                for (int j = 0; j < GVTime.RowCount; j++)
                {
                    if (GVTime.Rows[j].Cells[i].Value != null)
                    {
                        if ((bool)GVTime.Rows[j].Cells[i].Value == true)
                        {
                            k++;
                        }
                    }
                }
                string[] Times = new string[k];
                int x = 0;
                for (int j = 0; j < GVTime.RowCount; j++)
                {
                    if (GVTime.Rows[j].Cells[i].Value != null)
                    {
                        if ((bool)GVTime.Rows[j].Cells[i].Value == true)
                        {
                            Times[x] = GVTime.Rows[j].Cells[0].Value.ToString();
                            x = x + 1;
                        }
                    }
                }
                timeInTariffs.Add(new TimeInTariff(WeekDay, Times));
            }
            Tariff.SaveTimeInTariff(MainForm.ID_Tariff, timeInTariffs);
            this.Close();
        }


        private void GVTime_Click(object sender, EventArgs e)
        {
            int row = GVTime.CurrentCell.RowIndex;
            int cell = GVTime.CurrentCell.ColumnIndex;
            if (cell != 0)
            {
                if (GVTime.Rows[row].Cells[cell].Value != null)
                {
                    if ((bool)GVTime.Rows[row].Cells[cell].Value == true)
                    {
                        GVTime.Rows[row].Cells[cell].Value = false;
                    }
                    else
                    {
                        GVTime.Rows[row].Cells[cell].Value = true;
                    }
                }
                else
                {
                    GVTime.Rows[row].Cells[cell].Value = true;
                }
            }
           
        }
    }
}
