﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace CRM
{
    public partial class SelectClient : Telerik.WinControls.UI.RadForm
    {
        public SelectClient()
        {
            InitializeComponent();
        }

        //public static bool StateAdd = false;// истина - если объекты добавляются, ложь - если удаляются

        public static Client ClientSelect;
        public static List<Client> ClientInGV = new List<Client>();

        public bool StateEndVisit = false;

        private void search_Click(object sender, EventArgs e)
        {
            SearchClients searchClients = new SearchClients();
            searchClients.ShowDialog();
            CreateGVFilter();
        }

        //private void del_Click(object sender, EventArgs e)
        //{
        //    SearchClients searchClients = new SearchClients();
        //    searchClients.ShowDialog();
        //}

        private void SelectClient_Load(object sender, EventArgs e)
        {
            if (StateEndVisit)
            {
                CreateGVForEndVisit();
                search.Visible = false;
            }
            else
            {
                CreateGV();
            }
            
        }

        private void CreateGV()
        {
            List<Client> ClientsList = Client.GetAll();
            GVClients.Rows.Clear();
            int p = 0;
            for (int i = 0; i < ClientsList.Count; i++)
            {
                for (int x = 0; x < ClientInGV.Count; x++)
                {
                    if (ClientsList[i].id == ClientInGV[x].id)
                    {
                        p++;
                    }
                }
                if (p == 0)
                {
                    GVClients.Rows.Add(ClientsList[i].id, ClientsList[i].surname, ClientsList[i].name, ClientsList[i].middleName);
                }
                p = 0;
            }
        }

        private void CreateGVFilter()
        {
            List<Client> ClientsList = MainForm.ClientList;
            GVClients.Rows.Clear();
            int p = 0;
            for (int i = 0; i < ClientsList.Count; i++)
            {
                for (int x = 0; x < ClientInGV.Count; x++)
                {
                    if (ClientsList[i].id == ClientInGV[x].id)
                    {
                        p++;
                    }
                }
                if (p == 0)
                {
                    GVClients.Rows.Add(ClientsList[i].id, ClientsList[i].surname, ClientsList[i].name, ClientsList[i].middleName);
                }
                p = 0;
            }
            MainForm.ClientList = Client.GetAll();
        }





        private void CreateGVForEndVisit()
        {
            List<Client> ClientsList = Visitor_Log.GetClientForEndVisit();
            GVClients.Rows.Clear();
            int p = 0;
            for (int i = 0; i < ClientsList.Count; i++)
            {
                for (int x = 0; x < ClientInGV.Count; x++)
                {
                    if (ClientsList[i].id == ClientInGV[x].id)
                    {
                        p++;
                    }
                }
                if (p == 0)
                {
                    GVClients.Rows.Add(ClientsList[i].id, ClientsList[i].surname, ClientsList[i].name, ClientsList[i].middleName);
                }
                p = 0;
            }
        }




        private void btnSelect_Click(object sender, EventArgs e)
        {
            ClientSelect = new Client();
            int d = 0;

            for (int i = 0; i < GVClients.RowCount; i++)
            {
                if (GVClients.Rows[i].IsSelected)
                {
                    d = Convert.ToInt32(GVClients.Rows[i].Cells[0].Value);
                }
            }
            //int row = GVClients.CurrentCell.RowIndex;
            //int d = Convert.ToInt32(GVClients.Rows[row].Cells[0].Value);
            ClientSelect = Client.FindByID(Convert.ToInt32(d));
            this.Close();


            //ClientSelect = new Client();
            //for (int i = 0; i < GVClients.RowCount; i++)
            //{
            //    if (GVClients.Rows[i].Cells[4].Value != null)
            //    {
            //        ClientSelect = Client.FindByID(Convert.ToInt32((string)GVClients.Rows[i].Cells[0].Value));
            //        //TrainerCard.ServiceInTariffList.Add(id (int)GVServices.Rows[i].Cells[0].Value, (string)GVServices.Rows[i].Cells[1].Value);
            //        //ServiceList.Add(service);
            //    }
            //}
            //this.Close();
        }
    }
}
