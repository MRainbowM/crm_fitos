﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace CRM
{
    public partial class ServiceInTariffForm : Telerik.WinControls.UI.RadForm
    {
        //TariffCard tariffCard;
        public ServiceInTariffForm()
        {
            InitializeComponent();

        }

        public static int ID_Service;
        public static string ServiceName;
        public static int Amount;
        public static int Periodicity;

        public static bool StateSave = false;


        private void ServiceInTariff_Load(object sender, EventArgs e)
        {
            if (StateSave)
            {
                cmbServices.Enabled = false;
                cmbServices.Text = Service.FindByID(ID_Service).name;
                spAmount.Value = ServiceInTariff.FindByIDServiceAndIDTariff(TariffCard.ID_Tariff, ID_Service).Amount;
                spPeriodicity.Value = ServiceInTariff.FindByIDServiceAndIDTariff(TariffCard.ID_Tariff, ID_Service).Periodicity;
                btnAddService.Visible = false;
                saveService.Visible = true;
            }
            else
            {
                CreateCMB();
                btnAddService.Visible = true;
                saveService.Visible = false;
            }
        }

        private void CreateCMB()
        {
            cmbServices.Items.Clear();
            cmbServices.Enabled = true;
            string ServiceN;
            string ServiceN2;
            List<Service> Services = Service.GetAll();
            for (int i = 0; i < Services.Count; i++)
            {
                ServiceN = Services[i].name;
                if (TariffCard.ServiceInGV.Count != 0)
                {
                    int k = 0;
                    for (int x = 0; x < TariffCard.ServiceInGV.Count; x++)
                    {
                        ServiceN2 = TariffCard.ServiceInGV[x].name;
                        if (ServiceN == ServiceN2)
                        {
                            k++;
                            break;
                        }
                    }
                    if (k == 0)
                    {
                        cmbServices.Items.Add(Services[i].name);
                    }
                }
                else
                {
                    cmbServices.Items.Add(Services[i].name);
                }
               
            }
            if (cmbServices.Items.Count == 0)
            {
                MessageBox.Show(
                "Все возможные услуги включены в тариф",
                "Ошибка добавления услуги",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information,
                MessageBoxDefaultButton.Button1,
                MessageBoxOptions.DefaultDesktopOnly);
                this.Close();
            }
            cmbServices.SelectedIndex = 0;
        }

        //private int GetID(string Name)
        //{
        //    int ID = 0;
        //    List<Service> Services = Service.GetAll();
        //    for (int i = 0; i < Services.Count; i++)
        //    {
        //        if (Services[i].name == Name)
        //        {
        //            ID = Services[i].id;
        //        }
        //    }
        //    return ID;
        //}


        private void btnAddService_Click(object sender, EventArgs e)
        {
            if (Validation())
            {

                ServiceName = (string)cmbServices.Text;
                Amount = (int)spAmount.Value;
                Periodicity = (int)spPeriodicity.Value;
                ID_Service = Service.FindByName(ServiceName).id;

                this.Close();
            }
        }

        private bool Validation()
        {
            string message = "\n";
            bool v = true;
            if (spAmount.Value == 0) { error.SetError(spAmount, "Заполните поле!"); message += "Количество услуги в тарифе \n"; v = false; }
            if (spPeriodicity.Value == 0) { error.SetError(spPeriodicity, "Заполните поле!"); message += "Периодичность услуги \n"; v = false; }
            if (v == false)
            {
                DialogResult result = MessageBox.Show(
                "Заполните поля: " + message,
                "Ошибка сохранения!",
                MessageBoxButtons.OK,
                MessageBoxIcon.Stop,
                MessageBoxDefaultButton.Button1,
                MessageBoxOptions.DefaultDesktopOnly);
            }
            return v;
        }

        private void saveService_Click(object sender, EventArgs e)
        {
            if (Validation())
            {
                ServiceInTariff.Save(TariffCard.ID_Tariff, ID_Service, (int)spAmount.Value, (int)spPeriodicity.Value);
                this.Close();
            }
        }
    }
}
