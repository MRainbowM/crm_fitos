﻿namespace CRM
{
    partial class PayCard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn1 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.tbClient = new Telerik.WinControls.UI.RadTextBox();
            this.btnSelectClient = new Telerik.WinControls.UI.RadButton();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.tbAmount = new Telerik.WinControls.UI.RadTextBox();
            this.bntSave = new Telerik.WinControls.UI.RadButton();
            this.GVService = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSelectClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVService)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVService.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(12, 12);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(42, 18);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "Клиент";
            // 
            // tbClient
            // 
            this.tbClient.Enabled = false;
            this.tbClient.Location = new System.Drawing.Point(13, 37);
            this.tbClient.Name = "tbClient";
            this.tbClient.Size = new System.Drawing.Size(320, 20);
            this.tbClient.TabIndex = 2;
            this.tbClient.TextChanged += new System.EventHandler(this.tbClient_TextChanged);
            // 
            // btnSelectClient
            // 
            this.btnSelectClient.Location = new System.Drawing.Point(339, 35);
            this.btnSelectClient.Name = "btnSelectClient";
            this.btnSelectClient.Size = new System.Drawing.Size(124, 24);
            this.btnSelectClient.TabIndex = 3;
            this.btnSelectClient.Text = "Выбрать клиента";
            this.btnSelectClient.Click += new System.EventHandler(this.selectClient_Click);
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(15, 398);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(40, 18);
            this.radLabel3.TabIndex = 1;
            this.radLabel3.Text = "Сумма";
            // 
            // tbAmount
            // 
            this.tbAmount.Location = new System.Drawing.Point(15, 422);
            this.tbAmount.Name = "tbAmount";
            this.tbAmount.Size = new System.Drawing.Size(82, 20);
            this.tbAmount.TabIndex = 3;
            // 
            // bntSave
            // 
            this.bntSave.Location = new System.Drawing.Point(339, 418);
            this.bntSave.Name = "bntSave";
            this.bntSave.Size = new System.Drawing.Size(124, 24);
            this.bntSave.TabIndex = 4;
            this.bntSave.Text = "Оплатить";
            this.bntSave.Click += new System.EventHandler(this.bntSave_Click);
            // 
            // GVService
            // 
            this.GVService.Location = new System.Drawing.Point(15, 65);
            // 
            // 
            // 
            this.GVService.MasterTemplate.AllowAddNewRow = false;
            gridViewDecimalColumn1.HeaderText = "ID";
            gridViewDecimalColumn1.Name = "column3";
            gridViewDecimalColumn1.Width = 30;
            gridViewTextBoxColumn1.HeaderText = "Услуга";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.Width = 250;
            gridViewTextBoxColumn2.HeaderText = "Стоимость";
            gridViewTextBoxColumn2.Name = "column2";
            gridViewTextBoxColumn2.Width = 100;
            gridViewCheckBoxColumn1.HeaderText = "Выбор";
            gridViewCheckBoxColumn1.Name = "Выбор";
            this.GVService.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewDecimalColumn1,
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewCheckBoxColumn1});
            this.GVService.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GVService.Name = "GVService";
            this.GVService.ReadOnly = true;
            this.GVService.Size = new System.Drawing.Size(448, 327);
            this.GVService.TabIndex = 6;
            this.GVService.Click += new System.EventHandler(this.GVService_Click);
            // 
            // PayCard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 454);
            this.Controls.Add(this.GVService);
            this.Controls.Add(this.bntSave);
            this.Controls.Add(this.tbAmount);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.btnSelectClient);
            this.Controls.Add(this.tbClient);
            this.Controls.Add(this.radLabel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "PayCard";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Платеж";
            this.Load += new System.EventHandler(this.PayCard_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSelectClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVService.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVService)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox tbClient;
        private Telerik.WinControls.UI.RadButton btnSelectClient;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTextBox tbAmount;
        private Telerik.WinControls.UI.RadButton bntSave;
        private Telerik.WinControls.UI.RadGridView GVService;
    }
}
