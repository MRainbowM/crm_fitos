﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace CRM
{
    public partial class SearchClients : Telerik.WinControls.UI.RadForm
    {
        public SearchClients()
        {
            InitializeComponent();
        }

        public static List<Client> FilterClient = new List<Client>();

        private void SearchClients_Load(object sender, EventArgs e)
        {
            grSex.Enabled = false;
            grCard.Enabled = false;
            rbWM.IsChecked = true;
            spTo.Value = 90;
            for (int i = 0; i < Tariff.GetAll().Count; i++)
            {
                cbTariff.Items.Add(Tariff.GetAll()[i].name);
            }
            cbTariff.SelectedIndex = 0;
        }

        private void cbSex_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (cbSex.Checked)
            {
                grSex.Enabled = true;
            }
            else
            {
                grSex.Enabled = false;
            }
        }

        private void cbCard_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (cbCard.Checked)
            {
                grCard.Enabled = true;
                tbNCard.Enabled = false;
                cbTariff.Enabled = false;
            }
            else
            {
                grCard.Enabled = false;
            }
        }

        private void rbNCard_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (rbNCard.IsChecked)
            {
                tbNCard.Enabled = true;
            }
            else
            {
                tbNCard.Enabled = false;
            }
        }

        private void rbTariff_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (rbTariff.IsChecked)
            {
                cbTariff.Enabled = true;
            }
            else
            {
                cbTariff.Enabled = false;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            tbNCard.Text = "";
            tbSurname.Text = "";
            tbName.Text = "";
            tbMiddleName.Text = "";
            tbPhone.Text = "";
            spFrom.Value = 0;
            cbCard.Checked = false;
            cbSex.Checked = false;
            rbWM.IsChecked = true;
            spTo.Value = 90;
            rbNCard.IsChecked = false;
            rbTariff.IsChecked = false;

        }

        private void tbPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string Surname = tbSurname.Text;
            string Name = tbName.Text;
            string MiddleName = tbMiddleName.Text;
            string Phone = tbPhone.Text;
            string NCard = tbNCard.Text;
            string Tariff1 = cbTariff.Text;

            decimal From = spFrom.Value;
            decimal To = spTo.Value;

            int p = -1;

            MainForm.ClientList = Client.GetAll();
            FilterClient.Clear();


            for (int i = 0; i < MainForm.ClientList.Count; i++) //совпадения по фио и нмеру телефона
            {
                p = MainForm.ClientList[i].surname.ToLower().IndexOf(Surname.ToLower());
                if (p > -1)
                {
                    p = -1;
                    p = MainForm.ClientList[i].name.ToLower().IndexOf(Name.ToLower());
                    if (p > -1)
                    {
                        p = -1;
                        p = MainForm.ClientList[i].middleName.ToLower().IndexOf(MiddleName.ToLower());
                        if (p > -1)
                        {
                            p = -1;
                            p = MainForm.ClientList[i].phone.ToLower().IndexOf(Phone.ToLower());
                            if (p > -1)
                            {
                                FilterClient.Add(MainForm.ClientList[i]);
                                p = -1;
                            }
                        }
                        
                    }
                }
            }
            List<Card> CardList = new List<Card>();
            List<Client> FilterClient2 = new List<Client>(); 
            for (int i = 0; i < FilterClient.Count; i++)
            {
                if (rbTariff.IsChecked || rbNCard.IsChecked)
                {
                    CardList = Card.GetCardByClient(FilterClient[i].id);
                    p = -1;
                    for (int j = 0; j < CardList.Count; j++)
                    {
                        if (Tariff.FindByID(CardList[j].id_Tariff).name == Tariff1)
                        {
                            p++;
                        }
                    }
                    if (p > -1 && rbTariff.IsChecked)
                    {
                        FilterClient2.Add(FilterClient[i]);
                    }
                    int k = 0;
                    for (int j = 0; j < CardList.Count; j++)
                    {
                        p = -1;
                        p = CardList[j].n_Card.ToLower().IndexOf(NCard.ToLower());
                        if (p > -1)
                        {
                            k++;
                        }
                    }
                    if (k > 0 && rbNCard.IsChecked)
                    {
                        FilterClient2.Add(FilterClient[i]);
                    }
                }
                else
                {
                    FilterClient2.Add(FilterClient[i]);
                }
            }
            List<Client> FilterClient3 = new List<Client>();
            if (cbSex.Checked)
            {
                for (int i = 0; i < FilterClient2.Count; i++)
                {
                    if (spFrom.Value <=  (Convert.ToInt32((DateTime.Now - FilterClient2[i].dob).TotalDays)/365))
                    {
                        if (spTo.Value >= (Convert.ToInt32((DateTime.Now - FilterClient2[i].dob).TotalDays) / 365))
                        {
                            if (rbM.IsChecked)
                            {
                                if (FilterClient2[i].sex == 1)
                                {
                                    FilterClient3.Add(FilterClient[i]);
                                }
                            }
                            if (rbW.IsChecked)
                            {
                                if (FilterClient2[i].sex == 0)
                                {
                                    FilterClient3.Add(FilterClient[i]);
                                }
                            }
                            if (rbWM.IsChecked)
                            {
                                FilterClient3.Add(FilterClient[i]);
                            }
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < FilterClient2.Count; i++)
                {
                    FilterClient3.Add(FilterClient2[i]);
                }
            }
            MainForm.ClientList.Clear();
            if ((FilterClient3.Count != 0 && cbSex.Checked == true) || (FilterClient3.Count != 0 && cbCard.Checked == true))
            {
                for (int i = 0; i < FilterClient3.Count; i++)
                {
                    MainForm.ClientList.Add(FilterClient3[i]);
                }
            }
            if (FilterClient.Count != 0 && cbSex.Checked == false && rbTariff.IsChecked == false && rbNCard.IsChecked == false)
            {
                for (int i = 0; i < FilterClient.Count; i++)
                {
                    MainForm.ClientList.Add(FilterClient[i]);
                }
            }

            else if (MainForm.ClientList.Count == 0)
            {
                MessageBox.Show(
                "Совпадений не найдено",
                "Результат поиска",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information,
                MessageBoxDefaultButton.Button1,
                MessageBoxOptions.DefaultDesktopOnly);
                MainForm.ClientList = Client.GetAll();
            }
            this.Close();
        }

    }
}
