﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace CRM
{
    public partial class TariffCard : Telerik.WinControls.UI.RadForm
    {
        public TariffCard()
        {
            InitializeComponent();
        }

        public bool StateSave = false;//true - тариф уже существует, false - новый тариф

        public static List<Service> ServiceInGV = new List<Service>();

        public static int ID_Tariff;


       private void TariffCard_Load(object sender, EventArgs e)
       {
            spDuration.Minimum = 0;
            spDuration.Maximum = 366;
            spTotalCost.DecimalPlaces = 2;
            spTotalCost.Minimum = 0;
            spTotalCost.Maximum = 100000;

            GVServicesInTariff.Rows.Clear();
            ServiceInTariffForm.ServiceName = "";
            ServiceInTariffForm.Amount = 0;
            ServiceInTariffForm.Periodicity = 0;

            if (StateSave)
            {
                Tariff tariff = Tariff.FindByID(MainForm.ID_Tariff);
                FillForm(tariff);
                ID_Tariff = MainForm.ID_Tariff;
                btnTime.Enabled = true;
            }
            else
            {
                btnTime.Enabled = false;
            }
       }


        private void Autorizatsiya()
        {
            switch (User.AutorizedUser.stateUser)
            {
                case "Client":
                    btnDelete.Visible = false;
                    btnSave.Visible = false;
                    delService.Visible = false;
                    addService.Visible = false;
                    btnSave.Visible = false;

                    tbName.Enabled = false;
                    spDuration.Enabled = false;
                    dtStartDate.Enabled = false;
                    dtExpirationDate.Enabled = false;
                    dtDateRemoved.Enabled = false;
                    spTotalCost.Enabled = false;
                    tbComment.Enabled = false;
                    break;
                case "Manager":
                    break;
                case "Admin":
                    break;
            }
        }



        private void addService_Click(object sender, EventArgs e)
        {
            ServiceInGV.Clear();
            for (int i = 0; i < GVServicesInTariff.RowCount; i++)
            {
                ServiceInGV.Add(Service.FindByID(Convert.ToInt32(GVServicesInTariff.Rows[i].Cells[0].Value)));
            }
            
            ServiceInTariffForm serviceInTariff = new ServiceInTariffForm();
            ServiceInTariffForm.StateSave = false;
            serviceInTariff.ShowDialog();
            AddServiceInGV();
        }

        private void delService_Click(object sender, EventArgs e)
        {
            int row = GVServicesInTariff.CurrentCell.RowIndex;
            int d = Convert.ToInt32(GVServicesInTariff.Rows[row].Cells[0].Value);

            DialogResult result = MessageBox.Show(
            "Удалить услугу " + (String)GVServicesInTariff.Rows[row].Cells[1].Value + "?",
            "Удаление услуги",
            MessageBoxButtons.YesNo,
            MessageBoxIcon.Exclamation,
            MessageBoxDefaultButton.Button1,
            MessageBoxOptions.DefaultDesktopOnly);
            if (result == DialogResult.Yes)
            {
                DeleteService(d);
            }

        }

        private void DeleteService(int ID_Service)
        {
            List<Service> ServiceList = new List<Service>();
            for (int i = 0; i < GVServicesInTariff.RowCount; i++)
            {
                if (Convert.ToInt32(GVServicesInTariff.Rows[i].Cells[0].Value) != ID_Service)
                {
                    ServiceList.Add(Service.FindByID(Convert.ToInt32(GVServicesInTariff.Rows[i].Cells[0].Value)));
                }
            }
            GVServicesInTariff.Rows.Clear();
            for (int i = 0; i < ServiceList.Count; i++)
            {
                GVServicesInTariff.Rows.Add(ServiceList[i].id, ServiceList[i].name, ServiceInTariff.FindByIDServiceAndIDTariff(ID_Tariff, ServiceList[i].id).Amount, ServiceInTariff.FindByIDServiceAndIDTariff(ID_Tariff, ServiceList[i].id).Periodicity);
            }
            if (GVServicesInTariff.RowCount == 0)
            {
                delService.Visible = false;
            }
            using (ApplicationContext db = new ApplicationContext())
            {
                var room = ServiceInTariff.FindByIDServiceAndIDTariff(ID_Tariff, ID_Service);
                if (room != null)
                {
                    ServiceInTariff.Del(ID_Service, ID_Tariff);
                }
            }
        }










        private void FillForm(Tariff tariff)
        {
            tbName.Text = tariff.name;
            spDuration.Value = tariff.duration;
            dtStartDate.Value = tariff.startDate;
            dtExpirationDate.Value = tariff.expirationDate;
            dtDateRemoved.Value = tariff.dateRemoved;
            tbComment.Text = tariff.comment;
            spTotalCost.Value = (decimal)tariff.totalCost;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveServices();
            this.Close();
        }

        private void SaveForm()
        {
            Tariff tariff = new Tariff();
            if (StateSave)
            {
                tariff.Save(MainForm.ID_Tariff, tbName.Text, (int)spDuration.Value, spTotalCost.Value, dtStartDate.Value, dtExpirationDate.Value, dtDateRemoved.Value, tbComment.Text);
            }
            else
            {
                MainForm.ID_Tariff = tariff.Add(tbName.Text, (int)spDuration.Value, spTotalCost.Value, dtStartDate.Value, dtExpirationDate.Value, dtDateRemoved.Value, tbComment.Text);
                ID_Tariff = MainForm.ID_Tariff;
                StateSave = true;
                btnTime.Enabled = true;
            }
            MessageBox.Show(
            "Изменения успешно сохранены",
            "Результат сохранения",
            MessageBoxButtons.OK,
            MessageBoxIcon.Information,
            MessageBoxDefaultButton.Button1,
            MessageBoxOptions.DefaultDesktopOnly);
            error.Clear();
            //this.Close();
        }

        private bool Validation() //true - все норм false - ошибки
        {
            string message = "\n";
            bool v = true;
            if (tbName.Text == "") { error.SetError(tbName, "Заполните поле!"); message += "Название \n"; v = false; }
            if (spTotalCost.Value == 0) { error.SetError(spTotalCost, "Заполните поле!"); message += "Стоимость \n"; v = false; }
            if (spTotalCost.Value == 0) { error.SetError(spTotalCost, "Заполните поле!"); message += "Длительность тарифа \n"; v = false; }

            if (v == false)
            {
                DialogResult result = MessageBox.Show(
                "Заполните поля: " + message,
                "Ошибка сохранения!",
                MessageBoxButtons.OK,
                MessageBoxIcon.Stop,
                MessageBoxDefaultButton.Button1,
                MessageBoxOptions.DefaultDesktopOnly);
            }
            return v;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(
            "Удалить тариф?",
            "Удаление тарифа " + tbName.Text,
            MessageBoxButtons.YesNo,
            MessageBoxIcon.Exclamation,
            MessageBoxDefaultButton.Button1,
            MessageBoxOptions.DefaultDesktopOnly);
            if (result == DialogResult.Yes)
            {
                Tariff.Del(MainForm.ID_Tariff);
                this.Close();
            }
        }

        private void PVTariffCard_SelectedPageChanged(object sender, EventArgs e)
        {
            if (PVTariffCard.SelectedPage == pvServices)
            {
                if (StateSave)
                {
                    CreateGV();
                }
            }
        }

        private void CreateGV()
        {
            List<ServiceInTariff> ServiceList = ServiceInTariff.GetServices(MainForm.ID_Tariff);
            GVServicesInTariff.Rows.Clear();

            for (int i = 0; i <= ServiceList.Count - 1; i++)
            {
                GVServicesInTariff.Rows.Add(ServiceList[i].ID_Service, Service.FindByID(ServiceList[i].ID_Service).name, ServiceList[i].Amount, ServiceList[i].Periodicity);
            }
        }

        private void TariffCard_Activated(object sender, EventArgs e)
        {
            //AddServiceInGV();
        }

        private void AddServiceInGV()
        {
            int p = 0;
            if (GVServicesInTariff.RowCount != 0)
            {
                for (int i = 0; i < GVServicesInTariff.RowCount; i++)
                {
                    if ((string)GVServicesInTariff.Rows[i].Cells[1].Value == ServiceInTariffForm.ServiceName && ServiceInTariffForm.ServiceName != null && ServiceInTariffForm.ServiceName != "")
                    {
                        p++;
                    }
                }
                if (p == 0 && ServiceInTariffForm.ServiceName != null && ServiceInTariffForm.ServiceName != "")
                {
                    GVServicesInTariff.Rows.Add(ServiceInTariffForm.ID_Service, ServiceInTariffForm.ServiceName, ServiceInTariffForm.Amount, ServiceInTariffForm.Periodicity);
                    ServiceInTariffForm.ServiceName = "";
                }
            }

            else
            {
                if (ServiceInTariffForm.ServiceName != null && ServiceInTariffForm.ServiceName != "")
                {
                    GVServicesInTariff.Rows.Add(ServiceInTariffForm.ID_Service, ServiceInTariffForm.ServiceName, ServiceInTariffForm.Amount, ServiceInTariffForm.Periodicity);
                    ServiceInTariffForm.ServiceName = "";
                }
            }
           
        }


        private void btnSaveService_Click(object sender, EventArgs e)
        {
            SaveServices();
            this.Close();
        }
        private void SaveServices()
        {
            if (Validation())
            {
                SaveForm();
                using (ApplicationContext db = new ApplicationContext())
                {
                    int ID_Service;
                    int Amount;
                    int Periodicity;
                    for (int i = 0; i < GVServicesInTariff.RowCount; i++)
                    {
                        ID_Service = Service.FindByName((string)GVServicesInTariff.Rows[i].Cells[1].Value).id;
                        Amount = Convert.ToInt32((string)GVServicesInTariff.Rows[i].Cells[2].Value);
                        Periodicity = Convert.ToInt32((string)GVServicesInTariff.Rows[i].Cells[3].Value);
                        var service = db.ServicesInTariffs
                        .Where(x => x.DateDelete == null && x.ID_Tariff == MainForm.ID_Tariff && x.ID_Service == ID_Service).FirstOrDefault();
                        if (service == null)
                        {
                            ServiceInTariff.Add(MainForm.ID_Tariff, ID_Service, Amount, Periodicity);
                        }
                    }
                }
            }
        }

        private void GVServicesInTariff_CellDoubleClick(object sender, Telerik.WinControls.UI.GridViewCellEventArgs e)
        {
            ServiceInTariffForm serviceInTariff = new ServiceInTariffForm();
            ServiceInTariffForm.StateSave = true;
            SaveServices();
            int row = GVServicesInTariff.CurrentCell.RowIndex;
            ServiceInTariffForm.ID_Service = Convert.ToInt32(GVServicesInTariff.Rows[row].Cells[0].Value);
            serviceInTariff.ShowDialog();
            CreateGV();
        }

        private void btnTime_Click(object sender, EventArgs e)
        {
            TariffTime tariffTime = new TariffTime();
            TariffTime.timeInTariffs = Tariff.FindByID(MainForm.ID_Tariff).Times;
            tariffTime.ShowDialog();
        }

        private void dtStartDate_ValueChanged(object sender, EventArgs e)
        {
            dtDateRemoved.MinDate = dtStartDate.Value;
            dtExpirationDate.MinDate = dtStartDate.Value; ;
        }
    }
}
