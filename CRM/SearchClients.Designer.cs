﻿namespace CRM
{
    partial class SearchClients
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbSurname = new Telerik.WinControls.UI.RadTextBox();
            this.tbName = new Telerik.WinControls.UI.RadTextBox();
            this.tbMiddleName = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.spFrom = new Telerik.WinControls.UI.RadSpinEditor();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.grSex = new Telerik.WinControls.UI.RadGroupBox();
            this.rbWM = new Telerik.WinControls.UI.RadRadioButton();
            this.rbW = new Telerik.WinControls.UI.RadRadioButton();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.rbM = new Telerik.WinControls.UI.RadRadioButton();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.spTo = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.grCard = new Telerik.WinControls.UI.RadGroupBox();
            this.cbTariff = new Telerik.WinControls.UI.RadDropDownList();
            this.rbTariff = new Telerik.WinControls.UI.RadRadioButton();
            this.rbNCard = new Telerik.WinControls.UI.RadRadioButton();
            this.tbNCard = new Telerik.WinControls.UI.RadTextBox();
            this.btnSearch = new Telerik.WinControls.UI.RadButton();
            this.radGroupBox4 = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.tbPhone = new Telerik.WinControls.UI.RadTextBox();
            this.btnClear = new Telerik.WinControls.UI.RadButton();
            this.cbSex = new Telerik.WinControls.UI.RadCheckBox();
            this.cbCard = new Telerik.WinControls.UI.RadCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.tbSurname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMiddleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grSex)).BeginInit();
            this.grSex.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbWM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grCard)).BeginInit();
            this.grCard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbTariff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbTariff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbNCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox4)).BeginInit();
            this.radGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tbSurname
            // 
            this.tbSurname.Location = new System.Drawing.Point(5, 46);
            this.tbSurname.Name = "tbSurname";
            this.tbSurname.Size = new System.Drawing.Size(183, 20);
            this.tbSurname.TabIndex = 1;
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(5, 96);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(183, 20);
            this.tbName.TabIndex = 2;
            // 
            // tbMiddleName
            // 
            this.tbMiddleName.Location = new System.Drawing.Point(5, 146);
            this.tbMiddleName.Name = "tbMiddleName";
            this.tbMiddleName.Size = new System.Drawing.Size(183, 20);
            this.tbMiddleName.TabIndex = 3;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(5, 21);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(53, 18);
            this.radLabel1.TabIndex = 4;
            this.radLabel1.Text = "Фамилия";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(5, 72);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(28, 18);
            this.radLabel2.TabIndex = 5;
            this.radLabel2.Text = "Имя";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(5, 122);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(54, 18);
            this.radLabel3.TabIndex = 6;
            this.radLabel3.Text = "Отчество";
            // 
            // spFrom
            // 
            this.spFrom.Location = new System.Drawing.Point(5, 46);
            this.spFrom.Name = "spFrom";
            this.spFrom.Size = new System.Drawing.Size(63, 20);
            this.spFrom.TabIndex = 7;
            this.spFrom.TabStop = false;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Controls.Add(this.tbSurname);
            this.radGroupBox1.Controls.Add(this.tbName);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.tbMiddleName);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.HeaderText = "ФИО";
            this.radGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(197, 177);
            this.radGroupBox1.TabIndex = 8;
            this.radGroupBox1.Text = "ФИО";
            // 
            // grSex
            // 
            this.grSex.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.grSex.Controls.Add(this.rbWM);
            this.grSex.Controls.Add(this.rbW);
            this.grSex.Controls.Add(this.radLabel6);
            this.grSex.Controls.Add(this.rbM);
            this.grSex.Controls.Add(this.radLabel7);
            this.grSex.Controls.Add(this.spTo);
            this.grSex.Controls.Add(this.radLabel5);
            this.grSex.Controls.Add(this.spFrom);
            this.grSex.HeaderText = "Возраст|Пол";
            this.grSex.Location = new System.Drawing.Point(215, 12);
            this.grSex.Name = "grSex";
            this.grSex.Size = new System.Drawing.Size(200, 92);
            this.grSex.TabIndex = 9;
            this.grSex.Text = "Возраст|Пол";
            // 
            // rbWM
            // 
            this.rbWM.Location = new System.Drawing.Point(125, 72);
            this.rbWM.Name = "rbWM";
            this.rbWM.Size = new System.Drawing.Size(57, 18);
            this.rbWM.TabIndex = 11;
            this.rbWM.Text = "Любой";
            // 
            // rbW
            // 
            this.rbW.Location = new System.Drawing.Point(85, 72);
            this.rbW.Name = "rbW";
            this.rbW.Size = new System.Drawing.Size(30, 18);
            this.rbW.TabIndex = 10;
            this.rbW.Text = "Ж";
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(125, 21);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(21, 18);
            this.radLabel6.TabIndex = 7;
            this.radLabel6.Text = "До";
            // 
            // rbM
            // 
            this.rbM.Location = new System.Drawing.Point(38, 72);
            this.rbM.Name = "rbM";
            this.rbM.Size = new System.Drawing.Size(30, 18);
            this.rbM.TabIndex = 9;
            this.rbM.Text = "М";
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(5, 72);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(27, 18);
            this.radLabel7.TabIndex = 8;
            this.radLabel7.Text = "Пол";
            // 
            // spTo
            // 
            this.spTo.Location = new System.Drawing.Point(125, 45);
            this.spTo.Name = "spTo";
            this.spTo.Size = new System.Drawing.Size(63, 20);
            this.spTo.TabIndex = 8;
            this.spTo.TabStop = false;
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(5, 21);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(19, 18);
            this.radLabel5.TabIndex = 6;
            this.radLabel5.Text = "От";
            // 
            // grCard
            // 
            this.grCard.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.grCard.Controls.Add(this.cbTariff);
            this.grCard.Controls.Add(this.rbTariff);
            this.grCard.Controls.Add(this.rbNCard);
            this.grCard.Controls.Add(this.tbNCard);
            this.grCard.HeaderText = "Карта";
            this.grCard.Location = new System.Drawing.Point(12, 195);
            this.grCard.Name = "grCard";
            this.grCard.Size = new System.Drawing.Size(403, 82);
            this.grCard.TabIndex = 9;
            this.grCard.Text = "Карта";
            // 
            // cbTariff
            // 
            this.cbTariff.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbTariff.Location = new System.Drawing.Point(208, 47);
            this.cbTariff.Name = "cbTariff";
            this.cbTariff.Size = new System.Drawing.Size(183, 20);
            this.cbTariff.TabIndex = 13;
            this.cbTariff.Text = "radDropDownList1";
            // 
            // rbTariff
            // 
            this.rbTariff.Location = new System.Drawing.Point(208, 25);
            this.rbTariff.Name = "rbTariff";
            this.rbTariff.Size = new System.Drawing.Size(53, 18);
            this.rbTariff.TabIndex = 12;
            this.rbTariff.Text = "Тариф";
            this.rbTariff.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbTariff_ToggleStateChanged);
            // 
            // rbNCard
            // 
            this.rbNCard.Location = new System.Drawing.Point(5, 25);
            this.rbNCard.Name = "rbNCard";
            this.rbNCard.Size = new System.Drawing.Size(89, 18);
            this.rbNCard.TabIndex = 11;
            this.rbNCard.Text = "Номер карты";
            this.rbNCard.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.rbNCard.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbNCard_ToggleStateChanged);
            // 
            // tbNCard
            // 
            this.tbNCard.Location = new System.Drawing.Point(5, 46);
            this.tbNCard.Name = "tbNCard";
            this.tbNCard.Size = new System.Drawing.Size(183, 20);
            this.tbNCard.TabIndex = 1;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(305, 283);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(110, 24);
            this.btnSearch.TabIndex = 10;
            this.btnSearch.Text = "Поиск";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // radGroupBox4
            // 
            this.radGroupBox4.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox4.Controls.Add(this.radLabel4);
            this.radGroupBox4.Controls.Add(this.tbPhone);
            this.radGroupBox4.HeaderText = "Номер телефона";
            this.radGroupBox4.Location = new System.Drawing.Point(215, 110);
            this.radGroupBox4.Name = "radGroupBox4";
            this.radGroupBox4.Size = new System.Drawing.Size(200, 79);
            this.radGroupBox4.TabIndex = 12;
            this.radGroupBox4.Text = "Номер телефона";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(5, 24);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(51, 18);
            this.radLabel4.TabIndex = 4;
            this.radLabel4.Text = "Телефон";
            // 
            // tbPhone
            // 
            this.tbPhone.Location = new System.Drawing.Point(5, 48);
            this.tbPhone.Name = "tbPhone";
            this.tbPhone.Size = new System.Drawing.Size(183, 20);
            this.tbPhone.TabIndex = 1;
            this.tbPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPhone_KeyPress);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(12, 283);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(110, 24);
            this.btnClear.TabIndex = 14;
            this.btnClear.Text = "Очистить";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // cbSex
            // 
            this.cbSex.Location = new System.Drawing.Point(220, 9);
            this.cbSex.Name = "cbSex";
            this.cbSex.Size = new System.Drawing.Size(84, 18);
            this.cbSex.TabIndex = 15;
            this.cbSex.Text = "Возраст|Пол";
            this.cbSex.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.cbSex_ToggleStateChanged);
            // 
            // cbCard
            // 
            this.cbCard.Location = new System.Drawing.Point(17, 195);
            this.cbCard.Name = "cbCard";
            this.cbCard.Size = new System.Drawing.Size(50, 18);
            this.cbCard.TabIndex = 1;
            this.cbCard.Text = "Карта";
            this.cbCard.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.cbCard_ToggleStateChanged);
            // 
            // SearchClients
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 318);
            this.Controls.Add(this.cbCard);
            this.Controls.Add(this.cbSex);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.radGroupBox4);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.grCard);
            this.Controls.Add(this.grSex);
            this.Controls.Add(this.radGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "SearchClients";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Поиск клиентов";
            this.Load += new System.EventHandler(this.SearchClients_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tbSurname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMiddleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grSex)).EndInit();
            this.grSex.ResumeLayout(false);
            this.grSex.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbWM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grCard)).EndInit();
            this.grCard.ResumeLayout(false);
            this.grCard.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbTariff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbTariff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbNCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox4)).EndInit();
            this.radGroupBox4.ResumeLayout(false);
            this.radGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Telerik.WinControls.UI.RadTextBox tbSurname;
        private Telerik.WinControls.UI.RadTextBox tbName;
        private Telerik.WinControls.UI.RadTextBox tbMiddleName;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadSpinEditor spFrom;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadGroupBox grSex;
        private Telerik.WinControls.UI.RadRadioButton rbWM;
        private Telerik.WinControls.UI.RadRadioButton rbW;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadRadioButton rbM;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadSpinEditor spTo;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadGroupBox grCard;
        private Telerik.WinControls.UI.RadRadioButton rbTariff;
        private Telerik.WinControls.UI.RadRadioButton rbNCard;
        private Telerik.WinControls.UI.RadTextBox tbNCard;
        private Telerik.WinControls.UI.RadButton btnSearch;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox4;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox tbPhone;
        private Telerik.WinControls.UI.RadButton btnClear;
        private Telerik.WinControls.UI.RadCheckBox cbSex;
        private Telerik.WinControls.UI.RadCheckBox cbCard;
        private Telerik.WinControls.UI.RadDropDownList cbTariff;
    }
}
