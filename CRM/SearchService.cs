﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace CRM
{
    public partial class SearchService : Telerik.WinControls.UI.RadForm
    {
        public SearchService()
        {
            InitializeComponent();
        }

        public static List<Service> FilterService = new List<Service>();

        private void SearchService_Load(object sender, EventArgs e)
        {
            spFrom.Minimum = 0;
            spFrom.Maximum = 100000;
            spTo.Minimum = 0;
            spTo.Maximum = 100000;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            spFrom.Value = 0;
            spTo.Value = 0;
            tbName.Text = "";
        }

        private void spFrom_ValueChanged(object sender, EventArgs e)
        {
            spTo.Minimum = spFrom.Value;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string Name = tbName.Text;

            decimal From = spFrom.Value;
            decimal To = spTo.Value;

            int p = -1;

            MainForm.ServiceList = Service.GetAll();
            FilterService.Clear();


            for (int i = 0; i < MainForm.ServiceList.Count; i++) //совпадения по фио и нмеру телефона
            {
                p = -1;
                p = MainForm.ServiceList[i].name.ToLower().IndexOf(Name.ToLower());
                if (p > -1)
                {
                    if (To != 0)
                    {
                        if (MainForm.ServiceList[i].cost >= From )
                        {
                            if (MainForm.ServiceList[i].cost <= To)
                            {
                                FilterService.Add(MainForm.ServiceList[i]);
                            }
                        }
                    }
                    else
                    {
                        FilterService.Add(MainForm.ServiceList[i]);
                    }

                }
            }
            MainForm.ServiceList.Clear();
            if (FilterService.Count != 0 )
            {
                for (int i = 0; i < FilterService.Count; i++)
                {
                    MainForm.ServiceList.Add(FilterService[i]);
                }
            }

            else if (MainForm.ServiceList.Count == 0)
            {
                MessageBox.Show(
                "Совпадений не найдено",
                "Результат поиска",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information,
                MessageBoxDefaultButton.Button1,
                MessageBoxOptions.DefaultDesktopOnly);
                MainForm.ServiceList = Service.GetAll();
            }
            this.Close();
        }
    }
}
