﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace CRM
{
    public partial class SearchCard : Telerik.WinControls.UI.RadForm
    {
        public SearchCard()
        {
            InitializeComponent();
        }
        public static List<Card> FilterCard = new List<Card>();

        private void SearchCard_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < Tariff.GetAll().Count; i++)
            {
                cbTariff.Items.Add(Tariff.GetAll()[i].name);
            }
            cbTariff.SelectedIndex = 0;
            rbNCard.IsChecked = false;
            rbTariff.IsChecked = false;
            tbNCard.Enabled = false;
            cbTariff.Enabled = false;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            tbSurname.Text = "";
            tbName.Text = "";
            tbMiddleName.Text = "";
            tbNCard.Text = "";
            rbNCard.IsChecked = false;
            rbTariff.IsChecked = false;

        }

        private void rbNCard_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (rbNCard.IsChecked)
            {
                tbNCard.Enabled = true;
            }
            else
            {
                tbNCard.Enabled = false;
            }
        }

        private void rbTariff_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (rbTariff.IsChecked)
            {
                cbTariff.Enabled = true;
            }
            else
            {
                cbTariff.Enabled = false;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string Surname = tbSurname.Text;
            string Name = tbName.Text;
            string MiddleName = tbMiddleName.Text;

            string NCard = tbNCard.Text;
            string Tariff1 = cbTariff.Text;

            int p = -1;

            MainForm.CardList = Card.GetAll();
            FilterCard.Clear();

            Client client = new Client();

            for (int i = 0; i < MainForm.CardList.Count; i++)
            {
                client = Client.FindByID(MainForm.CardList[i].id_User);

                p = client.surname.ToLower().IndexOf(Surname.ToLower());
                if (p > -1)
                {
                    p = -1;
                    p = client.name.ToLower().IndexOf(Name.ToLower());
                    if (p > -1)
                    {
                        p = -1;
                        p = client.middleName.ToLower().IndexOf(MiddleName.ToLower());
                        if (p > -1)
                        {
                            FilterCard.Add(MainForm.CardList[i]);
                        }
                    }
                }
            }
            List<Card> FilterCard2 = new List<Card>();
            if (rbNCard.IsChecked)
            {
                for (int i = 0; i < FilterCard.Count; i++)
                {
                    p = -1;
                    p = MainForm.CardList[i].n_Card.ToLower().IndexOf(NCard.ToLower());
                    if (p > -1)
                    {
                        FilterCard2.Add(FilterCard[i]);
                    }
                }
            }
            if (rbTariff.IsChecked)
            {
                for (int i = 0; i < FilterCard.Count; i++)
                {
                    if (Tariff.FindByID(FilterCard[i].id_Tariff).name == Tariff1)
                    {
                        FilterCard2.Add(FilterCard[i]);
                    }
                }
            }
            MainForm.CardList.Clear();
            if (FilterCard.Count != 0 && rbTariff.IsChecked == false && rbNCard.IsChecked == false)
            {
                for (int i = 0; i < FilterCard.Count; i++)
                {
                    MainForm.CardList.Add(FilterCard[i]);
                }
            }
            if (FilterCard2.Count != 0)
            {
                for (int i = 0; i < FilterCard2.Count; i++)
                {
                    MainForm.CardList.Add(FilterCard2[i]);
                }
            }

            else if (MainForm.CardList.Count == 0)
            {
                MessageBox.Show(
                "Совпадений не найдено",
                "Результат поиска",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information,
                MessageBoxDefaultButton.Button1,
                MessageBoxOptions.DefaultDesktopOnly);
                MainForm.CardList = Card.GetAll();
            }
            this.Close();
        }
    }
}
