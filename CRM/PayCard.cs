﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace CRM
{
    public partial class PayCard : Telerik.WinControls.UI.RadForm
    {
        public PayCard()
        {
            InitializeComponent();
        }
        public Client client;
        public bool StateSave = false;
        private void PayCard_Load(object sender, EventArgs e)
        {
            tbClient.Enabled = false;
            tbAmount.Text = "0";
            tbAmount.Enabled = false;
            if (StateSave)
            {
                tbClient.Text = client.surname + " " + client.name + " " + client.middleName;
                btnSelectClient.Visible = false;
            }
            else
            {
                client = new Client();
                
            }
        }

        private void selectClient_Click(object sender, EventArgs e)
        {
            SelectClient selectClient = new SelectClient();
            selectClient.ShowDialog();
            client = SelectClient.ClientSelect;
            if (client != null)
            {
                tbClient.Text = client.surname + " " + client.name + " " + client.middleName;
            }
            tbAmount.Text = "0";
        }

        private void tbClient_TextChanged(object sender, EventArgs e)
        {
            GVService.Rows.Clear();
            List<Payment> PaymentList = Payment.FindByIDClient(client.id);
            string service;
            for (int i = 0; i < PaymentList.Count; i++)
            {
                if (PaymentList[i].Paid == 0)
                {
                    service = Service.FindByID(ScheduleService.FindByID((Reservation.FindByID(PaymentList[i].ID_Reservation).ID_ScheduleService)).ID_Service).name;
                    GVService.Rows.Add(PaymentList[i].ID, service, PaymentList[i].Amount);
                }
            }
        }

        private void GVService_Click(object sender, EventArgs e)
        {
            decimal amount = Convert.ToDecimal(tbAmount.Text);
            int row = GVService.CurrentCell.RowIndex;
            if (GVService.Rows[row].Cells[3].Value != null)
            {
                if ((bool)GVService.Rows[row].Cells[3].Value == true)
                {
                    GVService.Rows[row].Cells[3].Value = false;
                    amount = amount - Convert.ToDecimal(GVService.Rows[row].Cells[2].Value);
                }
                else
                {
                    GVService.Rows[row].Cells[3].Value = true;
                    amount = amount + Convert.ToDecimal(GVService.Rows[row].Cells[2].Value);
                }
            }
            else
            {
                GVService.Rows[row].Cells[3].Value = true;
                amount = amount + Convert.ToDecimal(GVService.Rows[row].Cells[2].Value);
            }
            tbAmount.Text = amount.ToString();

        }

        private void bntSave_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < GVService.RowCount; i++)
            {
                if (GVService.Rows[i].Cells[3].Value != null)
                {
                    if ((bool)GVService.Rows[i].Cells[3].Value)
                    {
                        Payment.AddPay(DateTime.Now, Convert.ToInt32(GVService.Rows[i].Cells[0].Value));

                    }
                }
            }
            this.Close();
        }
    }
}
