﻿namespace CRM
{
    partial class FreezeCard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtFrom = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.tbIDCard = new Telerik.WinControls.UI.RadTextBoxControl();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.dtTo = new Telerik.WinControls.UI.RadDateTimePicker();
            this.btnFreeze = new Telerik.WinControls.UI.RadButton();
            this.spDays = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.tbNCard = new Telerik.WinControls.UI.RadTextBoxControl();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.lDays = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dtFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbIDCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFreeze)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spDays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lDays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // dtFrom
            // 
            this.dtFrom.Enabled = false;
            this.dtFrom.Location = new System.Drawing.Point(106, 62);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Size = new System.Drawing.Size(135, 20);
            this.dtFrom.TabIndex = 0;
            this.dtFrom.TabStop = false;
            this.dtFrom.Text = "31 октября 2018 г.";
            this.dtFrom.Value = new System.DateTime(2018, 10, 31, 17, 55, 50, 922);
            this.dtFrom.ValueChanged += new System.EventHandler(this.dtFrom_ValueChanged);
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(12, 12);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(52, 18);
            this.radLabel1.TabIndex = 1;
            this.radLabel1.Text = "ID Карты";
            // 
            // tbIDCard
            // 
            this.tbIDCard.Location = new System.Drawing.Point(176, 12);
            this.tbIDCard.Name = "tbIDCard";
            this.tbIDCard.Size = new System.Drawing.Size(65, 20);
            this.tbIDCard.TabIndex = 2;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(12, 64);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(87, 18);
            this.radLabel2.TabIndex = 2;
            this.radLabel2.Text = "Начальная дата";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(12, 88);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(82, 18);
            this.radLabel3.TabIndex = 3;
            this.radLabel3.Text = "Конечная дата";
            // 
            // dtTo
            // 
            this.dtTo.Location = new System.Drawing.Point(106, 88);
            this.dtTo.Name = "dtTo";
            this.dtTo.Size = new System.Drawing.Size(135, 20);
            this.dtTo.TabIndex = 1;
            this.dtTo.TabStop = false;
            this.dtTo.Text = "31 октября 2018 г.";
            this.dtTo.Value = new System.DateTime(2018, 10, 31, 17, 55, 50, 922);
            this.dtTo.ValueChanged += new System.EventHandler(this.dtTo_ValueChanged);
            // 
            // btnFreeze
            // 
            this.btnFreeze.Location = new System.Drawing.Point(69, 239);
            this.btnFreeze.Name = "btnFreeze";
            this.btnFreeze.Size = new System.Drawing.Size(132, 24);
            this.btnFreeze.TabIndex = 4;
            this.btnFreeze.Text = "Заморозить";
            this.btnFreeze.Click += new System.EventHandler(this.btnFreeze_Click);
            // 
            // spDays
            // 
            this.spDays.Location = new System.Drawing.Point(176, 114);
            this.spDays.Name = "spDays";
            this.spDays.Size = new System.Drawing.Size(65, 20);
            this.spDays.TabIndex = 5;
            this.spDays.TabStop = false;
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(12, 115);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(135, 18);
            this.radLabel4.TabIndex = 4;
            this.radLabel4.Text = "Остаток дней заморозки";
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(12, 36);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(75, 18);
            this.radLabel5.TabIndex = 2;
            this.radLabel5.Text = "Номер карты";
            // 
            // tbNCard
            // 
            this.tbNCard.Location = new System.Drawing.Point(106, 38);
            this.tbNCard.Name = "tbNCard";
            this.tbNCard.Size = new System.Drawing.Size(135, 20);
            this.tbNCard.TabIndex = 6;
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(15, 181);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(115, 18);
            this.radLabel6.TabIndex = 5;
            this.radLabel6.Text = "Заморозить карту на";
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(176, 181);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(21, 18);
            this.radLabel7.TabIndex = 6;
            this.radLabel7.Text = "дн.";
            // 
            // lDays
            // 
            this.lDays.Location = new System.Drawing.Point(145, 181);
            this.lDays.Name = "lDays";
            this.lDays.Size = new System.Drawing.Size(12, 18);
            this.lDays.TabIndex = 7;
            this.lDays.Text = "0";
            // 
            // FreezeCard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(260, 275);
            this.Controls.Add(this.lDays);
            this.Controls.Add(this.radLabel7);
            this.Controls.Add(this.radLabel6);
            this.Controls.Add(this.tbNCard);
            this.Controls.Add(this.radLabel5);
            this.Controls.Add(this.spDays);
            this.Controls.Add(this.radLabel4);
            this.Controls.Add(this.btnFreeze);
            this.Controls.Add(this.dtTo);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.tbIDCard);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.dtFrom);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "FreezeCard";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Заморозка";
            this.Load += new System.EventHandler(this.FreezeCard_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbIDCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFreeze)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spDays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lDays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadDateTimePicker dtFrom;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBoxControl tbIDCard;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadDateTimePicker dtTo;
        private Telerik.WinControls.UI.RadButton btnFreeze;
        private Telerik.WinControls.UI.RadSpinEditor spDays;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadTextBoxControl tbNCard;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel lDays;
    }
}
