﻿namespace CRM
{
    partial class SelectCardClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.cbCard = new Telerik.WinControls.UI.RadDropDownList();
            this.tbName = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.tbTariff = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.btnSelect = new Telerik.WinControls.UI.RadButton();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.tbService = new Telerik.WinControls.UI.RadTextBox();
            this.lMas = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.dtDate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.lTime = new Telerik.WinControls.UI.RadLabel();
            this.tTime = new Telerik.WinControls.UI.RadTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTariff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbService)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lMas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(12, 12);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(42, 18);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "Клиент";
            // 
            // cbCard
            // 
            this.cbCard.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbCard.Location = new System.Drawing.Point(60, 88);
            this.cbCard.Name = "cbCard";
            this.cbCard.Size = new System.Drawing.Size(192, 20);
            this.cbCard.TabIndex = 1;
            this.cbCard.Text = "radDropDownList1";
            this.cbCard.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbCard_SelectedIndexChanged);
            // 
            // tbName
            // 
            this.tbName.Enabled = false;
            this.tbName.Location = new System.Drawing.Point(60, 10);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(310, 20);
            this.tbName.TabIndex = 2;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(12, 88);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(36, 18);
            this.radLabel2.TabIndex = 1;
            this.radLabel2.Text = "Карта";
            // 
            // tbTariff
            // 
            this.tbTariff.Location = new System.Drawing.Point(60, 114);
            this.tbTariff.Name = "tbTariff";
            this.tbTariff.Size = new System.Drawing.Size(192, 20);
            this.tbTariff.TabIndex = 3;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(12, 115);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(39, 18);
            this.radLabel3.TabIndex = 2;
            this.radLabel3.Text = "Тариф";
            // 
            // btnSelect
            // 
            this.btnSelect.Location = new System.Drawing.Point(276, 146);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(110, 24);
            this.btnSelect.TabIndex = 4;
            this.btnSelect.Text = "Выбрать";
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(12, 36);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(39, 18);
            this.radLabel4.TabIndex = 1;
            this.radLabel4.Text = "Услуга";
            // 
            // tbService
            // 
            this.tbService.Enabled = false;
            this.tbService.Location = new System.Drawing.Point(60, 36);
            this.tbService.Name = "tbService";
            this.tbService.Size = new System.Drawing.Size(310, 20);
            this.tbService.TabIndex = 3;
            // 
            // lMas
            // 
            this.lMas.Location = new System.Drawing.Point(258, 115);
            this.lMas.Name = "lMas";
            this.lMas.Size = new System.Drawing.Size(138, 18);
            this.lMas.TabIndex = 3;
            this.lMas.Text = "Услуга включена в тариф";
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(12, 60);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(39, 18);
            this.radLabel5.TabIndex = 2;
            this.radLabel5.Text = "Время";
            // 
            // dtDate
            // 
            this.dtDate.Enabled = false;
            this.dtDate.Location = new System.Drawing.Point(60, 62);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(120, 20);
            this.dtDate.TabIndex = 5;
            this.dtDate.TabStop = false;
            this.dtDate.Text = "25 декабря 2018 г.";
            this.dtDate.Value = new System.DateTime(2018, 12, 25, 1, 54, 8, 132);
            // 
            // lTime
            // 
            this.lTime.Location = new System.Drawing.Point(258, 62);
            this.lTime.Name = "lTime";
            this.lTime.Size = new System.Drawing.Size(136, 18);
            this.lTime.TabIndex = 4;
            this.lTime.Text = "Время не входит в тариф";
            // 
            // tTime
            // 
            this.tTime.Enabled = false;
            this.tTime.Location = new System.Drawing.Point(186, 62);
            this.tTime.MaxValue = new System.DateTime(9999, 12, 31, 23, 59, 59, 0);
            this.tTime.MinValue = new System.DateTime(((long)(0)));
            this.tTime.Name = "tTime";
            this.tTime.Size = new System.Drawing.Size(66, 20);
            this.tTime.TabIndex = 6;
            this.tTime.TabStop = false;
            this.tTime.Value = new System.DateTime(2018, 12, 25, 1, 56, 20, 154);
            // 
            // SelectCardClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 182);
            this.Controls.Add(this.tTime);
            this.Controls.Add(this.lTime);
            this.Controls.Add(this.dtDate);
            this.Controls.Add(this.radLabel5);
            this.Controls.Add(this.lMas);
            this.Controls.Add(this.tbService);
            this.Controls.Add(this.radLabel4);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.tbTariff);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.cbCard);
            this.Controls.Add(this.radLabel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "SelectCardClient";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Выбор карты клиента";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SelectCardClient_FormClosing);
            this.Load += new System.EventHandler(this.SelectCardClient_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTariff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbService)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lMas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadDropDownList cbCard;
        private Telerik.WinControls.UI.RadTextBox tbName;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox tbTariff;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadButton btnSelect;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox tbService;
        private Telerik.WinControls.UI.RadLabel lMas;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadDateTimePicker dtDate;
        private Telerik.WinControls.UI.RadLabel lTime;
        private Telerik.WinControls.UI.RadTimePicker tTime;
    }
}
