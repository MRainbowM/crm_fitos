﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM
{
    public class Visitor_Log
    {
        public int ID { get; set; }
        public int ID_Reservation { get; set; }
        public DateTime TimeVisiting { get; set; }
        public DateTime TimeLeaving { get; set; }
        public string Payment { get; set; }


        public static void AddTimeVisiting(int ID_Reservation, string Payment)
        {
            DateTime Date = DateTime.Now;
            using (ApplicationContext db = new ApplicationContext())
            {
                db.Database.EnsureCreated();
                Visitor_Log Visitor_Log = new Visitor_Log
                {
                    ID_Reservation = ID_Reservation,
                    TimeVisiting = Date,
                    Payment = Payment
                };
                db.Visitors_Log.Add(Visitor_Log);
                db.SaveChanges();
            }
        }

        public static void AddTimeLeaving(int ID_Reservation)
        {
            DateTime Date = DateTime.Now;
            using (ApplicationContext db = new ApplicationContext())
            {
                db.Database.EnsureCreated();
                Visitor_Log Visitor_Log = db.Visitors_Log.Where(c => c.ID_Reservation == ID_Reservation).FirstOrDefault();
                Visitor_Log.TimeLeaving = Date;
                db.SaveChanges();
            }
        }


        public static Visitor_Log FindByIDReservation(int ID_Reservation)
        {
            ApplicationContext db = new ApplicationContext();
            Visitor_Log Visitor_Log = db.Visitors_Log.Where(x => x.ID_Reservation == ID_Reservation).FirstOrDefault();
            return Visitor_Log;
        }

        public static List<Reservation> FindByIDClientForEndVisit(int ID_Client)
        {
            ApplicationContext db = new ApplicationContext();
            List<Visitor_Log> Visitor_Log = db.Visitors_Log
                   .Where(x => x.TimeVisiting != null && x.TimeLeaving == DateTime.MinValue)
                   .Select(x => new Visitor_Log
                   {
                       ID = x.ID,
                       ID_Reservation = x.ID_Reservation,
                       Payment = x.Payment,
                       TimeVisiting = x.TimeVisiting
                   }).ToList();
            Reservation reservation;
            List<Reservation> ResList = new List<Reservation>();
            for (int i = 0; i < Visitor_Log.Count; i++)
            {
                reservation = Reservation.FindByID(Visitor_Log[i].ID_Reservation);
                if (reservation.ID_User == ID_Client)
                {
                    ResList.Add(reservation);
                }
            }
            return ResList;
        }



        public static List<Client> GetClientForEndVisit()
        {
            List<Client> ClientsList = new List<Client>();
            ApplicationContext db = new ApplicationContext();

            List<Visitor_Log> Visitor_Log = db.Visitors_Log
                    .Where(x => x.TimeVisiting != null && x.TimeLeaving == DateTime.MinValue)
                    .Select(x => new Visitor_Log
                    {
                        ID = x.ID,
                        ID_Reservation = x.ID_Reservation,
                        Payment = x.Payment,
                        TimeVisiting = x.TimeVisiting
                    }).ToList();
            for (int i = 0; i < Visitor_Log.Count; i++)
            {
                ClientsList.Add(Client.FindByID(Reservation.FindByID(Visitor_Log[i].ID_Reservation).ID_User));
            }
            return ClientsList;
        }

        public static List<Visitor_Log> GetVisitorByDate(DateTime Date1, DateTime Date2)
        {
            ApplicationContext db = new ApplicationContext();
            List<Visitor_Log> Visitor_Log = db.Visitors_Log
                .Where(x => x.TimeVisiting != null && x.TimeLeaving != null && x.TimeVisiting >= Date1 && x.TimeVisiting <= Date2)
                .Select(x => new Visitor_Log
                {
                    ID = x.ID,
                    ID_Reservation = x.ID_Reservation,
                    Payment = x.Payment,
                    TimeVisiting = x.TimeVisiting,
                    TimeLeaving = x.TimeLeaving
                }).ToList();
            return Visitor_Log;
        }
    }
}
