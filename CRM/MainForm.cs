﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace CRM
{
    public partial class MainForm : Telerik.WinControls.UI.RadForm
    {
        public MainForm()
        {
            InitializeComponent();
        }

        //Фильтры
        public static List<Room> RoomList = new List<Room>();
        public static List<Tariff> TariffList = new List<Tariff>();
        public static List<Client> ClientList = new List<Client>();
        public static List<Trainer> TrainerList = new List<Trainer>();
        public static List<Service> ServiceList = new List<Service>();
        public static List<Card> CardList = new List<Card>();
        public static List<Payment> PaymentList = new List<Payment>();
        public static List<ScheduleService> ScheduleServiceList = new List<ScheduleService>();

        private void RadForm1_Load(object sender, EventArgs e)
        {
            RoomList = Room.GetAll();
            TariffList = Tariff.GetAll();
            ClientList = Client.GetAll();
            TrainerList = Trainer.GetAll();
            ServiceList = Service.GetAll();
            CardList = Card.GetAll();
            PaymentList = Payment.GetAll();
            ScheduleServiceList = ScheduleService.GetAll();

            int k = PageView.Pages.Count;
            for (int i = 0; i < k; i++)
            {
                PageView.Pages.Remove(PageView.Pages[0]);
            }

            CalendarSchedule.AllowMultipleSelect = true;
            GVSchedule.Columns[6].IsVisible = false;
            GVPayments.Columns[6].IsVisible = false;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            tbNameClient.Text = "";
            tbNameClient2.Text = "";
            tbNameClient.Enabled = false;
            tbNameClient2.Enabled = false;
            lNCard.Visible = false;
            cbNCard.Visible = false;
            btnGetCard.Visible = false;
            btnGetCard2.Visible = false;
            tbAmount.Text = 0.ToString();
            btnSelectSchedule.Visible = false;
            cbNCard2.Enabled = false;
            tbAmountPayments.Enabled = false;
            Autorizatsiya();

            spCountSchedule.Minimum = 1;
            spCountSchedule.Maximum = 11;
            spCountSchedule.Value = 11;
            spFromSchedule.Minimum = 1;
            spFromSchedule.Value = 1;
            spToSchedule.Enabled = false;

            spCountClient.Minimum = 1;
            spCountClient.Maximum = 11;
            spCountClient.Value = 11;
            spFromClient.Minimum = 1;
            spFromClient.Value = 1;
            spToClient.Enabled = false;

            spCountPayment.Minimum = 1;
            spCountPayment.Maximum = 11;
            spCountPayment.Value = 11;
            spFromPayment.Minimum = 1;
            spFromPayment.Value = 1;
            spToPayment.Enabled = false;


            spCountCard.Minimum = 1;
            spCountCard.Maximum = 11;
            spCountCard.Value = 11;
            spFromCard.Minimum = 1;
            spFromCard.Value = 1;
            spToCard.Enabled = false;

            CreateStatictics();
        }
        private void Autorizatsiya()
        {
            Autorizatsiya autorizatsiya = new Autorizatsiya();
            switch (User.AutorizedUser.stateUser)
            {
                case "Client":
                    ID_Client = User.AutorizedUser.id;
                    ClientCard clientCard = new ClientCard();
                    clientCard.StateSave = true;
                    clientCard.ShowDialog();
                    this.Close();
                    autorizatsiya.Show();
                break;
                case "Trainer":
                    ID_Trainer = User.AutorizedUser.id;
                    TrainerCard trainerCard = new TrainerCard();
                    trainerCard.StateSave = true;
                    trainerCard.ShowDialog();
                    this.Close();
                    autorizatsiya.Show();
                    break;
                case "Manager":
                    MenuOptions.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
                    break;
                case "Admin":
                    break;
            }
        }

        private void CreateStatictics()
        {
            List<Service> services = Service.GetAll();
            for (int i = 0; i < services.Count; i++)
            {
                if (services[i].name != "Заморозка")
                {
                    cbService.Items.Add(services[i].name);
                }
            }
            rbDateStatistics.IsChecked = true;
        }




        //для карточек
        public static int ID_Client;
        public static int ID_Trainer;
        public static int ID_Service;
        public static int ID_Tariff;
        public static int ID_Card;
        public static int ID_Room;
        public static int ID_Schedule;
        public static int ID_Payment;


        //ПОИСК
        private void searchRoom_Click(object sender, EventArgs e)
        {
            SearchRoom searchRoom = new SearchRoom();
            searchRoom.ShowDialog();
            RefreshTable();
        }
        private void searchTariff_Click(object sender, EventArgs e)
        {
            SearchTariff searchTariff = new SearchTariff();
            searchTariff.ShowDialog();
            RefreshTable();
        }
        private void searchClient_Click(object sender, EventArgs e)
        {
            SearchClients searchClients = new SearchClients();
            searchClients.ShowDialog();
            RefreshTable();
        }
        private void searchWorker_Click(object sender, EventArgs e)
        {
            SearchTrainer SearchTrainer = new SearchTrainer();
            SearchTrainer.ShowDialog();
            RefreshTable();
        }
        private void searchService_Click(object sender, EventArgs e)
        {
            SearchService searchService = new SearchService();
            searchService.ShowDialog();
            RefreshTable();
        }
        private void searchCard_Click(object sender, EventArgs e)
        {
            SearchCard searchCard = new SearchCard();
            searchCard.ShowDialog();
            RefreshTable();
        }
        private void searchPay_Click(object sender, EventArgs e)
        {
            SearchPay searchPay = new SearchPay();
            searchPay.ShowDialog();
            rbDay.IsChecked = false;
            rbMonth.IsChecked = false;
            rbYear.IsChecked = false;
            RefreshTable();
        }

        private void searchSchedule_Click(object sender, EventArgs e)
        {
            SearchSchedule searchSchedule = new SearchSchedule();
            searchSchedule.ShowDialog();
        }

        protected void ShowPage(Telerik.WinControls.UI.RadPageViewPage namePage)
        {
            int k = 0;
            for (int i = 0; i < PageView.Pages.Count; i++)
            {
                string name = PageView.Pages[i].Name;
                if (namePage.Name == name)
                {
                    k++;
                }
            }
            if (k == 0)
            {
                PageView.Pages.Add(namePage);
            }
            PageView.SelectedPage = namePage;
        }
        private void radMenuItemOperations_Click(object sender, EventArgs e)
        {
            ShowPage(radPageViewPageOperation);
        }
        private void radMenuItemSchedule_Click(object sender, EventArgs e)
        {
            ShowPage(pvSchedule);
        }
        private void radMenuItemCLients_Click(object sender, EventArgs e)
        {
            ShowPage(pvClients);
        }
        private void radMenuItemCards_Click(object sender, EventArgs e)
        {
            ShowPage(pvCards);
        }
        private void radMenuItemWorkers_Click(object sender, EventArgs e)
        {
            ShowPage(pvTrainers);
        }
        private void radMenuItemServices_Click(object sender, EventArgs e)
        {
            ShowPage(pvServices);
        }
        private void radMenuItemTariffs_Click(object sender, EventArgs e)
        {
            ShowPage(pvTariffs);
        }
        private void radMenuItemRooms_Click(object sender, EventArgs e)
        {
            ShowPage(pvRooms);
        }
        private void radMenuItemPay_Click(object sender, EventArgs e)
        {
            ShowPage(pvPayment);
        }
        private void radMenuItemStatistics_Click(object sender, EventArgs e)
        {
            ShowPage(pvStatistics);
        }
        private void Options_Click(object sender, EventArgs e)
        {
            OptionsForm optionsForm = new OptionsForm();
            optionsForm.ShowDialog();
        }

        //добавление записей
        private void addRoom_Click(object sender, EventArgs e)
        {
            RoomCard roomCard = new RoomCard();
            roomCard.StateSave = false;
            roomCard.ShowDialog();
            if (ID_Room != 0)
            {
                var index = RoomList.FindIndex(x => x.id == ID_Room);
                if (index == -1)
                {
                    RoomList.Add(Room.FindByID(ID_Room));
                }
            }
            RefreshTable();
        }

        private void addTariff_Click(object sender, EventArgs e)
        {
            TariffCard tariffCard = new TariffCard();
            tariffCard.StateSave = false;
            tariffCard.ShowDialog();
            if (ID_Tariff != 0)
            {
                var index = TariffList.FindIndex(x => x.id == ID_Tariff);
                if (index == -1)
                {
                    TariffList.Add(Tariff.FindByID(ID_Tariff));
                }
            }
            RefreshTable();
        }

        private void AddClient_Click(object sender, EventArgs e)
        {
            ClientCard clientCard = new ClientCard();
            clientCard.StateSave = false;
            clientCard.ShowDialog();
            if (ID_Client != 0)
            {
                var index = ClientList.FindIndex(x => x.id == ID_Client);
                if (index == -1)
                {
                    ClientList.Add(Client.FindByID(ID_Client));
                }
            }
            RefreshTable();
        }
        private void addTrainer_Click(object sender, EventArgs e)
        {
            TrainerCard trainerCard = new TrainerCard();
            trainerCard.StateSave = false;
            trainerCard.ShowDialog();
            if (ID_Trainer != 0)
            {
                var index = TrainerList.FindIndex(x => x.id == ID_Trainer);
                if (index == -1)
                {
                    TrainerList.Add(Trainer.FindByID(ID_Trainer));
                }
            }
            RefreshTable();
        }
        private void addService_Click(object sender, EventArgs e)
        {
            ServiceCard serviceCard = new ServiceCard();
            serviceCard.StateSave = false;
            serviceCard.ShowDialog();
            if (ID_Service != 0)
            {
                var index = ServiceList.FindIndex(x => x.id == ID_Service);
                if (index == -1)
                {
                    ServiceList.Add(Service.FindByID(ID_Service));
                }
            }
            RefreshTable();
        }
        private void addCard_Click(object sender, EventArgs e)
        {
            CardCard cardCard = new CardCard();
            cardCard.StateSave = false;
            CardCard.ID_Client = 0;
            cardCard.ShowDialog();
            if (ID_Card != 0)
            {
                var index = CardList.FindIndex(x => x.id == ID_Card);
                if (index == -1)
                {
                    CardList.Add(Card.FindByID(ID_Card));
                }
            }
            RefreshTable();
        }
        private void addPay_Click(object sender, EventArgs e)
        {
            PayCard payCard = new PayCard();
            payCard.ShowDialog();
            if (ID_Payment != 0)
            {
                var index = PaymentList.FindIndex(x => x.ID == ID_Payment);
                if (index == -1)
                {
                    PaymentList.Add(Payment.FindByID(ID_Payment));
                }
            }
            RefreshTable();
        }

        private void addSchedule_Click(object sender, EventArgs e)
        {
            ScheduleCard scheduleCard = new ScheduleCard();
            scheduleCard.StateSave = false;
            scheduleCard.ShowDialog();
            this.Show();
            if (ID_Schedule != 0)
            {
                var index = ScheduleServiceList.FindIndex(x => x.ID == ID_Schedule);
                if (index == -1)
                {
                    ScheduleServiceList.Add(ScheduleService.FindByID(ID_Schedule));
                }
            }

            RefreshTable();
        }

        private void PageView_SelectedPageChanged(object sender, EventArgs e) //ИЗМЕНЕНИЕ ВКЛАДОК
        {
            RefreshTable();
        }
        
        private void RefreshTable() // обновить/заполнить таблицу
        {
            if (PageView.SelectedPage == pvRooms)
            {
                GVRooms.Rows.Clear();
                for (int i = 0; i < RoomList.Count; i++)// создание таблицы с ПОМЕЩЕНИЯМИ
                {
                    if (Room.FindByID(RoomList[i].id).dateDelete == null)
                    {
                        GVRooms.Rows.Add(RoomList[i].id, RoomList[i].name, RoomList[i].equipment, RoomList[i].capacity);
                    }
                }
            }
            if (PageView.SelectedPage == pvTariffs)
            {
                GVTariffs.Rows.Clear();
                for (int i = 0; i <= TariffList.Count - 1; i++)// создание таблицы с ТАРИФАМИ
                {
                    if (Tariff.FindByID(TariffList[i].id).dateDelete == null)
                    {
                        GVTariffs.Rows.Add(TariffList[i].id, TariffList[i].name, TariffList[i].totalCost, TariffList[i].startDate, TariffList[i].expirationDate, TariffList[i].duration);
                    }
                }
            }
            if (PageView.SelectedPage == pvClients)
            {
                CreateGVClient();
            }
            if (PageView.SelectedPage == pvTrainers)
            {
                GVTrainers.Rows.Clear();
                for (int i = 0; i < TrainerList.Count; i++)// создание таблицы с ТРЕНЕРАМИ
                {
                    if (Trainer.FindByID(TrainerList[i].id).dateDelete == null)
                    {
                        GVTrainers.Rows.Add(TrainerList[i].id, TrainerList[i].surname, TrainerList[i].name, TrainerList[i].middleName, TrainerList[i].phone);
                    }
                }
            }
            if (PageView.SelectedPage == pvServices)
            {
                GVServices.Rows.Clear();
                for (int i = 1; i <= ServiceList.Count - 1; i++)// создание таблицы с УСЛУГАМИ
                {
                    if (Service.FindByID(ServiceList[i].id).dateDelete == null)
                    {
                        GVServices.Rows.Add(ServiceList[i].id, ServiceList[i].name, ServiceList[i].cost, ServiceList[i].numberOfPeople);
                    }
                }
            }
            if (PageView.SelectedPage == pvCards)
            {
                CreateGVCard();
            }
            if (PageView.SelectedPage == pvSchedule)
            {
                CreateGVSchedule();
            }
            if (PageView.SelectedPage == pvPayment)
            {
                CreateGVPayment();
            }

        }

        //////КАРТЫ СТРАНИЦЫ
        private void CreateGVCard()
        {
            GVCard.Rows.Clear();
            if (spCountCard.Value == 0)
            {
                spCountCard.Value = 1;
            }
            int ElementInPage = Convert.ToInt32(spCountCard.Value);
            int MaxPage = CardList.Count / ElementInPage;
            if (CardList.Count % ElementInPage > 0)
            {
                MaxPage = MaxPage + 1;
            }
            spToCard.Value = MaxPage;
            spFromCard.Maximum = MaxPage;
            if (spFromCard.Value == 0)
            {
                if (spToCard.Value > 0)
                {
                    spFromCard.Value = 1;
                }
            }
            List<List<Card>> Card2 = new List<List<Card>>();
            int x = 0;
            for (int pag = 0; pag < MaxPage; pag++)
            {
                List<Card> Card1 = new List<Card>();
                for (int el = 0; el < ElementInPage; el++)
                {
                    if (x == CardList.Count)
                    {
                        break;
                    }
                    else
                    {
                        Card1.Add(CardList[x]);
                        x++;
                    }
                }
                Card2.Add(Card1);
                if (x == CardList.Count)
                {
                    break;
                }
            }
            int nPage = Convert.ToInt32(spFromCard.Value);
            if (Card2.Count > 0 && nPage > 0)
            {
                for (int i = 0; i < Card2[nPage - 1].Count; i++)
                {

                    if (Card.FindByID(Card2[nPage - 1][i].id).dateDelete == null)
                    {
                        GVCard.Rows.Add(Card2[nPage - 1][i].id, Card2[nPage - 1][i].n_Card, Tariff.FindByID(Card2[nPage - 1][i].id_Tariff).name, Card2[nPage - 1][i].id_User,
                            Client.FindByID(Card2[nPage - 1][i].id_User).surname, Client.FindByID(Card2[nPage - 1][i].id_User).name,
                            Client.FindByID(Card2[nPage - 1][i].id_User).middleName, Card2[nPage - 1][i].dateOfCreation);
                    }
                }
            }
            ChengeFromCard();
        }
        private void ChengeFromCard()
        {
            if (spFromCard.Value == spToCard.Value)
            {
                btnNextCard.Enabled = false;
            }
            else
            {
                btnNextCard.Enabled = true;
            }
            if (spFromCard.Value == 1)
            {
                btnPrevCard.Enabled = false;
            }
            else
            {
                btnPrevCard.Enabled = true;
            }
        }

        private void btnPrevCard_Click(object sender, EventArgs e)
        {
            if (spFromCard.Value > 1)
            {
                spFromCard.Value = spFromCard.Value - 1;
            }
            CreateGVCard();
        }
        private void spFromCard_ValueChanged(object sender, EventArgs e)
        {
            if (spFromCard.Value <= spToCard.Value)
            {
                ChengeFromCard();
                CreateGVCard();
            }
        }
        private void btnNextCard_Click(object sender, EventArgs e)
        {
            spFromCard.Value = spFromCard.Value + 1;
            CreateGVCard();
        }
        private void spCountCard_ValueChanged(object sender, EventArgs e)
        {
            CreateGVCard();
        }

        //////ПЛАТЕЖИ
        private void CreateGVPayment()//построение таблицы с платежами
        {
            GVPayments.Rows.Clear();
            if (spCountPayment.Value == 0)
            {
                spCountPayment.Value = 1;
            }
            int ElementInPage = Convert.ToInt32(spCountPayment.Value);
            int MaxPage = PaymentList.Count / ElementInPage;
            if (PaymentList.Count % ElementInPage > 0)
            {
                MaxPage = MaxPage + 1;
            }
            spToPayment.Value = MaxPage;
            spFromPayment.Maximum = MaxPage;
            if (spFromPayment.Value == 0)
            {
                if (spToPayment.Value > 0)
                {
                    spFromPayment.Value = 1;
                }
            }
            List<List<Payment>> Payment2 = new List<List<Payment>>();
            int x = 0;
            for (int pag = 0; pag < MaxPage; pag++)
            {
                List<Payment> Payment1 = new List<Payment>();
                for (int el = 0; el < ElementInPage; el++)
                {
                    if (x == PaymentList.Count)
                    {
                        break;
                    }
                    else
                    {
                        Payment1.Add(PaymentList[x]);
                        x++;
                    }
                }
                Payment2.Add(Payment1);
                if (x == PaymentList.Count)
                {
                    break;
                }
            }
            int nPage = Convert.ToInt32(spFromPayment.Value);
            tbAmountPayments.Text = "0";
            GVPayments.Rows.Clear();
            string fio;
            Client client;
            string service;
            string state = "";
            if (Payment2.Count > 0 && nPage > 0)
            {
                for (int i = 0; i < Payment2[nPage - 1].Count; i++)
                {
                    if (Payment2[nPage - 1][i].Paid != 0)
                    {
                        client = Client.FindByID(Payment2[nPage - 1][i].ID_User);
                        fio = client.surname + " " + client.name + " " + client.middleName;
                        service = Service.FindByID(ScheduleService.FindByID((Reservation.FindByID(Payment2[nPage - 1][i].ID_Reservation).ID_ScheduleService)).ID_Service).name;
                        if (Payment.FindByID(Payment2[nPage - 1][i].ID).DateDelete != null)
                        {
                            state = "удален";
                        }
                        else
                        {
                            state = "";
                        }
                        GVPayments.Rows.Add(Payment2[nPage - 1][i].ID, Payment2[nPage - 1][i].DateOfPayment, service, fio, Payment2[nPage - 1][i].Paid, state);
                        if (state == "")
                        {
                            tbAmountPayments.Text = (Convert.ToDecimal(tbAmountPayments.Text) + Payment2[nPage - 1][i].Paid).ToString();
                        }
                    }
                }
            }
            ChengeFromPayment();
        }

        private void ChengeFromPayment()
        {
            if (spFromPayment.Value == spToPayment.Value)
            {
                btnNextPayment.Enabled = false;
            }
            else
            {
                btnNextPayment.Enabled = true;
            }
            if (spFromPayment.Value == 1)
            {
                btnPrevPayment.Enabled = false;
            }
            else
            {
                btnPrevPayment.Enabled = true;
            }
        }
        private void btnPrevPayment_Click(object sender, EventArgs e)
        {
            if (spFromPayment.Value > 1)
            {
                spFromPayment.Value = spFromPayment.Value - 1;
            }
            CreateGVPayment();
        }
        private void spFrom_ValueChanged(object sender, EventArgs e)
        {
            if (spFromPayment.Value <= spToPayment.Value)
            {
                ChengeFromPayment();
                CreateGVPayment();
            }
        }
        private void btnNextPayment_Click(object sender, EventArgs e)
        {
            spFromPayment.Value = spFromPayment.Value + 1;
            CreateGVPayment();
        }
        private void spCountPayment_ValueChanged(object sender, EventArgs e)
        {
            CreateGVPayment();
        }

        //////КЛИЕНТЫ СТРАНИЦЫ
        private void CreateGVClient()
        {
            GVClients.Rows.Clear();
            if (spCountClient.Value == 0)
            {
                spCountClient.Value = 1;
            }
            int ElementInPage = Convert.ToInt32(spCountClient.Value);
            int MaxPage = ClientList.Count / ElementInPage;
            if (ClientList.Count % ElementInPage > 0)
            {
                MaxPage = MaxPage + 1;
            }
            spToClient.Value = MaxPage;
            spFromClient.Maximum = MaxPage;
            if (spFromClient.Value == 0)
            {
                if (spToClient.Value > 0)
                {
                    spFromClient.Value = 1;
                }
            }
            List<List<Client>> Client2 = new List<List<Client>>();
            int x = 0;
            for (int pag = 0; pag < MaxPage; pag++)
            {
                List<Client> Client1 = new List<Client>();
                for (int el = 0; el < ElementInPage; el++)
                {
                    if (x == ClientList.Count)
                    {
                        break;
                    }
                    else
                    {
                        Client1.Add(ClientList[x]);
                        x++;
                    }
                }
                Client2.Add(Client1);
                if (x == ClientList.Count)
                {
                    break;
                }
            }
            int nPage = Convert.ToInt32(spFromClient.Value);
            if (Client2.Count > 0 && nPage > 0)
            {
                for (int i = 0; i < Client2[nPage - 1].Count; i++)
                {
                    Client client = Client.FindByID(Client2[nPage - 1][i].id);
                    if (client.dateDelete == null)
                    {
                        GVClients.Rows.Add(Client2[nPage - 1][i].id, Client2[nPage - 1][i].surname, Client2[nPage - 1][i].name, Client2[nPage - 1][i].middleName, Client2[nPage - 1][i].phone, Client2[nPage - 1][i].dateAdd);
                    }
                }
            }
            ChengeFromClient();
        }

        private void ChengeFromClient()
        {
            if (spFromClient.Value == spToClient.Value)
            {
                btnNextClient.Enabled = false;
            }
            else
            {
                btnNextClient.Enabled = true;
            }
            if (spFromClient.Value == 1)
            {
                btnPrevClient.Enabled = false;
            }
            else
            {
                btnPrevClient.Enabled = true;
            }
        }
        private void btnNextClient_Click(object sender, EventArgs e)
        {
            spFromClient.Value = spFromClient.Value + 1;
            CreateGVClient();
        }
        private void btnPrevClient_Click(object sender, EventArgs e)
        {
            if (spFromClient.Value > 1)
            {
                spFromClient.Value = spFromClient.Value - 1;
            }
            CreateGVClient();
        }
        private void spFromClient_ValueChanged(object sender, EventArgs e)
        {
            if (spFromClient.Value <= spToClient.Value)
            {
                ChengeFromClient();
                CreateGVClient();
            }
        }
        private void spCountClient_ValueChanged(object sender, EventArgs e)
        {
            CreateGVClient();
        }

        //////РАСПИСАНИЕ СТРАНИЦЫ
        private void CreateGVSchedule()//заполнение таблицы расписания
        {
            GVSchedule.Rows.Clear();
            List<Reservation> reservations = new List<Reservation>();
            int k = 0;
            if (spCountSchedule.Value == 0)
            {
                spCountSchedule.Value = 1;
            }
            int ElementInPage = Convert.ToInt32(spCountSchedule.Value);
            int MaxPage = ScheduleServiceList.Count / ElementInPage;
            
            if (ScheduleServiceList.Count % ElementInPage > 0)
            {
                MaxPage = MaxPage + 1;
            }
            spToSchedule.Value = MaxPage;
            spFromSchedule.Maximum = MaxPage;
            if (spFromSchedule.Value == 0)
            {
                if (spToSchedule.Value > 0)
                {
                    spFromSchedule.Value = 1;
                }
            }
            List<List<ScheduleService>> Schedule2 = new List<List<ScheduleService>>();
            int x = 0;
            for (int pag = 0; pag < MaxPage; pag++)
            {
                List<ScheduleService> Schedule1 = new List<ScheduleService>();
                for (int el = 0; el < ElementInPage; el++)
                {
                    if (x == ScheduleServiceList.Count)
                    {
                        break;
                    }
                    else
                    {
                        Schedule1.Add(ScheduleServiceList[x]);
                        x++;
                    }
                }
                Schedule2.Add(Schedule1);
                if (x == ScheduleServiceList.Count)
                {
                    break;
                }
            }
            int nPage = Convert.ToInt32(spFromSchedule.Value);
            if (Schedule2.Count > 0 && nPage > 0)
            {
                if (GVSchedule.RowCount > 0)
                {
                    int z = 0;
                    for (int i = 0; i < Schedule2[nPage - 1].Count; i++)
                    {
                        for (int j = 0; j < GVSchedule.RowCount; j++)
                        {
                            if (Convert.ToInt32(GVSchedule.Rows[j].Cells[0].Value.ToString()) == Schedule2[nPage - 1][i].ID)
                            {
                                z++;
                            }
                        }
                        if (z == 0)
                        {
                            int t = TrainerInSchedule.FindByIDSchedule(Schedule2[nPage - 1][i].ID).Count;
                            reservations = Reservation.FindByIDSchudule(Schedule2[nPage - 1][i].ID);
                            k = Schedule2[nPage - 1][i].NumberOfPeople - reservations.Count;
                            GVSchedule.Rows.Add(Schedule2[nPage - 1][i].ID, Schedule2[nPage - 1][i].Date, Service.FindByID(Schedule2[nPage - 1][i].ID_Service).name, Room.FindByID(Schedule2[nPage - 1][i].ID_Room).name, k, t);
                            k = 0;
                        }
                    }
                }
                else
                {
                    if (Schedule2.Count > 0 && nPage > 0)
                    {
                        for (int i = 0; i < Schedule2[nPage - 1].Count; i++)
                        {
                            int t = TrainerInSchedule.FindByIDSchedule(Schedule2[nPage - 1][i].ID).Count;
                            reservations = Reservation.FindByIDSchudule(Schedule2[nPage - 1][i].ID);
                            k = Schedule2[nPage - 1][i].NumberOfPeople - reservations.Count;
                            GVSchedule.Rows.Add(Schedule2[nPage - 1][i].ID, Schedule2[nPage - 1][i].Date, Service.FindByID(Schedule2[nPage - 1][i].ID_Service).name, Room.FindByID(Schedule2[nPage - 1][i].ID_Room).name, k, t);
                            k = 0;
                        }
                    }
                }
            }

            ChengeFromSchedule();
        }

        private void btnNextSchedule_Click(object sender, EventArgs e)
        {
            spFromSchedule.Value = spFromSchedule.Value + 1;
            CreateGVSchedule();
        }
        private void btnPrevSchedule_Click(object sender, EventArgs e)
        {
            if (spFromSchedule.Value > 1)
            {
                spFromSchedule.Value = spFromSchedule.Value - 1;
            }
            CreateGVSchedule();
        }
        private void spFromSchedule_ValueChanged(object sender, EventArgs e)
        {
            if (spFromSchedule.Value <= spToSchedule.Value)
            {
                ChengeFromSchedule();
                CreateGVSchedule();
            }
        }
        private void spCountSchedule_ValueChanged(object sender, EventArgs e)
        {
            CreateGVSchedule();
        }
        private void ChengeFromSchedule()
        {
            if (spFromSchedule.Value == spToSchedule.Value)
            {
                btnNextSchedule.Enabled = false;
            }
            else
            {
                btnNextSchedule.Enabled = true;
            }
            if (spFromSchedule.Value == 1)
            {
                btnPrevSchedule.Enabled = false;
            }
            else
            {
                btnPrevSchedule.Enabled = true;
            }
        }

        // двойной клик по записи (изменение записи)
        private void GVRooms_CellDoubleClick(object sender, Telerik.WinControls.UI.GridViewCellEventArgs e)
        {
            RoomCard roomCard = new RoomCard();
            roomCard.StateSave = true;

            int d = 0;
            for (int i = 0; i < GVRooms.RowCount; i++)
            {
                if (GVRooms.Rows[i].IsSelected)
                {
                    d = Convert.ToInt32(GVRooms.Rows[i].Cells[0].Value);
                }
            }
            ID_Room = d;
            roomCard.ShowDialog();
            for (int i = 0; i < RoomList.Count; i++)
            {
                if (RoomList[i].id == ID_Room)
                {
                    RoomList.RemoveAt(i);
                }
            }
            RoomList.Add(Room.FindByID(ID_Room));

            RefreshTable();
        }

        private void GVClients_CellDoubleClick(object sender, Telerik.WinControls.UI.GridViewCellEventArgs e)
        {
            int d = 0;
            for (int i = 0; i < GVClients.RowCount; i++)
            {
                if (GVClients.Rows[i].IsSelected)
                {
                    d = Convert.ToInt32(GVClients.Rows[i].Cells[0].Value);
                }
            }

            ID_Client = d;

            ClientCard clientCard = new ClientCard();
            clientCard.StateSave = true;
           
            clientCard.ShowDialog();
            for (int i = 0; i < ClientList.Count; i++)
            {
                if (ClientList[i].id == ID_Client)
                {
                    ClientList.RemoveAt(i);
                }
            }
            ClientList.Add(Client.FindByID(ID_Client));
            RefreshTable();
        }

        private void GVTrainers_CellDoubleClick(object sender, Telerik.WinControls.UI.GridViewCellEventArgs e)
        {
            TrainerCard trainerCard = new TrainerCard();
            trainerCard.StateSave = true;
            int d = 0;
            for (int i = 0; i < GVTrainers.RowCount; i++)
            {
                if (GVTrainers.Rows[i].IsSelected)
                {
                    d = Convert.ToInt32(GVTrainers.Rows[i].Cells[0].Value);
                }
            }
            ID_Trainer = d;
            trainerCard.ShowDialog();

            for (int i = 0; i < TrainerList.Count; i++)
            {
                if (TrainerList[i].id == ID_Trainer)
                {
                    TrainerList.RemoveAt(i);
                }
            }
            TrainerList.Add(Trainer.FindByID(ID_Trainer));

            RefreshTable();

        }

        private void GVServices_CellDoubleClick(object sender, Telerik.WinControls.UI.GridViewCellEventArgs e)
        {
            ServiceCard serviceCard = new ServiceCard();
            serviceCard.StateSave = true;
            int d = 0;
            for (int i = 0; i < GVServices.RowCount; i++)
            {
                if (GVServices.Rows[i].IsSelected)
                {
                    d = Convert.ToInt32(GVServices.Rows[i].Cells[0].Value);
                }
            }
            ID_Service = d;
            serviceCard.ShowDialog();

            for (int i = 0; i < ServiceList.Count; i++)
            {
                if (ServiceList[i].id == ID_Service)
                {
                    ServiceList.RemoveAt(i);
                }
            }
            ServiceList.Add(Service.FindByID(ID_Service));

            RefreshTable();
        }

        private void GVTariffs_CellDoubleClick(object sender, Telerik.WinControls.UI.GridViewCellEventArgs e)
        {
            TariffCard tariffCard = new TariffCard();
            tariffCard.StateSave = true;

            int d = 0;
            for (int i = 0; i < GVTariffs.RowCount; i++)
            {
                if (GVTariffs.Rows[i].IsSelected)
                {
                    d = Convert.ToInt32(GVTariffs.Rows[i].Cells[0].Value);
                }
            }
            ID_Tariff = d;
            tariffCard.ShowDialog();

            for (int i = 0; i < TariffList.Count; i++)
            {
                if (TariffList[i].id == ID_Tariff)
                {
                    TariffList.RemoveAt(i);
                }
            }
            if (Tariff.FindByID(ID_Tariff) != null)
            {
                TariffList.Add(Tariff.FindByID(ID_Tariff));
            }

            RefreshTable();
        }

        private void GVCard_CellDoubleClick(object sender, Telerik.WinControls.UI.GridViewCellEventArgs e)
        {
            CardCard cardCard = new CardCard();
            cardCard.StateSave = true;
            int d = 0;
            for (int i = 0; i < GVCard.RowCount; i++)
            {
                if (GVCard.Rows[i].IsSelected)
                {
                    d = Convert.ToInt32(GVCard.Rows[i].Cells[0].Value);
                }
            }
            ID_Card = d;
            cardCard.ShowDialog();

            for (int i = 0; i < CardList.Count; i++)
            {
                if (CardList[i].id == ID_Card)
                {
                    CardList.RemoveAt(i);
                }
            }
            CardList.Add(Card.FindByID(ID_Card));

            RefreshTable();
        }

        private void GVSchedule_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            ScheduleCard scheduleCard = new ScheduleCard();
            scheduleCard.StateSave = true;
            int d = 0;
            for (int i = 0; i < GVSchedule.RowCount; i++)
            {
                if (GVSchedule.Rows[i].IsSelected)
                {
                    d = Convert.ToInt32(GVSchedule.Rows[i].Cells[0].Value);
                }
            }
            ID_Schedule = d;
            scheduleCard.ShowDialog();
            RefreshTable();
        }

        ///РАСПИСАНИЕ
        private void CalendarSchedule_SelectionChanged(object sender, EventArgs e)
        {
            SearchSchedule();
        }

        private void delSchedule_Click(object sender, EventArgs e) //удалить записи расписания
        {
            List<ScheduleService> DelSchedule = new List<ScheduleService>();
            if (GVSchedule.Columns[6].IsVisible)
            {
                for (int i = 0; i < GVSchedule.RowCount; i++)
                {
                    if (GVSchedule.Rows[i].Cells[6].Value != null)
                    {
                        if ((bool)GVSchedule.Rows[i].Cells[6].Value)
                        {
                            DelSchedule.Add(ScheduleService.FindByID(Convert.ToInt32(GVSchedule.Rows[i].Cells[0].Value)));
                        }
                    }
                }
                GVSchedule.Columns[6].IsVisible = false;
            }
            else
            {
                GVSchedule.Columns[6].IsVisible = true;
                for (int i = 0; i < GVSchedule.RowCount; i++)
                {
                    if (GVSchedule.Rows[i].Cells[6].Value != null)
                    {
                        if ((bool)GVSchedule.Rows[i].Cells[6].Value)
                        {
                            GVSchedule.Rows[i].Cells[6].Value = false;
                        }
                    }
                }
            }
            string Massage = "\n";
            for (int i = 0; i < DelSchedule.Count; i++)
            {
                Massage = Massage + DelSchedule[i].Date + " " + Service.FindByID(DelSchedule[i].ID_Service).name + "\n";
            }

            if (DelSchedule.Count > 0)
            {
                DialogResult result = MessageBox.Show(
                "Удалить записи: " + Massage,
                "Удаление " + DelSchedule.Count + " записей",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Exclamation,
                MessageBoxDefaultButton.Button1,
                MessageBoxOptions.DefaultDesktopOnly);
                if (result == DialogResult.Yes)
                {
                    for (int i = 0; i < DelSchedule.Count; i++)
                    {
                        ScheduleService.Del(Convert.ToInt32(DelSchedule[i].ID));
                        for (int j = 0; j < ScheduleServiceList.Count; j++)
                        {
                            if (ScheduleServiceList[j].ID == DelSchedule[i].ID)
                            {
                                ScheduleServiceList.RemoveAt(j);
                            }
                        }
                    }
                }
            }
            RefreshTable();
        }

        private void GVSchedule_Click(object sender, EventArgs e) //установка флажков
        {
            if (GVSchedule.Columns[6].IsVisible)
            {
                int row = GVSchedule.CurrentCell.RowIndex;
                if (GVSchedule.Rows[row].Cells[6].Value != null)
                {
                    if ((bool)GVSchedule.Rows[row].Cells[6].Value == true)
                    {
                        GVSchedule.Rows[row].Cells[6].Value = false;
                    }
                    else
                    {
                        GVSchedule.Rows[row].Cells[6].Value = true;
                    }
                }
                else
                {
                    GVSchedule.Rows[row].Cells[6].Value = true;
                }
            }
        }

        private void tbServiceInSchedule_TextChanged(object sender, EventArgs e)
        {
            SearchSchedule();
        }

        private void SearchSchedule()//вывод записей расписания за выбранные даты
        {
            GVSchedule.Rows.Clear();
            List<ScheduleService> scheduleServicesList2 = new List<ScheduleService>();
            ScheduleServiceList = ScheduleService.GetAll();
            string dateTime1 = "";
            string dateTime2 = "";
            int p = -1;
            List<DateTime> dateTime = CalendarSchedule.SelectedDates.ToList();
            for (int i = 0; i < ScheduleServiceList.Count; i++)
            {
                for (int j = 0; j < dateTime.Count; j++)
                {
                    dateTime1 = (ScheduleServiceList[i].Date).ToShortDateString();
                    dateTime2 = (dateTime[j]).ToShortDateString();
                    if (dateTime1 == dateTime2)
                    {
                        p = Service.FindByID(ScheduleServiceList[i].ID_Service).name.ToLower().IndexOf(tbServiceInSchedule.Text.ToLower());
                        if (p > -1)
                        {
                            scheduleServicesList2.Add(ScheduleServiceList[i]);
                        }
                    }
                }
            }
            ScheduleServiceList.Clear();
            for (int i = 0; i < scheduleServicesList2.Count; i++)
            {
                ScheduleServiceList.Add(scheduleServicesList2[i]);
            }

            RefreshTable(); //обновление таблицы с расписанием
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            tbServiceInSchedule.Text = "";
        }
        
        //////ПЛАТЕЖИ
        private void rbDay_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (rbDay.IsChecked)
            {
                List<Payment> payments = Payment.GetAll();
                PaymentList.Clear();
                string dateTime1 = "";
                string dateTime2 = "";
                GVPayments.Rows.Clear();
                for (int i = 0; i < payments.Count; i++)
                {
                    dateTime1 = (DateTime.Now).ToShortDateString();
                    dateTime2 = (payments[i].DateOfPayment).ToShortDateString();

                    if (dateTime1 == dateTime2)
                    {
                        PaymentList.Add(payments[i]);
                    }
                }
                RefreshTable();
            }
        }

        private void rdMonth_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (rbMonth.IsChecked)
            {
                List<Payment> payments = Payment.GetAll();
                PaymentList.Clear();
                string dateTime1 = "";
                string dateTime2 = "";
                GVPayments.Rows.Clear();
                for (int i = 0; i < payments.Count; i++)
                {
                    dateTime1 = (DateTime.Now.Month).ToString();
                    dateTime2 = (payments[i].DateOfPayment.Month).ToString();

                    if (dateTime1 == dateTime2)
                    {
                        PaymentList.Add(payments[i]);
                    }
                }
                RefreshTable();
            }
        }

        private void rbYear_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (rbYear.IsChecked)
            {
                List<Payment> payments = Payment.GetAll();
                PaymentList.Clear();
                string dateTime1 = "";
                string dateTime2 = "";
                GVPayments.Rows.Clear();
                for (int i = 0; i < payments.Count; i++)
                {
                    dateTime1 = (DateTime.Now.Year).ToString();
                    dateTime2 = (payments[i].DateOfPayment.Year).ToString();
                    if (dateTime1 == dateTime2)
                    {
                        PaymentList.Add(payments[i]);
                    }
                }
                RefreshTable();
            }
        }

        private void btnDeletePay_Click(object sender, EventArgs e)
        {
            List<Payment> DelPayment = new List<Payment>();
            if (GVPayments.Columns[6].IsVisible)
            {
                for (int i = 0; i < GVPayments.RowCount; i++)
                {
                    if (GVPayments.Rows[i].Cells[6].Value != null)
                    {
                        if ((bool)GVPayments.Rows[i].Cells[6].Value)
                        {
                            DelPayment.Add(Payment.FindByID(Convert.ToInt32(GVPayments.Rows[i].Cells[0].Value)));
                        }
                    }
                }
                GVPayments.Columns[6].IsVisible = false;
            }
            else
            {
                GVPayments.Columns[6].IsVisible = true;
                for (int i = 0; i < GVPayments.RowCount; i++)
                {
                    if (GVPayments.Rows[i].Cells[6].Value != null)
                    {
                        if ((bool)GVPayments.Rows[i].Cells[6].Value)
                        {
                            GVPayments.Rows[i].Cells[6].Value = false;
                        }
                    }
                }
            }
            string Massage = "\n";
            for (int i = 0; i < DelPayment.Count; i++)
            {
                Massage = Massage + DelPayment[i].DateOfPayment + " " + DelPayment[i].Paid + " " + Service.FindByID(ScheduleService.FindByID(Reservation.FindByID(DelPayment[i].ID_Reservation).ID_ScheduleService).ID_Service).name + " " +
                    Client.FindByID(Reservation.FindByID(DelPayment[i].ID_Reservation).ID_User).surname + " " + Client.FindByID(Reservation.FindByID(DelPayment[i].ID_Reservation).ID_User).name + " " + 
                    Client.FindByID(Reservation.FindByID(DelPayment[i].ID_Reservation).ID_User).middleName + "\n";
            }

            if (DelPayment.Count > 0)
            {
                DialogResult result = MessageBox.Show(
                "Удалить Платежи: " + Massage,
                "Удаление " + DelPayment.Count + " платежей",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Exclamation,
                MessageBoxDefaultButton.Button1,
                MessageBoxOptions.DefaultDesktopOnly);
                if (result == DialogResult.Yes)
                {
                    for (int i = 0; i < DelPayment.Count; i++)
                    {
                        Payment.DelDebt(Convert.ToInt32(DelPayment[i].ID));
                        for (int j = 0; j < PaymentList.Count; j++)
                        {
                            if (PaymentList[j].ID == DelPayment[i].ID)
                            {
                                PaymentList.RemoveAt(j);
                                PaymentList.Add(DelPayment[i]);
                            }
                        }
                    }

                }
            }
            RefreshTable();
        }

        private void GVPayments_Click(object sender, EventArgs e)
        {
            if (GVPayments.Columns[6].IsVisible)
            {
                int row = GVPayments.CurrentCell.RowIndex;
                if (GVPayments.Rows[row].Cells[6].Value != null)
                {
                    if ((bool)GVPayments.Rows[row].Cells[6].Value == true)
                    {
                        GVPayments.Rows[row].Cells[6].Value = false;
                    }
                    else
                    {
                        if (GVPayments.Rows[row].Cells[5].Value.ToString() != "удален")
                        {
                            GVPayments.Rows[row].Cells[6].Value = true;

                        }
                    }
                }
                else
                {
                    if (GVPayments.Rows[row].Cells[5].Value.ToString() != "удален")
                    {
                        GVPayments.Rows[row].Cells[6].Value = true;
                    }
                }
            }
        }

        //////ОПЕРАЦИИ
        //////Добавить посещение

        List<Card> cardsClient;
        int SelectID_Card = 0;
        public static Client Client;
        private void btnSelectClient_Click(object sender, EventArgs e)
        {
            tbAmount.Text = "0";
            GVReservation.Rows.Clear();
            cbNCard.Items.Clear();
            SelectClient selectClient = new SelectClient();
            selectClient.ShowDialog();
            Client = SelectClient.ClientSelect;
            if (Client != null)
            {
                tbNameClient.Text = Client.surname + " " + Client.name + " " + Client.middleName;
                cardsClient = Card.GetCardByClient(Client.id);

                if (cardsClient.Count != 0)
                {
                    for (int i = 0; i < cardsClient.Count; i++)
                    {
                        cbNCard.Items.Add(cardsClient[i].n_Card);
                    }
                    cbNCard.SelectedIndex = 0;
                }
                else
                {
                    cbNCard.Text = "Карты отсутсвуют";
                    btnGetCard.Visible = false;
                }

                if (Photo.GetPhoto(Client.id) != "")
                {
                    pictureBox1.Image = Image.FromFile(Photo.GetPhoto(Client.id));
                }
                List<ScheduleService> scheduleServices = new List<ScheduleService>();
                CreateGVReserv111(scheduleServices);
                //CreateGVReservation(Reservation.FindReservationScheduleNowByIDClient(Client.id));
            }
        }

        private void btnGetCard_Click(object sender, EventArgs e)
        {
            ID_Card = SelectID_Card;
            CardCard cardCard = new CardCard();
            cardCard.StateSave = true;
            cardCard.ShowDialog();
        }

        private void CreateGVReserv111(List<ScheduleService> scheduleServices)
        {
            GVReservation.Rows.Clear();
            CreateGVReservation(Reservation.FindReservationScheduleNowByIDClient(Client.id));
            if (scheduleServices.Count > 0)
            {
                CreateGVReservation(scheduleServices);
            }
        }
        private void cbNCard_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            for (int i = 0; i < cardsClient.Count; i++)
            {
                if (cbNCard.Text == cardsClient[i].n_Card)
                {
                    SelectID_Card = cardsClient[i].id;
                }
            }
            List<ScheduleService> scheduleServices = new List<ScheduleService>();
            CreateGVReserv111(scheduleServices);
        }

        private void btnSelectSchedule_Click(object sender, EventArgs e)
        {
            SelectSchedule selectSchedule = new SelectSchedule();
            selectSchedule.ShowDialog();
            CreateGVReservation(SelectSchedule.SelectSchedules);
        }

        private void CreateGVReservation(List<ScheduleService> scheduleServices) //заполнение таблицы с зарезервированными занятиями (при добавлении посещения)
        {
            ScheduleService service = new ScheduleService();
            BalanceCard BalanceCard = new BalanceCard();
            Payment payment = new Payment();
            GVReservation.Rows.Clear();
            if (scheduleServices.Count > 0)
            {
                for (int i = 0; i < scheduleServices.Count; i++)
                {
                    service = scheduleServices[i];
                    BalanceCard = BalanceCard.GetBalanceService(SelectID_Card, ScheduleService.FindByID(scheduleServices[i].ID).ID_Service);
                    bool TimeState = Tariff.CheckedTimeUnTariff(service.Date, Card.FindByID(SelectID_Card).id_Tariff);
                    if (Reservation.FindByIDSchuduleAndIDClient(scheduleServices[i].ID, Client.id) == null)
                    {
                        Reservation reservation = new Reservation();
                        int reserv = reservation.AddByUser(scheduleServices[i].ID, Client.id);
                        if ((BalanceCard == null) || ( BalanceCard != null && BalanceCard.Balance == 0) ||(BalanceCard != null && BalanceCard.Balance == 0 && !TimeState) )
                        {
                            Payment.AddDebt(Client.id, DateTime.Now, scheduleServices[i].Cost, reserv);
                        }
                        else if (BalanceCard != null && BalanceCard.Balance > 0 && TimeState)
                        {
                            if (BalanceCard.LastDay != null)
                            {
                                DateTime n = Convert.ToDateTime(BalanceCard.LastDay).AddDays(ServiceInTariff.FindByIDServiceAndIDTariff(Card.FindByID(SelectID_Card).id_Tariff, service.ID_Service).Periodicity);
                                if (n > DateTime.Now)
                                {
                                    Payment.AddDebt(Client.id, DateTime.Now, scheduleServices[i].Cost, reserv);
                                }
                                else
                                {
                                    Reservation.Del(reserv);
                                    reservation.AddByCard(scheduleServices[i].ID, SelectID_Card);
                                }
                            }
                            else
                            {
                                Reservation.Del(reserv);
                                reservation.AddByCard(scheduleServices[i].ID, SelectID_Card);
                            }
                        }
                    }
                    if (Visitor_Log.FindByIDReservation(Reservation.FindByIDSchuduleAndIDClient(scheduleServices[i].ID, Client.id).ID) == null)
                    {
                        payment = Payment.FindByIDReservationAndIDClient(Reservation.FindByIDSchuduleAndIDClient(scheduleServices[i].ID, Client.id).ID, Client.id);
                        if (payment != null && payment.Paid > 0)
                        {
                            GVReservation.Rows.Add(service.ID, service.Date, Service.FindByID(service.ID_Service).name, Room.FindByID(service.ID_Room).name, service.Cost, "оплачено");
                        }
                        if (payment != null && payment.Paid == 0)
                        {
                            GVReservation.Rows.Add(service.ID, service.Date, Service.FindByID(service.ID_Service).name, Room.FindByID(service.ID_Room).name, service.Cost, "не оплачено");

                        }
                           
                        if (BalanceCard != null && BalanceCard.Balance != 0 && TimeState)
                        {
                            if (BalanceCard.LastDay != null)
                            {
                                DateTime n = Convert.ToDateTime(BalanceCard.LastDay).AddDays(ServiceInTariff.FindByIDServiceAndIDTariff(Card.FindByID(SelectID_Card).id_Tariff, service.ID_Service).Periodicity);
                                if (n > DateTime.Now && payment == null)
                                {
                                    GVReservation.Rows.Add(service.ID, service.Date, Service.FindByID(service.ID_Service).name, Room.FindByID(service.ID_Room).name, service.Cost, "не оплачено");
                                }
                                else if(payment == null)
                                {
                                    GVReservation.Rows.Add(service.ID, service.Date, Service.FindByID(service.ID_Service).name, Room.FindByID(service.ID_Room).name, 0, "по тарифу");
                                }
                            }
                            else
                            {
                                GVReservation.Rows.Add(service.ID, service.Date, Service.FindByID(service.ID_Service).name, Room.FindByID(service.ID_Room).name, 0, "по тарифу");
                            }
                        }
                        else if (BalanceCard != null && BalanceCard.Balance != 0 && !TimeState && payment == null )
                        {
                            GVReservation.Rows.Add(service.ID, service.Date, Service.FindByID(service.ID_Service).name, Room.FindByID(service.ID_Room).name, service.Cost, "не оплачено");
                        }
                        if (BalanceCard != null && BalanceCard.Balance == 0)
                        {
                            GVReservation.Rows.Add(service.ID, service.Date, Service.FindByID(service.ID_Service).name, Room.FindByID(service.ID_Room).name, service.Cost, "не оплачено");
                        }
                        if (BalanceCard == null && payment == null)
                        {
                            GVReservation.Rows.Add(service.ID, service.Date, Service.FindByID(service.ID_Service).name, Room.FindByID(service.ID_Room).name, service.Cost, "не оплачено");
                        }
                    }
                }
            }
        }

        private void tbNameClient_TextChanged(object sender, EventArgs e)
        {
            if (tbNameClient.Text == "")
            {
                lNCard.Visible = false;
                cbNCard.Visible = false;
                btnGetCard.Visible = false;
            }
            else
            {
                lNCard.Visible = true;
                cbNCard.Visible = true;
                btnGetCard.Visible = true;
                btnSelectSchedule.Visible = true;
            }
        }

        private void GVReservation_Click(object sender, EventArgs e)
        {
            if (GVReservation.RowCount != 0)
            {
                int row = GVReservation.CurrentCell.RowIndex;
                if (GVReservation.Rows[row].Cells[6].Value != null)
                {
                    if ((bool)GVReservation.Rows[row].Cells[6].Value == true)
                    {
                        GVReservation.Rows[row].Cells[6].Value = false;
                        tbAmount.Text = Convert.ToString(Convert.ToDecimal(tbAmount.Text) - Convert.ToDecimal(GVReservation.Rows[row].Cells[4].Value));
                    }
                    else
                    {
                        GVReservation.Rows[row].Cells[6].Value = true;
                        tbAmount.Text = Convert.ToString(Convert.ToDecimal(tbAmount.Text) + Convert.ToDecimal(GVReservation.Rows[row].Cells[4].Value));
                    }
                }
                else
                {
                    GVReservation.Rows[row].Cells[6].Value = true;
                    tbAmount.Text = Convert.ToString(Convert.ToDecimal(tbAmount.Text) + Convert.ToDecimal(GVReservation.Rows[row].Cells[4].Value));
                }
            }
            
        }

        private void btnAddVisit_Click(object sender, EventArgs e)
        {
            Reservation reservation = new Reservation();
            List<Payment> paymentsList = new List<Payment>();
            Payment payment = new Payment();
            string mess = "";
            for (int i = 0; i < GVReservation.RowCount; i++)
            {
                if (GVReservation.Rows[i].Cells[6].Value != null)
                {
                    if ((bool)GVReservation.Rows[i].Cells[6].Value)
                    {
                        int ID_Reservation = 0;
                        switch (GVReservation.Rows[i].Cells[5].Value.ToString())
                        {
                            case "не оплачено":
                                ID_Reservation = Reservation.FindByIDSchuduleAndIDClient(Convert.ToInt32(GVReservation.Rows[i].Cells[0].Value), Client.id).ID;
                                if (Payment.FindByIDReservationAndIDClient(ID_Reservation, Client.id) == null)
                                {
                                    Payment.AddDebt(Client.id, DateTime.Now, Convert.ToDecimal(GVReservation.Rows[i].Cells[4].Value), ID_Reservation);
                                }
                                paymentsList.Add(Payment.FindByIDReservationAndIDClient(ID_Reservation, Client.id));
                                break;
                            case "по тарифу":
                                if (Reservation.FindByIDSchuduleAndIDClient(Convert.ToInt32(GVReservation.Rows[i].Cells[0].Value), Client.id) == null)
                                {
                                    ID_Reservation = reservation.AddByUser(Convert.ToInt32(GVReservation.Rows[i].Cells[0].Value), Client.id);
                                    Visitor_Log.AddTimeVisiting(ID_Reservation, "по тарифу");
                                }
                                BalanceCard.AddUseService(SelectID_Card, ScheduleService.FindByID(Convert.ToInt32(GVReservation.Rows[i].Cells[0].Value)).ID_Service, 1);
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        int ID_Reservation = Reservation.FindByIDSchuduleAndIDClient(Convert.ToInt32(GVReservation.Rows[i].Cells[0].Value), Client.id).ID;
                        Reservation.Del(ID_Reservation);
                    }
                }
            }
            if (paymentsList.Count != 0)
            {
                PayCard payCard = new PayCard();
                payCard.client = Client;
                payCard.StateSave = true;
                payCard.ShowDialog();
                for (int i = 0; i < paymentsList.Count; i++)
                {
                    Payment pay = Payment.FindByID(paymentsList[i].ID);
                    if (pay.Paid > 0)
                    {
                        Visitor_Log.AddTimeVisiting(pay.ID_Reservation, "оплачено");
                    }
                    else
                    {
                        Reservation.Del(pay.ID_Reservation);
                    }
                }
            }
            for (int p = 0; p < paymentsList.Count; p++)
            {
                if (Payment.FindByID(paymentsList[p].ID).Paid == 0)
                {
                    mess = mess + '\n' + "Не оплачено: " + Service.FindByID(ScheduleService.FindByID(Reservation.FindByID(paymentsList[p].ID_Reservation).ID_ScheduleService).ID_Service).name + ", посещение добавлено";
                }
                if (Payment.FindByID(paymentsList[p].ID).Paid > 0)
                {
                    mess = mess + '\n' + "Оплачено: " + Service.FindByID(ScheduleService.FindByID(Reservation.FindByID(paymentsList[p].ID_Reservation).ID_ScheduleService).ID_Service).name + ", посещение добавлено";
                }

            }
            MessageBox.Show(
            mess,
            "Результат",
            MessageBoxButtons.OK,
            MessageBoxIcon.Information,
            MessageBoxDefaultButton.Button1,
            MessageBoxOptions.DefaultDesktopOnly);
            tbNameClient.Text = "";
            cbNCard.Items.Clear();
            GVReservation.Rows.Clear();
            pictureBox1.Image = null;
        }
        
        //////Закрыть посещение
        private void btnSelectClient2_Click(object sender, EventArgs e)
        {
            SelectClient selectClient = new SelectClient();
            selectClient.StateEndVisit = true;
            selectClient.ShowDialog();
            GVReservation2.Rows.Clear();
            cbNCard2.Items.Clear();
            Client = SelectClient.ClientSelect;
            if (Client != null)
            {
                tbNameClient2.Text = Client.surname + " " + Client.name + " " + Client.middleName;
                List<Reservation> ResList = Visitor_Log.FindByIDClientForEndVisit(Client.id);
                for (int i = 0; i < ResList.Count; i++)
                {
                    if (ResList[i].ID_Card != 0)
                    {
                        cbNCard2.Items.Add(Card.FindByID(ResList[i].ID_Card).n_Card);
                        cbNCard2.SelectedIndex = 0;
                    }
                }
                if (cbNCard2.Items.Count == 0)
                {
                    cbNCard2.Text = "Карты отсутсвуют";
                    btnGetCard2.Visible = false;
                }
                if (Photo.GetPhoto(Client.id) != "")
                {
                    pictureBox2.Image = Image.FromFile(Photo.GetPhoto(Client.id));
                }
                GVReservation2.Rows.Clear();
                ScheduleService scheduleService;
                Visitor_Log visitor_Log;
                for (int i = 0; i < ResList.Count; i++)
                {
                    visitor_Log = Visitor_Log.FindByIDReservation(ResList[i].ID);
                    scheduleService = ScheduleService.FindByID(ResList[i].ID_ScheduleService);
                    decimal cost = 0;
                    if (visitor_Log.Payment == "оплачено")
                    {
                        cost = scheduleService.Cost;
                    }
                    GVReservation2.Rows.Add(scheduleService.ID, scheduleService.Date, Service.FindByID(scheduleService.ID_Service).name, Room.FindByID(scheduleService.ID_Room).name, cost, visitor_Log.Payment);
                }
            }
        }

        private void btnEndVisit_Click(object sender, EventArgs e)
        {
            if (GVReservation2.RowCount > 0)
            {
                for (int i = 0; i < GVReservation2.RowCount; i++)
                {
                    Visitor_Log.AddTimeLeaving(Reservation.FindByIDSchuduleAndIDClient(Convert.ToInt32(GVReservation2.Rows[i].Cells[0].Value), Client.id).ID);
                }
                MessageBox.Show(
                "Посещение закрыто успешно!",
                "Успешно",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information,
                MessageBoxDefaultButton.Button1,
                MessageBoxOptions.DefaultDesktopOnly);
                tbNameClient2.Text = "";
                cbNCard2.Items.Clear();
                GVReservation2.Rows.Clear();
                pictureBox2.Image = null;
            }
        }

        private void RadForm1_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result = MessageBox.Show(
               "Вы уверены, что хотите выйти?",
               "Закрытие программы",
               MessageBoxButtons.YesNo,
               MessageBoxIcon.Exclamation,
               MessageBoxDefaultButton.Button1,
               MessageBoxOptions.DefaultDesktopOnly);
            if (result == DialogResult.Yes)
            {
                Environment.Exit(0);
            }
        }

        //////Статистика
        private void CreateStatisticsByDate()//построение графика: статистика посещений за период времени
        {
            ChartStatistics.Series.Clear();
            DateTime Date1str = fromStatistics.Value;
            DateTime Date2str = toStatistics.Value;

            List<Visitor_Log> Visit = Visitor_Log.GetVisitorByDate(Date1str, Date2str); //все посещения за выбранный период

            List<int> Year = new List<int>();
            for (int i = 0; i < Visit.Count; i++)
            {
                if (Year.Count > 0)
                {
                    int k = 0;
                    for (int j = 0; j < Year.Count; j++)
                    {
                        if (Year[j] == Visit[i].TimeVisiting.Year)
                        {
                            k++;
                        }
                    }
                    if (k == 0)
                    {
                        Year.Add(Visit[i].TimeVisiting.Year);
                    }
                }
                else
                {
                    Year.Add(Visit[i].TimeVisiting.Year);
                }
            }
            string year;
            string name = "";
            string[] month = new string[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" };
            for (int y = 0; y < Year.Count; y++)//цикл для годов
            {
                year = Year[y].ToString();
                ChartStatistics.Series.Add(year);
                ChartStatistics.Series[y].BorderWidth = 5;
                for (int m = 0; m <= 11; m++)//цикл для месяцев
                {
                    int kol = 0;
                    for (int i = 0; i < Visit.Count; i++)
                    {
                        if (Visit[i].TimeVisiting.Month.ToString() == month[m] && Visit[i].TimeVisiting.Year.ToString() == year)
                        {
                            kol++;
                        }
                    }

                    switch (m)
                    {
                        case 0:
                            name = "Янв";
                            break;
                        case 1:
                            name = "Фев";
                            break;
                        case 2:
                            name = "Мар";
                            break;
                        case 3:
                            name = "Апр";
                            break;
                        case 4:
                            name = "Май";
                            break;
                        case 5:
                            name = "Июн";
                            break;
                        case 6:
                            name = "Июл";
                            break;
                        case 7:
                            name = "Авг";
                            break;
                        case 8:
                            name = "Сен";
                            break;
                        case 9:
                            name = "Окт";
                            break;
                        case 10:
                            name = "Ноя";
                            break;
                        case 11:
                            name = "Дек";
                            break;
                    }

                    ChartStatistics.Series[y].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
                    ChartStatistics.Series[y].Points.AddXY(name, kol);
                }
            }
        }

        private void fromStatistics_ValueChanged(object sender, EventArgs e)
        {
            if (rbDateStatistics.IsChecked)
            {
                CreateStatisticsByDate();
            }
            else
            {
                CreateStatisticsByService();
            }
        }

        private void toStatistics_ValueChanged(object sender, EventArgs e)
        {
            if (rbDateStatistics.IsChecked)
            {
                CreateStatisticsByDate();
            }
            else
            {
                CreateStatisticsByService();
            }
        }

        private void rbDateStatistics_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (rbDateStatistics.IsChecked)
            {
                gbDateStatistics.Enabled = true;
                gbServiceStatistics.Enabled = false;
            }
            else
            {
                gbDateStatistics.Enabled = false;
                gbServiceStatistics.Enabled = true;
            }
        }

        private void rbServiceStatistics_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (rbServiceStatistics.IsChecked)
            {
                gbDateStatistics.Enabled = true;
                gbServiceStatistics.Enabled = true;
            }
            else
            {
                gbDateStatistics.Enabled = true;
                gbServiceStatistics.Enabled = false;
            }
        }

        private void CreateStatisticsByService()
        {
            ChartStatistics.Series.Clear();
            DateTime Date1str = fromStatistics.Value;
            DateTime Date2str = toStatistics.Value;
            List<Visitor_Log> Visit = Visitor_Log.GetVisitorByDate(Date1str, Date2str);
            List<Service> Services = new List<Service>();

            for (int i = 0; i < cbService.Items.Count; i++)
            {
                if (cbService.GetItemCheckState(i) == CheckState.Checked)
                {
                    Services.Add(Service.FindByName(cbService.Items[i].ToString()));
                }
            }
            string series = "Количество услуг с " + Date1str.ToLongDateString() + " по " + Date2str.ToLongDateString();
            ChartStatistics.Series.Add(series);
            ChartStatistics.Series[series].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
            for (int j = 0; j < Services.Count; j++)
            {
                int k = 0;
                for (int i = 0; i < Visit.Count; i++)
                {
                    Service s = Service.FindByID(ScheduleService.FindByID(Reservation.FindByID(Visit[i].ID_Reservation).ID_ScheduleService).ID_Service);
                    if (s.name == Services[j].name)
                    {
                        k++;
                    }
                }
                ChartStatistics.Series[series].Points.AddXY(Services[j].name, k);
            }
        }

        private void cbService_SelectedIndexChanged(object sender, EventArgs e)
        {
            CreateStatisticsByService();
        }
    }
}
