-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: fitos
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `balancecards`
--

DROP TABLE IF EXISTS `balancecards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `balancecards` (
  `ID_Service` int(50) NOT NULL,
  `ID_Card` int(50) NOT NULL,
  `Balance` int(50) DEFAULT NULL,
  `LastDay` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `balancecards`
--

LOCK TABLES `balancecards` WRITE;
/*!40000 ALTER TABLE `balancecards` DISABLE KEYS */;
INSERT INTO `balancecards` VALUES (1,1,2,NULL),(1,2,1,'24.12.2018 0:00:00'),(1,3,0,'24.12.2018 0:00:00'),(3,3,5,'23.12.2018 0:00:00'),(1,4,1,'25.12.2018 0:00:00'),(3,4,6,NULL),(1,5,30,NULL),(5,5,100,NULL),(2,5,10,NULL),(3,5,99,'26.12.2018 0:00:00');
/*!40000 ALTER TABLE `balancecards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cards`
--

DROP TABLE IF EXISTS `cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cards` (
  `ID` int(50) NOT NULL AUTO_INCREMENT,
  `N_Card` varchar(20) NOT NULL,
  `ID_User` int(50) NOT NULL,
  `ID_Tariff` int(11) DEFAULT NULL,
  `DateOfCreation` varchar(45) DEFAULT NULL,
  `DataPurchaseTariff` varchar(50) DEFAULT NULL,
  `DateDelete` date DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cards`
--

LOCK TABLES `cards` WRITE;
/*!40000 ALTER TABLE `cards` DISABLE KEYS */;
INSERT INTO `cards` VALUES (1,'1222222222222222',1,2,'17.12.2018 0:00:00','17.12.2018 0:00:00',NULL),(2,'1122222222222222',2,2,'17.12.2018 0:00:00','17.12.2018 0:00:00',NULL),(3,'5454343544444444',2,2,'20.12.2018 0:00:00','20.12.2018 0:00:00',NULL),(4,'1233213123123211',8,2,'25.12.2018 0:00:00','25.12.2018 5:49:06',NULL),(5,'12345678765432',11,4,'25.12.2018 0:00:00','25.12.2018 7:39:19',NULL);
/*!40000 ALTER TABLE `cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `options`
--

DROP TABLE IF EXISTS `options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `options` (
  `ID` int(11) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `Value` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `options`
--

LOCK TABLES `options` WRITE;
/*!40000 ALTER TABLE `options` DISABLE KEYS */;
INSERT INTO `options` VALUES (1,'WayToPhoto','C:\\\\#bgu\\\\crm_fitos\\\\CRM\\\\bin\\\\Debug\\\\Photo'),(2,'PasswordAdmin','admin'),(3,'22','C:\\#bgu\\crm_fitos\\Photo');
/*!40000 ALTER TABLE `options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `payments` (
  `ID` int(50) NOT NULL AUTO_INCREMENT,
  `ID_Card` int(50) DEFAULT NULL,
  `DateOfCreation` datetime NOT NULL,
  `DateOfPayment` datetime DEFAULT NULL,
  `Amount` double NOT NULL,
  `Paid` float DEFAULT NULL,
  `ID_Reservation` int(50) NOT NULL,
  `ID_User` int(50) NOT NULL,
  `DateDelete` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` VALUES (1,NULL,'2018-12-20 16:40:12','2018-12-20 19:34:27',400,400,8,8,'21.12.2018 0:00:00'),(2,NULL,'2018-12-20 17:10:18','2018-12-20 19:32:00',44.44,44.44,9,1,NULL),(3,NULL,'2018-12-20 22:42:15','2018-12-20 22:47:43',400,400,10,2,'21.12.2018 0:00:00'),(4,NULL,'2018-12-21 00:44:35','2018-12-23 20:21:45',400,400,11,8,NULL),(5,NULL,'2018-12-21 01:10:44','2018-12-23 20:21:45',400,400,14,8,NULL),(6,NULL,'2018-12-21 01:12:19','2018-12-23 20:18:10',400,400,16,1,NULL),(7,NULL,'2018-12-23 14:29:07','2018-12-23 20:19:44',100,100,22,2,NULL),(8,NULL,'2018-12-23 15:23:51','2018-12-23 20:19:45',47,47,23,2,'24.12.2018 0:00:00'),(12,NULL,'2018-12-23 17:32:32','2018-12-23 17:33:58',47,47,27,8,'24.12.2018 0:00:00'),(13,NULL,'2018-12-23 17:50:58','2018-12-23 20:19:45',47,47,28,2,'24.12.2018 0:00:00'),(14,NULL,'2018-12-23 17:57:22','2018-12-23 18:04:19',400,400,29,1,NULL),(15,NULL,'2018-12-26 03:45:44','2018-12-26 03:53:00',400,400,35,11,NULL),(16,NULL,'2018-12-26 03:54:57','2018-12-26 03:55:36',100,100,36,11,NULL),(17,NULL,'2018-12-26 06:23:08','2018-12-26 06:23:55',400,400,37,11,NULL),(18,NULL,'2018-12-26 06:26:26','2018-12-26 06:27:46',100,100,38,11,NULL),(19,NULL,'2018-12-26 06:45:28','2018-12-26 06:46:35',400,400,39,11,NULL),(23,NULL,'2018-12-26 07:21:57','2018-12-26 07:22:26',100,100,45,8,NULL),(24,NULL,'2018-12-26 07:26:29','2018-12-26 07:27:12',400,400,46,2,NULL),(25,NULL,'2019-03-20 12:52:15','0001-01-01 00:00:00',500,0,47,11,'20.03.2019 0:00:00'),(26,NULL,'2019-03-20 12:52:42','0001-01-01 00:00:00',500,0,48,11,'20.03.2019 0:00:00'),(27,NULL,'2019-03-20 12:53:04','0001-01-01 00:00:00',500,0,49,11,'20.03.2019 0:00:00'),(28,NULL,'2019-03-20 12:58:24','0001-01-01 00:00:00',500,0,50,11,'20.03.2019 0:00:00'),(29,NULL,'2019-03-20 13:01:45','0001-01-01 00:00:00',500,0,51,11,'20.03.2019 0:00:00'),(30,NULL,'2019-03-24 14:24:00','2019-03-24 14:29:31',400,400,54,11,NULL);
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photos`
--

DROP TABLE IF EXISTS `photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `photos` (
  `ID` int(50) NOT NULL AUTO_INCREMENT,
  `ID_User` int(50) NOT NULL,
  `ID_Room` int(50) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `DateDelete` date DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photos`
--

LOCK TABLES `photos` WRITE;
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;
INSERT INTO `photos` VALUES (1,1,0,'1.jpg',NULL),(5,8,0,'11.jpg',NULL),(6,2,0,'7.jpg',NULL),(8,4,0,'3.jpg',NULL),(9,6,0,'4.jpg',NULL),(10,11,0,'333.jpg',NULL);
/*!40000 ALTER TABLE `photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservations`
--

DROP TABLE IF EXISTS `reservations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `reservations` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_ScheduleService` int(11) NOT NULL,
  `ID_Card` int(11) DEFAULT NULL,
  `ID_User` int(11) DEFAULT NULL,
  `DateDelete` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservations`
--

LOCK TABLES `reservations` WRITE;
/*!40000 ALTER TABLE `reservations` DISABLE KEYS */;
INSERT INTO `reservations` VALUES (2,1,2,2,NULL),(3,2,1,1,NULL),(4,1,1,1,NULL),(5,1,0,8,NULL),(6,3,2,2,'21.12.2018 0:00:00'),(7,3,1,1,'23.12.2018 0:00:00'),(8,3,0,8,'21.12.2018 0:00:00'),(9,4,1,1,'21.12.2018 0:00:00'),(10,4,3,2,'21.12.2018 0:00:00'),(11,3,0,8,'23.12.2018 0:00:00'),(12,3,2,2,'21.12.2018 0:00:00'),(13,3,2,2,'21.12.2018 0:00:00'),(14,4,0,8,NULL),(15,4,3,2,NULL),(16,5,1,1,NULL),(17,6,3,2,NULL),(18,7,2,2,NULL),(19,8,2,2,NULL),(20,9,3,2,NULL),(21,10,1,1,NULL),(22,12,2,2,NULL),(23,12,1,1,NULL),(27,12,0,8,NULL),(28,15,0,2,NULL),(29,15,0,1,NULL),(30,16,3,2,NULL),(31,21,2,2,NULL),(32,22,3,2,NULL),(33,153,4,8,NULL),(35,23,0,11,NULL),(36,76,0,11,NULL),(37,28,0,11,NULL),(38,24,0,11,NULL),(39,32,0,11,NULL),(45,24,0,8,NULL),(46,75,0,2,NULL),(47,155,5,11,'20.03.2019 0:00:00'),(48,155,5,11,'20.03.2019 0:00:00'),(49,155,0,11,'20.03.2019 0:00:00'),(50,155,0,11,'20.03.2019 0:00:00'),(51,155,0,11,'20.03.2019 0:00:00'),(52,155,5,11,'20.03.2019 0:00:00'),(53,155,5,11,NULL),(54,156,5,11,NULL);
/*!40000 ALTER TABLE `reservations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `rooms` (
  `ID` int(50) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL,
  `Equipment` longtext,
  `Capacity` int(50) NOT NULL,
  `Comment` longtext,
  `DateDelete` date DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms`
--

LOCK TABLES `rooms` WRITE;
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` VALUES (1,'Бассейн','шнегнполорплплпр',200,'фуафуццццццц',NULL),(2,'Комната1','rfrfrfrfrfrfrfrfrfrfrfrfrf',50,'',NULL),(3,'Комната2','qqsesgeseseses',100,'',NULL);
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scheduleservices`
--

DROP TABLE IF EXISTS `scheduleservices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `scheduleservices` (
  `ID` int(50) NOT NULL AUTO_INCREMENT,
  `ID_Service` int(50) NOT NULL,
  `ID_Room` int(50) NOT NULL,
  `NumberOfPeople` int(11) DEFAULT NULL,
  `Cost` decimal(10,0) DEFAULT NULL,
  `Date` datetime NOT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `Duration` int(11) DEFAULT NULL,
  `DateDelete` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scheduleservices`
--

LOCK TABLES `scheduleservices` WRITE;
/*!40000 ALTER TABLE `scheduleservices` DISABLE KEYS */;
INSERT INTO `scheduleservices` VALUES (1,5,1,50,400,'2018-12-20 14:43:34','0001-01-01','0001-01-01',60,'23.12.2018 0:00:00'),(2,3,2,10,400,'2018-12-20 15:49:57','0001-01-01','0001-01-01',70,'23.12.2018 0:00:00'),(3,5,1,50,400,'2018-12-21 06:31:27','0001-01-01','0001-01-01',60,'23.12.2018 0:00:00'),(4,3,2,10,400,'2018-12-21 10:33:35','0001-01-01','0001-01-01',60,'23.12.2018 0:00:00'),(5,3,2,10,400,'2018-12-21 15:10:30','0001-01-01','0001-01-01',60,'23.12.2018 0:00:00'),(6,3,2,1,400,'2018-12-21 18:22:53','0001-01-01','0001-01-01',60,'23.12.2018 0:00:00'),(7,5,1,10,500,'2018-12-30 07:10:39','0001-01-01','0001-01-01',60,'23.12.2018 0:00:00'),(11,4,3,7,100,'2018-12-22 12:19:04','0001-01-01','0001-01-01',60,'23.12.2018 0:00:00'),(12,4,3,7,100,'2018-12-23 20:24:00','0001-01-01','0001-01-01',60,'23.12.2018 0:00:00'),(13,3,2,10,400,'2018-12-24 13:39:19','0001-01-01','0001-01-01',60,'23.12.2018 0:00:00'),(14,3,2,10,400,'2018-12-24 13:40:46','0001-01-01','0001-01-01',60,'23.12.2018 0:00:00'),(15,3,1,10,400,'2018-12-23 20:00:01','0001-01-01','0001-01-01',60,'23.12.2018 0:00:00'),(16,3,2,10,400,'2018-12-23 22:00:01','0001-01-01','0001-01-01',60,NULL),(17,4,3,7,100,'2018-12-25 10:00:00','0001-01-01','0001-01-01',60,'24.12.2018 0:00:00'),(18,3,2,10,400,'2018-12-24 17:24:00','0001-01-01','0001-01-01',60,NULL),(19,3,1,10,400,'2018-12-24 17:24:01','0001-01-01','0001-01-01',60,NULL),(20,4,3,7,100,'2018-12-27 21:00:00','0001-01-01','0001-01-01',60,NULL),(21,1,1,0,0,'0001-01-01 00:00:00','2018-12-24','2018-12-26',0,NULL),(22,1,1,0,0,'0001-01-01 00:00:00','2018-12-24','2018-12-27',0,NULL),(23,3,2,10,400,'2018-12-26 08:00:00','0001-01-01','0001-01-01',60,NULL),(24,4,1,7,100,'2018-12-26 08:00:00','0001-01-01','0001-01-01',60,NULL),(25,3,2,10,400,'2018-12-27 09:00:00','0001-01-01','0001-01-01',60,NULL),(26,4,3,7,100,'2018-12-27 09:00:00','0001-01-01','0001-01-01',60,NULL),(27,4,3,7,100,'2018-12-27 09:00:00','0001-01-01','0001-01-01',60,NULL),(28,3,2,10,400,'2018-12-26 12:00:00','0001-01-01','0001-01-01',60,NULL),(29,3,2,10,400,'2018-12-28 16:00:00','0001-01-01','0001-01-01',60,NULL),(30,4,3,7,100,'2018-12-25 08:00:01','0001-01-01','0001-01-01',60,NULL),(31,4,3,7,100,'2018-12-27 12:00:00','0001-01-01','0001-01-01',60,NULL),(32,3,2,10,400,'2018-12-26 08:00:00','0001-01-01','0001-01-01',60,NULL),(33,3,2,10,400,'2018-12-27 08:00:00','0001-01-01','0001-01-01',60,NULL),(34,3,2,10,400,'2018-12-28 08:00:00','0001-01-01','0001-01-01',60,NULL),(35,3,2,10,400,'2018-12-29 08:00:00','0001-01-01','0001-01-01',60,NULL),(36,3,2,10,400,'2018-12-30 08:00:00','0001-01-01','0001-01-01',60,NULL),(37,3,1,10,400,'2018-12-27 08:00:00','0001-01-01','0001-01-01',60,NULL),(38,3,1,10,400,'2018-12-28 08:00:00','0001-01-01','0001-01-01',60,NULL),(39,3,1,10,400,'2018-12-28 09:00:00','0001-01-01','0001-01-01',60,NULL),(40,3,1,10,400,'2018-12-28 10:00:00','0001-01-01','0001-01-01',60,NULL),(41,3,1,10,400,'2018-12-28 11:00:00','0001-01-01','0001-01-01',60,NULL),(42,3,1,10,400,'2018-12-28 12:00:00','0001-01-01','0001-01-01',60,NULL),(43,3,1,10,400,'2018-12-28 13:00:00','0001-01-01','0001-01-01',60,NULL),(44,3,1,10,400,'2018-12-28 14:00:00','0001-01-01','0001-01-01',60,NULL),(45,3,1,10,400,'2018-12-28 15:00:00','0001-01-01','0001-01-01',60,NULL),(46,3,1,10,400,'2018-12-28 16:00:00','0001-01-01','0001-01-01',60,NULL),(47,3,1,10,400,'2018-12-28 17:00:00','0001-01-01','0001-01-01',60,NULL),(48,3,1,10,400,'2018-12-28 18:00:00','0001-01-01','0001-01-01',60,NULL),(49,3,1,10,400,'2018-12-28 19:00:00','0001-01-01','0001-01-01',60,NULL),(50,3,1,10,400,'2018-12-29 08:00:00','0001-01-01','0001-01-01',60,NULL),(51,3,1,10,400,'2018-12-29 09:00:00','0001-01-01','0001-01-01',60,NULL),(52,3,1,10,400,'2018-12-29 10:00:00','0001-01-01','0001-01-01',60,NULL),(53,3,1,10,400,'2018-12-29 11:00:00','0001-01-01','0001-01-01',60,NULL),(54,3,1,10,400,'2018-12-29 12:00:00','0001-01-01','0001-01-01',60,NULL),(55,3,1,10,400,'2018-12-29 13:00:00','0001-01-01','0001-01-01',60,NULL),(56,3,1,10,400,'2018-12-29 14:00:00','0001-01-01','0001-01-01',60,NULL),(57,3,1,10,400,'2018-12-29 15:00:00','0001-01-01','0001-01-01',60,NULL),(58,3,1,10,400,'2018-12-29 16:00:00','0001-01-01','0001-01-01',60,NULL),(59,3,1,10,400,'2018-12-29 17:00:00','0001-01-01','0001-01-01',60,NULL),(60,3,1,10,400,'2018-12-29 18:00:00','0001-01-01','0001-01-01',60,NULL),(61,3,1,10,400,'2018-12-29 19:00:00','0001-01-01','0001-01-01',60,NULL),(62,3,1,10,400,'2018-12-30 08:00:00','0001-01-01','0001-01-01',60,NULL),(63,3,1,10,400,'2018-12-30 09:00:00','0001-01-01','0001-01-01',60,NULL),(64,3,1,10,400,'2018-12-30 10:00:00','0001-01-01','0001-01-01',60,NULL),(65,3,1,10,400,'2018-12-30 11:00:00','0001-01-01','0001-01-01',60,NULL),(66,3,1,10,400,'2018-12-30 12:00:00','0001-01-01','0001-01-01',60,NULL),(67,3,1,10,400,'2018-12-30 13:00:00','0001-01-01','0001-01-01',60,NULL),(68,3,1,10,400,'2018-12-30 14:00:00','0001-01-01','0001-01-01',60,NULL),(69,3,1,10,400,'2018-12-30 15:00:00','0001-01-01','0001-01-01',60,NULL),(70,3,1,10,400,'2018-12-30 16:00:00','0001-01-01','0001-01-01',60,NULL),(71,3,1,10,400,'2018-12-30 17:00:00','0001-01-01','0001-01-01',60,NULL),(72,3,1,10,400,'2018-12-30 18:00:00','0001-01-01','0001-01-01',60,NULL),(73,3,1,10,400,'2018-12-30 19:00:00','0001-01-01','0001-01-01',60,NULL),(74,3,2,10,400,'2018-12-28 14:00:00','0001-01-01','0001-01-01',60,NULL),(75,5,1,50,400,'2018-12-27 07:00:00','0001-01-01','0001-01-01',60,NULL),(76,4,1,7,100,'2018-12-26 07:00:00','0001-01-01','0001-01-01',60,NULL),(77,5,1,50,400,'2018-12-26 19:24:00','0001-01-01','0001-01-01',60,'25.12.2018 0:00:00'),(78,3,1,10,400,'2019-01-02 21:24:00','0001-01-01','0001-01-01',60,'25.12.2018 0:00:00'),(79,5,1,50,400,'2019-01-04 17:24:00','0001-01-01','0001-01-01',60,NULL),(80,5,1,50,400,'2019-01-06 17:24:00','0001-01-01','0001-01-01',60,NULL),(81,5,1,50,400,'2019-01-07 17:24:00','0001-01-01','0001-01-01',60,NULL),(82,5,1,50,400,'2019-01-07 18:24:00','0001-01-01','0001-01-01',60,NULL),(83,5,1,50,400,'2019-01-07 19:24:00','0001-01-01','0001-01-01',60,NULL),(84,5,1,50,400,'2019-01-07 20:24:00','0001-01-01','0001-01-01',60,NULL),(85,5,1,50,400,'2019-01-07 21:24:00','0001-01-01','0001-01-01',60,NULL),(86,5,1,50,400,'2019-01-07 22:24:00','0001-01-01','0001-01-01',60,NULL),(87,5,1,50,400,'2019-01-09 17:24:00','0001-01-01','0001-01-01',60,NULL),(88,5,1,50,400,'2019-01-09 18:24:00','0001-01-01','0001-01-01',60,NULL),(89,5,1,50,400,'2019-01-09 19:24:00','0001-01-01','0001-01-01',60,NULL),(90,5,1,50,400,'2019-01-09 20:24:00','0001-01-01','0001-01-01',60,NULL),(91,5,1,50,400,'2019-01-09 21:24:00','0001-01-01','0001-01-01',60,NULL),(92,5,1,50,400,'2019-01-09 22:24:00','0001-01-01','0001-01-01',60,NULL),(93,5,1,50,400,'2019-01-11 17:24:00','0001-01-01','0001-01-01',60,NULL),(94,5,1,50,400,'2019-01-11 18:24:00','0001-01-01','0001-01-01',60,NULL),(95,5,1,50,400,'2019-01-11 19:24:00','0001-01-01','0001-01-01',60,NULL),(96,5,1,50,400,'2019-01-11 20:24:00','0001-01-01','0001-01-01',60,NULL),(97,5,1,50,400,'2019-01-11 21:24:00','0001-01-01','0001-01-01',60,NULL),(98,5,1,50,400,'2019-01-11 22:24:00','0001-01-01','0001-01-01',60,NULL),(99,5,1,50,400,'2019-01-13 17:24:00','0001-01-01','0001-01-01',60,NULL),(100,5,1,50,400,'2019-01-13 18:24:00','0001-01-01','0001-01-01',60,NULL),(101,5,1,50,400,'2019-01-13 19:24:00','0001-01-01','0001-01-01',60,NULL),(102,5,1,50,400,'2019-01-13 20:24:00','0001-01-01','0001-01-01',60,NULL),(103,5,1,50,400,'2019-01-13 21:24:00','0001-01-01','0001-01-01',60,NULL),(104,5,1,50,400,'2019-01-13 22:24:00','0001-01-01','0001-01-01',60,NULL),(105,5,1,50,400,'2019-01-14 17:24:00','0001-01-01','0001-01-01',60,NULL),(106,5,1,50,400,'2019-01-14 18:24:00','0001-01-01','0001-01-01',60,NULL),(107,5,1,50,400,'2019-01-14 19:24:00','0001-01-01','0001-01-01',60,NULL),(108,5,1,50,400,'2019-01-14 20:24:00','0001-01-01','0001-01-01',60,NULL),(109,5,1,50,400,'2019-01-14 21:24:00','0001-01-01','0001-01-01',60,NULL),(110,5,1,50,400,'2019-01-14 22:24:00','0001-01-01','0001-01-01',60,NULL),(111,5,1,50,400,'2019-01-16 17:24:00','0001-01-01','0001-01-01',60,NULL),(112,5,1,50,400,'2019-01-16 18:24:00','0001-01-01','0001-01-01',60,NULL),(113,5,1,50,400,'2019-01-16 19:24:00','0001-01-01','0001-01-01',60,NULL),(114,5,1,50,400,'2019-01-16 20:24:00','0001-01-01','0001-01-01',60,NULL),(115,5,1,50,400,'2019-01-16 21:24:00','0001-01-01','0001-01-01',60,NULL),(116,5,1,50,400,'2019-01-16 22:24:00','0001-01-01','0001-01-01',60,NULL),(117,5,1,50,400,'2019-01-18 17:24:00','0001-01-01','0001-01-01',60,NULL),(118,5,1,50,400,'2019-01-18 18:24:00','0001-01-01','0001-01-01',60,NULL),(119,5,1,50,400,'2019-01-18 19:24:00','0001-01-01','0001-01-01',60,NULL),(120,5,1,50,400,'2019-01-18 20:24:00','0001-01-01','0001-01-01',60,NULL),(121,5,1,50,400,'2019-01-18 21:24:00','0001-01-01','0001-01-01',60,NULL),(122,5,1,50,400,'2019-01-18 22:24:00','0001-01-01','0001-01-01',60,NULL),(123,5,1,50,400,'2019-01-20 17:24:00','0001-01-01','0001-01-01',60,NULL),(124,5,1,50,400,'2019-01-20 18:24:00','0001-01-01','0001-01-01',60,NULL),(125,5,1,50,400,'2019-01-20 19:24:00','0001-01-01','0001-01-01',60,NULL),(126,5,1,50,400,'2019-01-20 20:24:00','0001-01-01','0001-01-01',60,NULL),(127,5,1,50,400,'2019-01-20 21:24:00','0001-01-01','0001-01-01',60,NULL),(128,5,1,50,400,'2019-01-20 22:24:00','0001-01-01','0001-01-01',60,NULL),(129,5,1,50,400,'2019-01-21 17:24:00','0001-01-01','0001-01-01',60,NULL),(130,5,1,50,400,'2019-01-21 18:24:00','0001-01-01','0001-01-01',60,NULL),(131,5,1,50,400,'2019-01-21 19:24:00','0001-01-01','0001-01-01',60,NULL),(132,5,1,50,400,'2019-01-21 20:24:00','0001-01-01','0001-01-01',60,NULL),(133,5,1,50,400,'2019-01-21 21:24:00','0001-01-01','0001-01-01',60,NULL),(134,5,1,50,400,'2019-01-21 22:24:00','0001-01-01','0001-01-01',60,NULL),(135,5,1,50,400,'2019-01-23 17:24:00','0001-01-01','0001-01-01',60,NULL),(136,5,1,50,400,'2019-01-23 18:24:00','0001-01-01','0001-01-01',60,NULL),(137,5,1,50,400,'2019-01-23 19:24:00','0001-01-01','0001-01-01',60,NULL),(138,5,1,50,400,'2019-01-23 20:24:00','0001-01-01','0001-01-01',60,NULL),(139,5,1,50,400,'2019-01-23 21:24:00','0001-01-01','0001-01-01',60,NULL),(140,5,1,50,400,'2019-01-23 22:24:00','0001-01-01','0001-01-01',60,NULL),(141,5,1,50,400,'2019-01-25 17:24:00','0001-01-01','0001-01-01',60,NULL),(142,5,1,50,400,'2019-01-25 18:24:00','0001-01-01','0001-01-01',60,NULL),(143,5,1,50,400,'2019-01-25 19:24:00','0001-01-01','0001-01-01',60,NULL),(144,5,1,50,400,'2019-01-25 20:24:00','0001-01-01','0001-01-01',60,NULL),(145,5,1,50,400,'2019-01-25 21:24:00','0001-01-01','0001-01-01',60,NULL),(146,5,1,50,400,'2019-01-25 22:24:00','0001-01-01','0001-01-01',60,NULL),(147,5,1,50,400,'2019-01-27 17:24:00','0001-01-01','0001-01-01',60,NULL),(148,5,1,50,400,'2019-01-27 18:24:00','0001-01-01','0001-01-01',60,NULL),(149,5,1,50,400,'2019-01-27 19:24:00','0001-01-01','0001-01-01',60,NULL),(150,5,1,50,400,'2019-01-27 20:24:00','0001-01-01','0001-01-01',60,NULL),(151,5,1,50,400,'2019-01-27 21:24:00','0001-01-01','0001-01-01',60,NULL),(152,5,1,50,400,'2019-01-28 22:24:00','0001-01-01','0001-01-01',60,NULL),(153,1,0,0,0,'0001-01-01 00:00:00','2018-12-25','2018-12-27',0,NULL),(154,2,2,1,500,'2018-12-25 12:00:01','0001-01-01','0001-01-01',60,NULL),(155,2,3,1,500,'2019-03-20 18:00:01','0001-01-01','0001-01-01',70,NULL),(156,3,2,10,400,'2019-03-24 17:24:00','0001-01-01','0001-01-01',60,NULL);
/*!40000 ALTER TABLE `scheduleservices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `serviceinrooms`
--

DROP TABLE IF EXISTS `serviceinrooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `serviceinrooms` (
  `ID_Service` int(50) NOT NULL,
  `ID_Room` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `serviceinrooms`
--

LOCK TABLES `serviceinrooms` WRITE;
/*!40000 ALTER TABLE `serviceinrooms` DISABLE KEYS */;
INSERT INTO `serviceinrooms` VALUES (1,1),(3,1),(3,2),(4,1),(4,3),(5,1),(2,1),(2,2),(2,3);
/*!40000 ALTER TABLE `serviceinrooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `services` (
  `ID` int(50) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL,
  `Cost` double NOT NULL,
  `NumberOfPeople` int(50) NOT NULL,
  `Comment` longtext NOT NULL,
  `DateDelete` date DEFAULT NULL,
  `Duration` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (1,'Заморозка',0,1,'11',NULL,1),(2,'Массаж',500,1,'',NULL,60),(3,'Йога',400,10,'',NULL,60),(4,'Услуга2',100,7,'',NULL,60),(5,'Плавание',400,50,'От 12 лет',NULL,60);
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicesintariffs`
--

DROP TABLE IF EXISTS `servicesintariffs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `servicesintariffs` (
  `ID_Service` int(50) NOT NULL,
  `ID_Tariff` int(50) NOT NULL,
  `Amount` int(50) NOT NULL,
  `Periodicity` int(50) NOT NULL,
  `DateDelete` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicesintariffs`
--

LOCK TABLES `servicesintariffs` WRITE;
/*!40000 ALTER TABLE `servicesintariffs` DISABLE KEYS */;
INSERT INTO `servicesintariffs` VALUES (1,1,53,9,NULL),(1,2,33,20,'16.12.2018 0:00:00'),(1,2,2,6,NULL),(3,2,6,3,NULL),(1,4,30,5,NULL),(5,4,100,1,NULL),(2,4,10,1,NULL),(3,4,100,1,NULL);
/*!40000 ALTER TABLE `servicesintariffs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tariffs`
--

DROP TABLE IF EXISTS `tariffs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tariffs` (
  `ID` int(50) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `Duration` int(50) NOT NULL,
  `TotalCost` decimal(10,0) NOT NULL,
  `StartDate` date NOT NULL,
  `ExpirationDate` date DEFAULT NULL,
  `DateRemoved` date DEFAULT NULL,
  `DateDelete` varchar(50) DEFAULT NULL,
  `Comment` longtext,
  `TariffTime` longtext,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tariffs`
--

LOCK TABLES `tariffs` WRITE;
/*!40000 ALTER TABLE `tariffs` DISABLE KEYS */;
INSERT INTO `tariffs` VALUES (1,'Тариф1',249,10000,'2018-10-28','2019-02-21','2019-07-11',NULL,'','ПН|09:00-10:00|11:00-12:00|12:00-13:00|14:00-15:00|16:00-17:00|18:00-19:00|19:00-20:00|20:00-21:00|#ВТ|13:00-14:00|#СР|11:00-12:00|#ЧТ|13:00-14:00|#ПТ|#СБ|#ВС|08:00-09:00|09:00-10:00|10:00-11:00|11:00-12:00|13:00-14:00|14:00-15:00|16:00-17:00|17:00-18:00|18:00-19:00|#'),(2,'Мини',59,281,'2018-10-28','2018-10-28','2018-10-28',NULL,'','ПН|09:00-10:00|13:00-14:00|15:00-16:00|16:00-17:00|18:00-19:00|#ВТ|10:00-11:00|12:00-13:00|13:00-14:00|16:00-17:00|18:00-19:00|#СР|13:00-14:00|14:00-15:00|17:00-18:00|19:00-20:00|#ЧТ|11:00-12:00|13:00-14:00|15:00-16:00|17:00-18:00|#ПТ|14:00-15:00|#СБ|12:00-13:00|14:00-15:00|16:00-17:00|18:00-19:00|19:00-20:00|#ВС|10:00-11:00|12:00-13:00|13:00-14:00|15:00-16:00|17:00-18:00|18:00-19:00|20:00-21:00|#'),(3,'rdzzgrt',5,33,'2018-10-28','2018-10-28','2018-10-28','25.12.2018 0:00:00','gchjgygcjgc',NULL),(4,'Утренний',365,20000,'2018-10-28','2018-10-28','2018-10-28',NULL,'','ПН|06:00-07:00|07:00-08:00|08:00-09:00|09:00-10:00|10:00-11:00|11:00-12:00|12:00-13:00|13:00-14:00|14:00-15:00|15:00-16:00|#ВТ|#СР|15:00-16:00|16:00-17:00|17:00-18:00|18:00-19:00|19:00-20:00|20:00-21:00|#ЧТ|#ПТ|#СБ|#ВС|#');
/*!40000 ALTER TABLE `tariffs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trainerservices`
--

DROP TABLE IF EXISTS `trainerservices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `trainerservices` (
  `ID_Service` int(50) NOT NULL,
  `ID_Trainer` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trainerservices`
--

LOCK TABLES `trainerservices` WRITE;
/*!40000 ALTER TABLE `trainerservices` DISABLE KEYS */;
INSERT INTO `trainerservices` VALUES (1,3),(4,3),(3,5),(2,6),(3,6),(4,6),(5,5),(5,7);
/*!40000 ALTER TABLE `trainerservices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trainersinschedule`
--

DROP TABLE IF EXISTS `trainersinschedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `trainersinschedule` (
  `ID_Trainer` int(50) NOT NULL,
  `ID_Schedule` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trainersinschedule`
--

LOCK TABLES `trainersinschedule` WRITE;
/*!40000 ALTER TABLE `trainersinschedule` DISABLE KEYS */;
INSERT INTO `trainersinschedule` VALUES (5,2),(3,1),(5,4),(3,7),(5,6),(5,11),(3,12),(5,13),(5,14),(3,3),(6,17),(6,155),(5,156);
/*!40000 ALTER TABLE `trainersinschedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `ID` int(50) NOT NULL AUTO_INCREMENT,
  `Login` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `Surname` varchar(80) DEFAULT NULL,
  `Name` varchar(80) DEFAULT NULL,
  `MiddleName` varchar(80) DEFAULT NULL,
  `Sex` tinyint(1) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `Phone` varchar(20) DEFAULT NULL,
  `Height` int(10) DEFAULT NULL,
  `Weight` int(10) DEFAULT NULL,
  `Health` longtext,
  `Comment` longtext,
  `Qualification` longtext,
  `StateUser` varchar(50) NOT NULL COMMENT 'Trainer/Admin/Manager/Client',
  `DateDelete` varchar(50) DEFAULT NULL,
  `DateAdd` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'кпупяку44334','1234','кпупяку44334','Иванов','Иванов','Иванов',1,'1998-01-16','434535333333333',80,0,'fdfddddddddggbgg','вымывмуыа',NULL,'Client',NULL,'25.12.2018 0:00:00'),(2,'1234','1234','123456789875643567890','Единорожкова','Алиса','Встарнечудесовна',0,'2000-12-17','111111111111111',80,0,'rdyydrtyrtydyt','аспесеар',NULL,'Client',NULL,'25.12.2018 0:00:00'),(3,'','','1343цуавам23','Петров','Петр','Петрович',1,'1990-12-19','123123123124414',0,0,NULL,'яч4еячцу4ч4цацасц4п4пцкыппппккккккккккккккккккккккккккккккккккккккккккккккккккккккккккккккккккккккк','кууууууууууууууууууууууууууууууккккккккккккк','Trainer',NULL,'25.12.2018 0:00:00'),(4,'','','234234234344444234','Андреева','Елизавета','Евгеньевна',0,'2013-12-19','12131431444443',0,0,NULL,'','','Trainer',NULL,'25.12.2018 0:00:00'),(5,'123123123123123','1234','кавсуукуккпупуккпукыкпуукувка','Нестерович','Полина','Андреевна',0,'2000-02-07','123123123123123',0,0,NULL,'','','Trainer',NULL,'25.12.2018 0:00:00'),(6,'0000','0000','343333333333333333333333334','Фалилеева','Лариса','Семеновна',0,'1961-11-24','123333333333333',0,0,NULL,'','','Trainer',NULL,'25.12.2018 0:00:00'),(7,'','','123412341234','Егоров','Егор','Егорович',1,'2000-12-19','233333333333333',0,0,NULL,'','','Trainer',NULL,'25.12.2018 0:00:00'),(8,'','','мсмс5ес54с54е45м','Кружкина','Екатерина','Петровна',0,'1990-12-19','23564324888',80,0,'','',NULL,'Client',NULL,'25.12.2018 0:00:00'),(9,'admin','admin',NULL,NULL,NULL,NULL,1,'1990-12-19',NULL,1,1,'11',NULL,NULL,'Admin',NULL,'25.12.2018 0:00:00'),(10,'','','с4у5еп','цук','цкцу','цукцук',1,'2013-12-25','5345',80,0,'','',NULL,'Client','25.12.2018 0:00:00','25.12.2018 0:00:00'),(11,'','','ddddd@mail.ru','Иванов','Иван','Иванович',1,'2013-12-25','3456789',80,0,'','',NULL,'Client',NULL,'25.12.2018 0:00:00');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visitors_log`
--

DROP TABLE IF EXISTS `visitors_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `visitors_log` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_Reservation` int(11) NOT NULL,
  `TimeVisiting` datetime DEFAULT NULL,
  `TimeLeaving` datetime DEFAULT NULL,
  `Payment` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visitors_log`
--

LOCK TABLES `visitors_log` WRITE;
/*!40000 ALTER TABLE `visitors_log` DISABLE KEYS */;
INSERT INTO `visitors_log` VALUES (1,30,'2018-12-23 22:24:37','2018-12-23 23:53:57','оплачено'),(3,35,'2018-12-26 03:52:57','2018-12-26 03:54:24','оплачено'),(4,36,'2018-12-26 03:55:33','2018-12-26 03:55:51','оплачено'),(5,37,'2018-12-26 06:23:50','2018-12-26 06:24:49','оплачено'),(6,38,'2018-12-26 06:27:20','2018-12-26 06:28:31','оплачено'),(7,39,'2018-12-26 06:46:26','2018-12-26 06:46:57','оплачено'),(8,45,'2018-12-26 07:22:35','2018-12-26 07:25:57','оплачено'),(9,46,'2018-12-26 07:27:13','2018-12-26 07:28:55','оплачено'),(10,54,'2019-03-24 14:29:33','2019-03-24 14:30:19','оплачено');
/*!40000 ALTER TABLE `visitors_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'fitos'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-03 17:43:39
