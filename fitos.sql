-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: fitos
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `balancecards`
--

DROP TABLE IF EXISTS `balancecards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `balancecards` (
  `ID_Service` int(11) NOT NULL,
  `ID_Card` int(11) NOT NULL,
  `Balance` int(11) NOT NULL,
  `LastDay` text,
  PRIMARY KEY (`ID_Service`,`ID_Card`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `balancecards`
--

LOCK TABLES `balancecards` WRITE;
/*!40000 ALTER TABLE `balancecards` DISABLE KEYS */;
/*!40000 ALTER TABLE `balancecards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cards`
--

DROP TABLE IF EXISTS `cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `n_Card` text,
  `id_User` int(11) NOT NULL,
  `id_Tariff` int(11) NOT NULL,
  `dateOfCreation` text,
  `dateDelete` text,
  `dataPurchaseTariff` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cards`
--

LOCK TABLES `cards` WRITE;
/*!40000 ALTER TABLE `cards` DISABLE KEYS */;
/*!40000 ALTER TABLE `cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `options`
--

DROP TABLE IF EXISTS `options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `options` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text,
  `Value` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `options`
--

LOCK TABLES `options` WRITE;
/*!40000 ALTER TABLE `options` DISABLE KEYS */;
/*!40000 ALTER TABLE `options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `payments` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DateOfCreation` datetime NOT NULL,
  `DateOfPayment` datetime NOT NULL,
  `Amount` decimal(18,2) NOT NULL,
  `Paid` decimal(18,2) NOT NULL,
  `ID_Reservation` int(11) NOT NULL,
  `ID_User` int(11) NOT NULL,
  `DateDelete` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photos`
--

DROP TABLE IF EXISTS `photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `photos` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_User` int(11) NOT NULL,
  `ID_Room` int(11) NOT NULL,
  `Name` text,
  `DateDelete` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photos`
--

LOCK TABLES `photos` WRITE;
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;
/*!40000 ALTER TABLE `photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservations`
--

DROP TABLE IF EXISTS `reservations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `reservations` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_ScheduleService` int(11) NOT NULL,
  `ID_Card` int(11) NOT NULL,
  `ID_User` int(11) NOT NULL,
  `DateDelete` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservations`
--

LOCK TABLES `reservations` WRITE;
/*!40000 ALTER TABLE `reservations` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `equipment` text,
  `capacity` int(11) NOT NULL,
  `comment` text,
  `dateDelete` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms`
--

LOCK TABLES `rooms` WRITE;
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scheduleservices`
--

DROP TABLE IF EXISTS `scheduleservices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `scheduleservices` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_Service` int(11) NOT NULL,
  `ID_Room` int(11) NOT NULL,
  `NumberOfPeople` int(11) NOT NULL,
  `Cost` decimal(18,2) NOT NULL,
  `Duration` int(11) NOT NULL,
  `Date` datetime NOT NULL,
  `StartDate` datetime NOT NULL,
  `EndDate` datetime NOT NULL,
  `DateDelete` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scheduleservices`
--

LOCK TABLES `scheduleservices` WRITE;
/*!40000 ALTER TABLE `scheduleservices` DISABLE KEYS */;
/*!40000 ALTER TABLE `scheduleservices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `serviceinrooms`
--

DROP TABLE IF EXISTS `serviceinrooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `serviceinrooms` (
  `ID_Service` int(11) NOT NULL,
  `ID_Room` int(11) NOT NULL,
  PRIMARY KEY (`ID_Service`,`ID_Room`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `serviceinrooms`
--

LOCK TABLES `serviceinrooms` WRITE;
/*!40000 ALTER TABLE `serviceinrooms` DISABLE KEYS */;
/*!40000 ALTER TABLE `serviceinrooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `duration` int(11) NOT NULL,
  `cost` decimal(18,2) NOT NULL,
  `numberOfPeople` int(11) NOT NULL,
  `comment` text,
  `dateDelete` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicesintariffs`
--

DROP TABLE IF EXISTS `servicesintariffs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `servicesintariffs` (
  `ID_Service` int(11) NOT NULL,
  `ID_Tariff` int(11) NOT NULL,
  `Amount` int(11) NOT NULL,
  `Periodicity` int(11) NOT NULL,
  `DateDelete` text,
  PRIMARY KEY (`ID_Service`,`ID_Tariff`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicesintariffs`
--

LOCK TABLES `servicesintariffs` WRITE;
/*!40000 ALTER TABLE `servicesintariffs` DISABLE KEYS */;
/*!40000 ALTER TABLE `servicesintariffs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tariffs`
--

DROP TABLE IF EXISTS `tariffs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tariffs` (
  `TariffTime` text,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `duration` int(11) NOT NULL,
  `totalCost` decimal(18,2) NOT NULL,
  `startDate` datetime NOT NULL,
  `expirationDate` datetime NOT NULL,
  `dateRemoved` datetime NOT NULL,
  `dateDelete` text,
  `comment` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tariffs`
--

LOCK TABLES `tariffs` WRITE;
/*!40000 ALTER TABLE `tariffs` DISABLE KEYS */;
/*!40000 ALTER TABLE `tariffs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trainerservices`
--

DROP TABLE IF EXISTS `trainerservices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `trainerservices` (
  `ID_Service` int(11) NOT NULL,
  `ID_Trainer` int(11) NOT NULL,
  PRIMARY KEY (`ID_Service`,`ID_Trainer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trainerservices`
--

LOCK TABLES `trainerservices` WRITE;
/*!40000 ALTER TABLE `trainerservices` DISABLE KEYS */;
/*!40000 ALTER TABLE `trainerservices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trainersinschedule`
--

DROP TABLE IF EXISTS `trainersinschedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `trainersinschedule` (
  `ID_Trainer` int(11) NOT NULL,
  `ID_Schedule` int(11) NOT NULL,
  PRIMARY KEY (`ID_Trainer`,`ID_Schedule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trainersinschedule`
--

LOCK TABLES `trainersinschedule` WRITE;
/*!40000 ALTER TABLE `trainersinschedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `trainersinschedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` text,
  `password` text,
  `email` text,
  `surname` text,
  `name` text,
  `middleName` text,
  `sex` smallint(6) NOT NULL,
  `phone` text,
  `dob` datetime NOT NULL,
  `comment` text,
  `dateDelete` text,
  `height` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  `health` text,
  `qualification` text,
  `stateUser` text,
  `dateAdd` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visitors_log`
--

DROP TABLE IF EXISTS `visitors_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `visitors_log` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_Reservation` int(11) NOT NULL,
  `TimeVisiting` datetime NOT NULL,
  `TimeLeaving` datetime NOT NULL,
  `Payment` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visitors_log`
--

LOCK TABLES `visitors_log` WRITE;
/*!40000 ALTER TABLE `visitors_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `visitors_log` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-03 17:57:30
